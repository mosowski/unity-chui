using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Chui {


	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Root")]
	public class Root : Widget {

		public enum TouchEventType {
			TouchBegan,
			TouchMoved,
			TouchEnded,
			TouchPressed,
			TouchReleased,
			TouchClicked
		}

		public enum ScalingBehaviour {
			AspectThreshold,
			InverseAspectThreshold
		}

		class QueuedTouchPhaseChange {
			public TouchInfo.Phase phase;
			public int id;
			public Vector3 rawPosition;
			public float pressure;

		}

		static Root instance;
		static public Root GetInstance() {
#if UNITY_EDITOR
			if (instance == null) {
				instance = GameObject.FindObjectOfType<Root>();
			}
#endif
			return instance;
		}

		static public GameObject InjectorsContainer {
			get {
				return GetInstance().GetInjectorsContainer();
			}
		}

		public float physicalWidth = 4;

		public List<Camera> slaveCameras = new List<Camera>();
		public Camera mainCamera;

		public ScalingBehaviour scalingBehaviour;
		public float aspectThreshold = 0;

		public float referenceDpi = 100;
		public bool useDpiScaling = false;

		public float referenceWidth;
		public float referenceHeight;

		public int deterministicFrameRate = 60;

		public Replay replay;

		public SpriteQuadContent.Settings spriteQuadContentSettings;

		public bool showInjectorsContainer = false;

		Vector3 mousePosition;
		List<TouchInfo> touches = new List<TouchInfo>();
		List<int> activeTouches = new List<int>();
		Plane touchPlane;
		Queue<QueuedTouchPhaseChange> touchPhaseChangeQueue = new Queue<QueuedTouchPhaseChange>();
		GameObject camerasContainer;
		GameObject injectorsContainer;
		int deterministicStepCount = -1;

		List<Window> windows = new List<Window>();

		public Camera MainCamera {
			get {
				return mainCamera;
			}
		}

		public int DeterministicStepCount {
			get {
				return deterministicStepCount;
			}
		}

		public List<Window> Windows {
			get {
				return windows;
			}
		}

		public static float PhysicalWidth {
			get {
				return GetInstance().physicalWidth;
			}
		}

		void Awake() {
			Root.instance = this;
			touchPlane = new Plane(Vector3.forward, Vector3.zero);
			touchPhaseChangeQueue = new Queue<QueuedTouchPhaseChange>();

		}

		void OnEnable() {
#if UNITY_EDITOR
			if (!Application.isPlaying) {
				RevalidateEverything();
			}
#endif
		}

		GameObject GetInjectorsContainer() {
			if (injectorsContainer == null) {
				injectorsContainer = Util.GetChild(gameObject, "InjectorsContainer");
				if (injectorsContainer != null) {
					GameObject.DestroyImmediate(injectorsContainer);
				}
				injectorsContainer = new GameObject();
				injectorsContainer.name = "InjectorsContainer";
				injectorsContainer.transform.SetParent(transform, false);
				if (!showInjectorsContainer) {
					injectorsContainer.hideFlags = HideFlags.HideInHierarchy;
				} else {
					injectorsContainer.hideFlags = HideFlags.None;
				}
				var widget = injectorsContainer.AddComponent<AnchoredWidget>();
			}
			return injectorsContainer;
		}

		void LateUpdate() {
			UpdateHierarchy();

			if (Application.isPlaying) {
				if (replay == null) {
					RecursiveDeterministicStep();
				}
				CollectInput();
			} else {
#if UNITY_EDITOR
				UnityEditor.EditorApplication.delayCall += () => {
					GetInjectorsContainer().GetComponent<Widget>().RevalidateHierarchy();
				};
#endif
			}
		}

		public void RevalidateEverything() {
			if (Application.isPlaying) {
				RevalidateHierarchy();
			} else {
				RevalidateHierarchy();
				GetInjectorsContainer().GetComponent<Widget>().RevalidateHierarchy();
			}
		}

		void RecalculateAspect() {
			if (slaveCameras.Count == 0 || mainCamera == null) {
				return;
			}

			if (referenceDpi == 0) {
				referenceDpi = 1;
			}
			float dpi = Screen.dpi;
			if (dpi == 0 || !useDpiScaling || Application.isEditor) {
				dpi = referenceDpi;
			}

			float dpiWidth = referenceWidth * dpi / referenceDpi;
			float dpiHeight = referenceHeight * dpi / referenceDpi;

			if ((scalingBehaviour == ScalingBehaviour.AspectThreshold && mainCamera.aspect > aspectThreshold)
				|| (scalingBehaviour == ScalingBehaviour.InverseAspectThreshold && mainCamera.aspect <= aspectThreshold)) {
				volume.Width = dpiWidth;
				volume.Height = dpiWidth / mainCamera.aspect;
			} else if ((scalingBehaviour == ScalingBehaviour.AspectThreshold && mainCamera.aspect <= aspectThreshold)
				|| (scalingBehaviour == ScalingBehaviour.InverseAspectThreshold && mainCamera.aspect > aspectThreshold)) {
				volume.Width = dpiHeight * mainCamera.aspect;
				volume.Height = dpiHeight;
			}

			volume.xMin = -volume.Width * 0.5f;
			volume.yMin = -volume.Height * 0.5f;

			float designScale = physicalWidth / referenceWidth;
			transform.localScale = new Vector3(designScale, designScale, designScale);
		}

		protected override void UpdateVolume() {
			derivedColor = Color.white;

			if (camerasContainer == null) {
				camerasContainer = Util.GetChild(gameObject, "CamerasContainer");
				if (camerasContainer == null) {
					camerasContainer = new GameObject();
					camerasContainer.name = "CamerasContainer";
					camerasContainer.transform.SetParent(transform, false);
				}
			}

			Vector3 lastSize = volume.Size;

			RecalculateAspect();

			if (volume.Size != lastSize) {
				changes |= ChangeBits.Transform;
			}

			foreach (Camera slaveCamera in slaveCameras) {
				if (slaveCamera == null) {
					continue;
				}

				slaveCamera.transparencySortMode = TransparencySortMode.Orthographic;
				slaveCamera.transform.parent = camerasContainer.transform;

				if (slaveCamera.orthographic) {
					slaveCamera.orthographicSize = volume.Height * physicalWidth / referenceWidth / 2;
				} else {
					float planeDist = volume.Height / (Mathf.Tan(slaveCamera.fieldOfView * Mathf.Deg2Rad * 0.5f) * 2);
					slaveCamera.transform.localPosition = new Vector3(
							slaveCamera.transform.localPosition.x,
							slaveCamera.transform.localPosition.y,
							-planeDist
					);
				}
			}
		}

		//NOTE: Window management capabilities should not be implemented by Root. This function might be removed soon.
		public void AddWindow(GameObject go) {
			AddWindow(go.GetComponent<Window>());
		}

		//NOTE: Window management capabilities should not be implemented by Root. This function might be removed soon.
		public void RemoveWindow(GameObject go) {
			RemoveWindow(go.GetComponent<Window>());
		}

		//NOTE: Window management capabilities should not be implemented by Root. This function might be removed soon.
		public void AddWindow(Window window) {
			windows.Add(window);
			window.transform.SetParent(transform, false);
			SortWindows();
			window.GetComponent<Widget>().RevalidateHierarchy();
		}

		//NOTE: Window management capabilities should not be implemented by Root. This function might be removed soon.
		public void RemoveWindow(Window window) {
			int windowIndex = windows.IndexOf(window);
			windows.Remove(window);
			SortWindows();
			for (int i = windowIndex; i < windows.Count; ++i) {
				windows[i].GetComponent<Widget>().RevalidateHierarchy();
			}
		}

		//NOTE: Window management capabilities should not be implemented by Root. This function might be removed soon.
		public void MoveWindowToFront(Window window) {
			int windowIndex = windows.IndexOf(window);
			windows.Remove(window);
			windows.Add(window);
			SortWindows();
			for (int i = windowIndex; i < windows.Count; ++i) {
				windows[i].GetComponent<Widget>().RevalidateHierarchy();
			}
		}

		//NOTE: Window management capabilities should not be implemented by Root. This function might be removed soon.
		void SortWindows() {
			int lastFullScreen = windows.FindLastIndex(w => w.isFullscreen);

			float depth = 0;
			for (int i = 0; i < windows.Count; ++i) {
				AnchoredWidget widget = windows[i].GetComponent<AnchoredWidget>();
				widget.backAnchor.offset = -depth;
				widget.frontAnchor.offset = -depth;
				if (i >= lastFullScreen) {
					widget.gameObject.SetActive(true);
					depth += windows[i].depthVolume;
				} else {
					widget.gameObject.SetActive(false);
				}
			}
		}

		public Vector3 GetPositionOfScreenPoint(Vector3 screenPosition) {
			Ray ray = mainCamera.ScreenPointToRay(screenPosition);
			float enter = 0;
			if (touchPlane.Raycast(ray, out enter)) {
				return ray.GetPoint(enter);
			} else {
				return Vector3.zero;
			}
		}

		public Plane GetPlane() {
			return touchPlane;
		}

		void SendTouchEvent(TouchEventType type, TouchInfo ti) {
			switch (type) {
				case TouchEventType.TouchBegan:
					SendTouchEvent(ti.rawPosition, eh => eh.onTouchBegan.Invoke(ti));
					break;

				case TouchEventType.TouchPressed:
					SendTouchEvent(ti.rawPosition, eh => {
						ti.onUpdate += eh.UpdateTouch;
						eh.capturedTouches.Add(ti);
						if (eh.onPressed.Invoke(ti)) {
							eh.pressedTouches.Add(ti);
							return true;
						} else {
							return false;
						}
					});
					break;

				case TouchEventType.TouchMoved:
					SendTouchEvent(ti.rawPosition, el => el.onTouchMoved.Invoke(ti));
					break;

				case TouchEventType.TouchClicked:
					SendTouchEvent(ti.rawPosition, eh => {
						if (eh.capturedTouches.Contains(ti)) {
							return eh.onClicked.Invoke(ti);
						} else {
							return false;
						}
					});
					break;

				case TouchEventType.TouchEnded:
					SendTouchEvent(ti.rawPosition, eh => eh.onTouchEnded.Invoke(ti));
					break;
			}
		}

		RaycastHit[] GetSortedHits(Vector3 rawPosition) {
			Ray ray = mainCamera.ScreenPointToRay(rawPosition);
			RaycastHit[] hits = Physics.RaycastAll(ray, 100);
			System.Array.Sort(hits, (h0, h1) => Vector3.Distance(ray.origin, h0.collider.transform.position).CompareTo(Vector3.Distance(ray.origin, h1.collider.transform.position)));
			return hits;
		}

		public bool CheckHit(Vector3 rawPosition, GameObject target) {
			return GetSortedHits(rawPosition).Any(h => h.collider.gameObject == target);
		}

		public bool CheckHit(TouchInfo ti, GameObject target) {
			return CheckHit(ti.rawPosition, target);
		}

		public List<GameObject> GetHitObjects(Vector3 rawPosition) {
			return GetSortedHits(rawPosition).Select(h => h.collider.gameObject).ToList();
		}

		public List<GameObject> GetHitObjects(TouchInfo ti) {
			return GetHitObjects(ti.rawPosition);
		}

		void SendTouchEvent(Vector3 position, Func<EventListener.Handlers, bool> sender) {
			if (mainCamera == null) {
				return;
			}
			var hits = GetSortedHits(position);

			// capturing
			for (int i = hits.Length - 1; i >= 0; --i) {
				EventListener el = hits[i].collider.gameObject.GetComponent<EventListener>();
				if (el != null) {
					if (sender(el.capturing)) {
						break;
					}
				}
			}

			// bubbling
			for (int i = 0; i < hits.Length; ++i) {
				EventListener el = hits[i].collider.gameObject.GetComponent<EventListener>();
				if (el != null) {
					if (sender(el.bubbling)) {
						break;
					}
				}
			}
		}

		TouchInfo GetTouchInfo(int id) {
			for (int i = 0; i < touches.Count; ++i) {
				if (touches[i].id == id) {
					return touches[i];
				}
			}
			TouchInfo ti = new TouchInfo();
			ti.id = id;
			ti.phase = TouchInfo.Phase.Invalid;
			touches.Add(ti);
			return ti;
		}

		void InvalidateTouches() {
			for (int i = touches.Count - 1; i >= 0; --i) {
				TouchInfo ti = touches[i];
				if (ti.phase == TouchInfo.Phase.Begin) {
					ti.phase = TouchInfo.Phase.Move;
				}
				if (ti.phase == TouchInfo.Phase.End) {
					ti.phase = TouchInfo.Phase.Invalid;
					touches.Remove(ti);
				}
			}
		}

		void QueueTouchPhaseChange(TouchInfo.Phase phase, int id, Vector3 rawPosition, float pressure) {
			touchPhaseChangeQueue.Enqueue(new QueuedTouchPhaseChange() {
				phase = phase,
				id = id,
				rawPosition = rawPosition,
				pressure = pressure
			});
		}

		void UpdateMouseInput() {
			int fingerId = 0;

			for (int i = 0; i < activeTouches.Count; ++i) {
				QueueTouchPhaseChange(TouchInfo.Phase.Move, fingerId + 1, Input.mousePosition, 1);
			}

			if (Input.GetMouseButtonDown(0)) {
				if (!activeTouches.Contains(fingerId)) {
					activeTouches.Add(fingerId);
					QueueTouchPhaseChange(TouchInfo.Phase.Begin, fingerId + 1, Input.mousePosition, 1);
				}
			}

			if (Input.GetMouseButtonUp(0)) {
				if (activeTouches.Contains(fingerId)) {
					QueueTouchPhaseChange(TouchInfo.Phase.End, fingerId + 1, Input.mousePosition, 1);
					activeTouches.Remove(fingerId);
				}
			} 
		}

		void UpdateTouchInput() {
			for (int i = 0; i < Input.touchCount; ++i) {
				Touch t = Input.GetTouch(i);

				//NOTE:
				// Crazy encounter: Sometimes, when touch is uncanny short, TouchPhase.Begin is being skipped,
				// so I decided to not rely on Begin phase.
				//
				if (activeTouches.Contains(t.fingerId)) {
					QueueTouchPhaseChange(TouchInfo.Phase.Move, t.fingerId + 1, t.position, t.pressure);
				} else {
					activeTouches.Add(t.fingerId);
					QueueTouchPhaseChange(TouchInfo.Phase.Begin, t.fingerId + 1, t.position, t.pressure);
				}

				if (t.phase == TouchPhase.Ended) {
					QueueTouchPhaseChange(TouchInfo.Phase.End, t.fingerId + 1, t.position, t.pressure);
					activeTouches.Remove(t.fingerId);
				}
			}
		}

		void UpdateReplayInput() {
			foreach (Replay.TouchReplayEvent touchEvent in replay.GetTouchEventsForCurrentFrame()) {
				float sx = (float)Screen.width / replay.screenWidth;
				float sy = (float)Screen.height / replay.screenHeight;
				TouchInfo ti = GetTouchInfo(touchEvent.id);
				ti.rawPosition = touchEvent.rawPosition;
				ti.rawPosition.x *= sx;
				ti.rawPosition.y *= sy;
				ti.pressure = touchEvent.pressure;

				if (touchEvent.type == TouchInfo.Phase.Begin) {
					QueueTouchPhaseChange(TouchInfo.Phase.Begin, ti.id, ti.rawPosition, ti.pressure);
				} else if (touchEvent.type == TouchInfo.Phase.Move) {
					QueueTouchPhaseChange(TouchInfo.Phase.Move, ti.id, ti.rawPosition, ti.pressure);
				} else if (touchEvent.type == TouchInfo.Phase.End) {
					QueueTouchPhaseChange(TouchInfo.Phase.End, ti.id, ti.rawPosition, ti.pressure);
				}
			}
		}

		void UpdateTouches() {
			foreach (TouchInfo ti in touches) {
				ti.Update(DeterministicStepCount);
			}
		}

		void UpdateTouchPlane() {
			touchPlane = new Plane(transform.TransformDirection(Vector3.forward), transform.position);
		}


		void CollectInput() {
			if (replay == null || !replay.isReplaying) {
				if (Application.isMobilePlatform) {
					UpdateTouchInput();
				} else {
					UpdateMouseInput();
				}
			}
		}

		void ConsumeTouchPhaseChange(QueuedTouchPhaseChange change) {
			TouchInfo ti = GetTouchInfo(change.id);
			if (replay != null && replay.isRecording) {
				replay.RecordTouchEvent(change.phase, change.id, change.rawPosition);
			}

			bool moved = ti.rawPosition != change.rawPosition;

			Vector3 oldPosition = ti.position;

			ti.phase = change.phase;
			ti.rawPosition = change.rawPosition;
			ti.UpdatePosition(GetPositionOfScreenPoint(change.rawPosition));
			ti.pressure = change.pressure;

			if (replay != null && replay.isReplaying) {
				if (replay.replayTouchIndicatorPrefab != null) {
					GameObject indicator = Instantiate(replay.replayTouchIndicatorPrefab);
					indicator.transform.SetParent(transform, false);
					indicator.GetComponent<Widget>().RevalidateHierarchy();
					indicator.GetComponent<Widget>().SetWorldPosition(ti.position);
					indicator.GetComponent<Widget>().RevalidateHierarchy();
					GameObject.Destroy(indicator, 1.0f);
				}
			}

			if (change.phase == TouchInfo.Phase.Begin) {
				ti.startPosition = ti.position;
				ti.startStep = DeterministicStepCount;
				SendTouchEvent(TouchEventType.TouchBegan, ti);
				SendTouchEvent(TouchEventType.TouchPressed, ti);
			} else if (change.phase == TouchInfo.Phase.Move && moved) {
				SendTouchEvent(TouchEventType.TouchMoved, ti);
			} else if (change.phase == TouchInfo.Phase.End) {
				SendTouchEvent(TouchEventType.TouchClicked, ti);
				SendTouchEvent(TouchEventType.TouchEnded, ti);
				ti.Update(DeterministicStepCount);
			}
		}


		void ConsumeInput() {
			if (replay != null && replay.isReplaying) {
				UpdateReplayInput();
			}

			UpdateTouchPlane();

			while (touchPhaseChangeQueue.Count > 0) {
				ConsumeTouchPhaseChange(touchPhaseChangeQueue.Dequeue());
			}
			InvalidateTouches();

			UpdateTouches();
		}

		override public void DeterministicStep() {
			deterministicStepCount++;
			ConsumeInput();
		}

		public void ClearInjectorContainer() {
			Util.DestroyImmediateDirectChildren(GetInjectorsContainer());
		}

		void OnDrawGizmos() {
			Gizmos.color = Color.magenta;
			Gizmos.matrix = transform.localToWorldMatrix;
			Gizmos.DrawWireCube(Vector3.zero, new Vector3(volume.Width, volume.Height, volume.Depth));
		}
	}
}
