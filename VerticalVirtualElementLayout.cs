﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Chui {
    public class VerticalVirtualElementLayout : VirtualElementLayout  {

        public override Data GenerateLayout(VirtualElementGroup group) {
            Data result = new Data();
            result.elements = new Dictionary<VirtualElement, ElementData>();

            float y = 0.0f;

            for(int i = 0; i < group.elements.Count; ++i) {
                VirtualElement e = group.elements[i];
                Vector3 size = e.GetSize();

                ElementData elementData = new ElementData();
                elementData.localPosition = new Vector3(0.0f, -y-0.5f*size.y, -50.0f);
                elementData.RecalculateVolume(size);

                result.elements.Add(e, elementData);
                y += size.y;

                result.contentWidth = Mathf.Max(result.contentWidth, size.x);
            }

            result.overrideHeight = true;
            result.contentHeight = y;

            return result;
        }
    }
}