using UnityEngine;
using System.Collections;

namespace Chui {

	public delegate void KeyboardInputSubmitDelegate(KeyboardInput ki);


	[AddComponentMenu("Chui/Keyboard Input")]
	public class KeyboardInput : MonoBehaviour {
		public TextContent target;
		public TouchScreenKeyboardType keyboardType;
		public bool autocorrection = true;
		public bool multiline = false;
		public bool secure = false;
		public bool alert = false;
		public bool hideInput = false;
		public bool clearOnFocus = false;
		public bool openOnTap = false;
		public string placeholderText = "";

		public event KeyboardInputSubmitDelegate onFocus;
		public event KeyboardInputSubmitDelegate onDefocus;
		public event KeyboardInputSubmitDelegate onSubmit;

		TouchScreenKeyboard keyboard;
		bool wasActivated;
		string originalText;
		string editText;

		static KeyboardInput focusedInput;

		public KeyboardInput FocusedKeyboardInput {
			get {
				return focusedInput;
			}
		}
		void Awake() {
			if (openOnTap) {
				Util.GetEventHandlers(gameObject).onClicked.Add((ti) => {
					if (openOnTap) {
						Open();
						return true;
					} else {
						return false;
					}
				});
			}
		}

		public void Open() {
#if (UNITY_IOS || UNITY_ANDROID || UNITY_WP8) && !UNITY_EDITOR
			if (TouchScreenKeyboard.isSupported) {
				if (keyboard == null && target != null) {
					TouchScreenKeyboard.hideInput = hideInput;
					wasActivated = false;
					if (clearOnFocus) {
						editText = "";
					} else {
						editText = target.text;
					}
					keyboard = TouchScreenKeyboard.Open(editText, keyboardType, autocorrection, multiline, secure, alert, placeholderText);
					Focus();
				}
			}
#else
			if (target != null) {
				if (focusedInput != null) {
					focusedInput.target.text = focusedInput.editText;
				}

				if (clearOnFocus) {
					editText = "";
				} else {
					editText = target.text;
				}
				Focus();
			}
#endif
		}

		void Submit() {
			if (onSubmit != null) {
				onSubmit(this);
			}
		}

		void Focus() {
			focusedInput = this;
			if (target != null) {
				originalText = target.text;
			}
			if (onFocus != null) {
				onFocus(this);
			}
		}

		void Defocus() {
			keyboard = null;
			focusedInput = null;
			if (onDefocus != null) {
				onDefocus(this);
			}
		}

		public string Text {
			get { return editText; }
		}

		void Update() {
#if (UNITY_IOS || UNITY_ANDROID || UNITY_WP8) && !UNITY_EDITOR
			if (keyboard != null) {
				if (keyboard.active) {
					wasActivated = true;
				}

				if (keyboard.done) {
					if (target != null) {
						target.text = keyboard.text;
					}
					Submit();
					Defocus();
				} else if (keyboard.wasCanceled || (wasActivated && !TouchScreenKeyboard.visible)) {
					if (target != null) {
						target.text = originalText;
					}
					Defocus();
				} else {
					if (target != null) {
						target.text = keyboard.text + '|';
					}
				}
			}
#else
			if (focusedInput == this) {
				if (target != null) {
					if (Input.GetKeyDown(KeyCode.Escape)) {
						if (target != null) {
							editText = originalText;
							target.text = originalText;
						}
						Defocus();
					} else if (Input.GetKeyDown(KeyCode.Return) && !Input.GetKey(KeyCode.LeftShift)) {
						if (target != null) {
							target.text = editText;
						}
						Submit();
						Defocus();
					} else {
						for (int i = 0; i < Input.inputString.Length; ++i) {
							if (Input.inputString[i] == '\b') {
								if (editText.Length > 0) {
									editText = editText.Substring(0, editText.Length - 1);
								}
							} else {
								editText += Input.inputString[i];
							}
						}
						target.text = editText + '|';
					}
				}
			}
#endif
		}
	}

}
