﻿using UnityEngine;
using System.Collections;

namespace Chui {

    public class CubeContent : Content  {

        private Volume volume = null;

        public Material material;

        public override void UpdateContent(Widget widget) {
            volume = widget.volume.Copy();
            GenerateMesh(widget);

            MeshRenderer renderer = GetComponent<MeshRenderer>();
            if(renderer == null) {
                renderer = gameObject.AddComponent<MeshRenderer>();
            }

            renderer.material = material;
        }

        private void GenerateMesh(Widget widget) {
            MeshFilter filter = GetComponent<MeshFilter>();
            if(filter == null) {
                filter = gameObject.AddComponent<MeshFilter>();
            }

            Mesh mesh = filter.sharedMesh;
            if(mesh == null) {
                mesh = new Mesh();
            }

            Vector3 s = widget.volume.Size;
            Vector3 p = widget.pivot;

            Vector3[] vertices = new Vector3[]  {
                new Vector3(-s.x*p.x, -s.y*p.y, s.z*(1.0f-p.z)),
                new Vector3(s.x*(1.0f-p.x), -s.y*p.y, s.z*(1.0f-p.z)),
                new Vector3(s.x*(1.0f-p.x), s.y*(1.0f-p.y), s.z*(1.0f-p.z)),
                new Vector3(-s.x*p.x, s.y*(1.0f-p.y), s.z*(1.0f-p.z)),

                new Vector3(-s.x*p.x, -s.y*p.y, -s.z*p.z),
                new Vector3(s.x*(1.0f-p.x), -s.y*p.y, -s.z*p.z),
                new Vector3(s.x*(1.0f-p.x), s.y*(1.0f-p.y), -s.z*p.z),
                new Vector3(-s.x*p.x, s.y*(1.0f-p.y), -s.z*p.z),
            };

            int[] triangles = new int[]  {
                0, 1, 2, 
                2, 3, 0, 
                3, 2, 6, 
                6, 7, 3, 
                7, 6, 5, 
                5, 4, 7, 
                4, 0, 3, 
                3, 7, 4, 
                0, 1, 5, 
                5, 4, 0,
                1, 5, 6, 
                6, 2, 1
            };

            mesh.vertices = vertices;
            mesh.triangles = triangles;

            filter.mesh = mesh;
        }
    }
}