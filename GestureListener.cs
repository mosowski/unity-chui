﻿using UnityEngine;
using System.Collections;

namespace Chui {
	public class GestureListener : MonoBehaviour {
		public EventHandler<DragListener> onTapped = new EventHandler<DragListener>();
		public EventHandler<DragListener> onFreeDragStarted = new EventHandler<DragListener>();
		public EventHandler<DragListener> onFreeDragMoved = new EventHandler<DragListener>();
		public EventHandler<DragListener> onFreeDragEnded = new EventHandler<DragListener>();
		public EventHandler<DragListener> onHorizontalDragStarted = new EventHandler<DragListener>();
		public EventHandler<DragListener> onHorizontalDragMoved = new EventHandler<DragListener>();
		public EventHandler<DragListener> onHorizontalDragEnded = new EventHandler<DragListener>();
		public EventHandler<DragListener> onVerticalDragStarted = new EventHandler<DragListener>();
		public EventHandler<DragListener> onVerticalDragMoved = new EventHandler<DragListener>();
		public EventHandler<DragListener> onVerticalDragEnded = new EventHandler<DragListener>();
	}
}
