﻿using UnityEngine;
using System.Collections;

namespace Chui {

	[ExecuteInEditMode]
	public class ViewportContent : MonoBehaviour {

		public Camera targetCamera;
		public float cameraDepth;

		// Use this for initialization
		void Start () {
		
		}
		
		void Update() {
			Widget widget = GetComponent<Widget>();
			if (widget != null && targetCamera != null) {
				Matrix4x4 rootMtx = Root.GetInstance().transform.worldToLocalMatrix * widget.transform.localToWorldMatrix;
				Vector3 bottomLeft = rootMtx.MultiplyPoint3x4(new Vector3(widget.volume.xMin, widget.volume.yMin, widget.volume.zMin));
				Vector3 topRight = rootMtx.MultiplyPoint3x4(new Vector3(widget.volume.xMax, widget.volume.yMax, widget.volume.zMax));

				targetCamera.depth = cameraDepth;
				targetCamera.rect = new Rect(
					bottomLeft.x / Root.GetInstance().volume.Width + 0.5f,
					bottomLeft.y / Root.GetInstance().volume.Height + 0.5f,
					(topRight.x - bottomLeft.x) / Root.GetInstance().volume.Width,
					(topRight.y - bottomLeft.y) / Root.GetInstance().volume.Height
				);
			}
		}
	}
}
