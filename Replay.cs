﻿using UnityEngine;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	public class Replay : MonoBehaviour {

		public class ReplayEvent {
			public int typeCode;
			public int frame;
			public float time;

			public virtual void Serialize(BinaryWriter writer) {
			}
		}

		public class TouchReplayEvent : ReplayEvent {
			public int id;
			public TouchInfo.Phase type;
			public Vector3 rawPosition;
			public float pressure;

			public static int TypeCode = 1;

			public TouchReplayEvent() {
				typeCode = TypeCode;
			}

			public TouchReplayEvent(BinaryReader reader) {
				id = (int)reader.ReadSByte();
				type = (TouchInfo.Phase)(int)reader.ReadSByte();
				rawPosition.x = reader.ReadSingle();
				rawPosition.y = reader.ReadSingle();
				pressure = reader.ReadSingle();
			}

			override public void Serialize(BinaryWriter writer) {
				writer.Write((sbyte)id);
				writer.Write((sbyte)type);
				writer.Write(rawPosition.x);
				writer.Write(rawPosition.y);
				writer.Write(pressure);
			}
		}

		public class ReplaySettingsReplayEvent : ReplayEvent {
			public string replayId;
			public string userData;
			public float screenWidth;
			public float screenHeight;

			public static int TypeCode = 2;

			public ReplaySettingsReplayEvent() {
			}

			public ReplaySettingsReplayEvent(BinaryReader reader) {
				replayId = reader.ReadString();
				userData = reader.ReadString();
				screenWidth = reader.ReadSingle();
				screenHeight = reader.ReadSingle();
			}

			override public void Serialize(BinaryWriter writer) {
				writer.Write(replayId);
				writer.Write(userData);
				writer.Write(screenWidth);
				writer.Write(screenHeight);
			}
		}

		public class AsyncCallReplayEvent : ReplayEvent {
			public int id;

			public static int TypeCode = 3;

			public AsyncCallReplayEvent() {
				typeCode = TypeCode;
			}

			public AsyncCallReplayEvent(BinaryReader reader) {
				id = reader.ReadInt32();
			}

			override public void Serialize(BinaryWriter writer) {
				writer.Write(id);
			}
		}

		public class SemanticReplayEvent : ReplayEvent {
			public string message;
			public string userData;
			public float realTime;

			public static int TypeCode = 4;

			public SemanticReplayEvent() {
				typeCode = TypeCode;
			}

			public SemanticReplayEvent(BinaryReader reader) {
				message = reader.ReadString();
				userData = reader.ReadString();
				realTime = reader.ReadSingle();
			}

			override public void Serialize(BinaryWriter writer) {
				writer.Write(message);
				writer.Write(userData);
				writer.Write(realTime);
			}
		}

		public class SignalReplayEvent : ReplayEvent {
			public int id;
			public byte[] userData;

			public static int TypeCode = 5;

			public SignalReplayEvent() {
				typeCode = TypeCode;
			}

			public SignalReplayEvent(BinaryReader reader) {
				id = reader.ReadInt32();
				int userDataLength = reader.ReadInt32();
				if (userDataLength != 0) {
					userData = reader.ReadBytes(userDataLength);
				}
			}

			override public void Serialize(BinaryWriter writer) {
				writer.Write(id);
				int userDataLength = userData != null ? userData.Length : 0;
				writer.Write(userDataLength);
				if (userDataLength != 0) {
					writer.Write(userData);
				}
			}
		}

		public class Records {
			public List<ReplayEvent> events = new List<ReplayEvent>();
		}

		public class AsyncCall {
			public AsyncCallback callback;
			public Queue<AsyncCall> queue;
			public int id;
			public bool hasFinished;

			public virtual void Call() {
				if (hasFinished) {
					Debug.LogWarning("<color=red>Danger: AsyncCall.Call " + id + " invoked multiple times.</color>");
				} else {
					hasFinished = true;
				}
			}
		}

		public class RecordedAsyncCall : AsyncCall {
			override public void Call() {
				queue.Enqueue(this);
			}
		}

		class EnqueuedSignal {
			public int id;
			public byte[] userData;
		}

		public Records records = new Records();

		public bool isReplaying;
		public bool isRecording;

		public GameObject replayTouchIndicatorPrefab;

		Dictionary<int, System.Reflection.MethodInfo> serializeMethods = new Dictionary<int, System.Reflection.MethodInfo>();
		Dictionary<int, System.Reflection.ConstructorInfo> constructorMethods = new Dictionary<int, System.Reflection.ConstructorInfo>();

		public delegate AsyncCallReplayEvent AsyncCallback(AsyncCallReplayEvent replayEvent);

		Dictionary<int, AsyncCall> asyncCallMap = new Dictionary<int, AsyncCall>();
		Queue<AsyncCall> asyncCallQueue = new Queue<AsyncCall>();
		Queue<SemanticReplayEvent> semanticQueue = new Queue<SemanticReplayEvent>();
		int asyncCallId = 0;

		public delegate void WriterDelegate(BinaryWriter writer);
		public delegate void SlotDelegate(BinaryReader userDataReader);
		Dictionary<int, SlotDelegate> slotDelegates = new Dictionary<int, SlotDelegate>();
		Queue<EnqueuedSignal> signalsQueue = new Queue<EnqueuedSignal>();

		string replayId;
		string userData;
		int currentFrame;
		int frameCount;
		ReplayEvent stallingEvent;
		bool isInFrame;

		[HideInInspector]
		public float screenWidth;
		[HideInInspector]
		public float screenHeight;

		List<ReplayEvent> consumedEvents = new List<ReplayEvent>();

		public int CurrentFrame {
			get {
				return currentFrame;
			}
		}

		public string ReplayId {
			get {
				return replayId;
			}
		}

		public string UserData {
			get {
				return userData;
			}
		}

		public int FrameCount {
			get {
				return frameCount;
			}
		}
		
		public bool IsFrameStalled {
			get {
				return stallingEvent != null;
			}
		}

		void Awake() {
			AddReplayEventType(typeof(TouchReplayEvent));
			AddReplayEventType(typeof(ReplaySettingsReplayEvent));
			AddReplayEventType(typeof(AsyncCallReplayEvent));
			AddReplayEventType(typeof(SemanticReplayEvent));
			AddReplayEventType(typeof(SignalReplayEvent));
		}

		public void AddReplayEventType(System.Type type) {
			int typeCode = (int)type.GetField("TypeCode", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public).GetValue(null);
			serializeMethods[typeCode] = type.GetMethod("Serialize", new System.Type[] { typeof(BinaryWriter) });
			constructorMethods[typeCode] = type.GetConstructor(new System.Type[] { typeof(BinaryReader) });
		}

		byte[] GetWriterDelegateContent(WriterDelegate writerDelegate) {
			using (var memory = new MemoryStream()) {
				using (var writer = new BinaryWriter(memory)) {
					writerDelegate(writer);
					return memory.ToArray();
				}
			}
		}

		public void SetSlotDelegate(int id, SlotDelegate slotDelegate) {
			slotDelegates[id] = slotDelegate;
		}

		public void RemoveSlotDelegate(int id) {
			slotDelegates.Remove(id);
		}

		public void EmitSignal(int id, WriterDelegate writerDelegate = null) {
			if (!isReplaying) {
				byte[] userData = null;
				if (writerDelegate != null) {
					userData = GetWriterDelegateContent(writerDelegate);
				}
				signalsQueue.Enqueue(new EnqueuedSignal() { id = id, userData = userData });
			}
		}

		public void InitRecording(string replayId, string userData) {
			this.userData = userData;
			this.replayId = replayId;
			isRecording = true;
			isReplaying = false;
			currentFrame = 0;

			// Initial frame
			BeginFrame();

			StoreEvent(new ReplaySettingsReplayEvent() {
				typeCode = ReplaySettingsReplayEvent.TypeCode,
				replayId = replayId,
				userData = userData,
				screenWidth = Screen.width,
				screenHeight = Screen.height
			});

			EndFrame();
		}

		public void InitReplaying() {
			isReplaying = true;
			isRecording = false;

			currentFrame = 0;

			if (records.events.Count > 0) {
				frameCount = records.events.Last().frame + 1;
			} else {
				frameCount = 0;
			}

			// Initial frame
			BeginFrame();

			foreach (var re in EventsForCurrentFrame) {
				if (re is ReplaySettingsReplayEvent) {
					var e = (ReplaySettingsReplayEvent)re;
					replayId = e.replayId;
					userData = e.userData;
					screenWidth = e.screenWidth;
					screenHeight = e.screenHeight;

					if (Mathf.Abs((float)Screen.width/Screen.height - screenWidth / screenHeight) > 0.0001f) {
						Debug.LogWarning("REPLAY WARNING: SCREEN ASPECT RATIO DIFFER TOO MUCH: (" + screenWidth + " x " + screenHeight + ")\nRESULTS MAY VARY.");
					}

					ConsumeEvent(re);
				}
			}

			EndFrame();
		}

		public IEnumerable<ReplayEvent> EventsForCurrentFrame {
			get {
				for (int i = 0; i < records.events.Count;) {
					if (records.events[i] == stallingEvent) {
						break;
					}

					if (records.events[i].frame == currentFrame) {
						yield return records.events[i];
						if (consumedEvents.Contains(records.events[i])) {
							consumedEvents.Remove(records.events[i]);
							records.events.RemoveAt(i);
						} else {
							++i;
						}
					} else {
						break;
					}
				}
			}
		}

		public IEnumerable<TouchReplayEvent> GetTouchEventsForCurrentFrame() {
			foreach (ReplayEvent e in EventsForCurrentFrame) {
				if (e is TouchReplayEvent) {
					yield return (TouchReplayEvent)e;
					ConsumeEvent(e);
				}
			}
		}

		public bool HasEvents {
			get {
				return records.events.Count > 0;
			}
		}

		public void BeginFrame() {
			isInFrame = true;
			stallingEvent = null;

			foreach (ReplayEvent re in EventsForCurrentFrame) {
				if (re is AsyncCallReplayEvent) {
					var e = (AsyncCallReplayEvent)re;
					AsyncCall asyncCall = null;
					if (asyncCallMap.TryGetValue(e.id, out asyncCall)) {
						if (!asyncCall.hasFinished) {
							stallingEvent = re;
							break;
						}
					} else {
						stallingEvent = re;
						break;
					}
				}
			}

			DeterministicStep();
		}

		public void EndFrame() {
			isInFrame = false;

			if (stallingEvent != null) {
				Debug.Log("<color=yellow>Frame stalled. Events remaining: " + EventsForCurrentFrame.Count() + " at frame " + currentFrame + "</color>");
			} else {
				currentFrame++;
			}

		}

		public void ConsumeEvent(ReplayEvent e) {
			if (!isInFrame) {
				Debug.LogWarning("REPLAY WARNING: EVENT CONSUMED OUTSIDE FRAME");
			}
			consumedEvents.Add(e);
		}

		public void StoreEvent(ReplayEvent e) {
			if (!isInFrame) {
				Debug.LogWarning("REPLAY WARNING: EVENT RECORDED OUTSIDE FRAME");
			}

			e.time = Time.time;
			e.frame = currentFrame;
			records.events.Add(e);
		}

		public void StoreEvent(int slot, ReplayEvent e) {
			if (!isInFrame) {
				Debug.LogWarning("REPLAY WARNING: EVENT RECORDED OUTSIDE FRAME");
			}
			e.time = Time.time;
			e.frame = currentFrame;
			records.events[slot] = e;
		}

		public int ReserveEventSlot() {
			int slot = records.events.Count;
			records.events.Add(null);
			return slot;
		}

		public void SerializeEvent(BinaryWriter writer, ReplayEvent e) {
			if (serializeMethods.ContainsKey(e.typeCode)) {
				writer.Write(e.typeCode);
				writer.Write(e.frame);
				writer.Write(e.time);
				serializeMethods[e.typeCode].Invoke(e, new object[] { writer });
			} else {
				throw new System.Exception("REPLAY ERROR: SERIALIZER NOT AVAILABLE FOR TYPE " + e.typeCode);
			}
		}

		public ReplayEvent DeserializeEvent(BinaryReader reader) {
			int typeCode = reader.ReadInt32();
			if (constructorMethods.ContainsKey(typeCode)) {
				int frame = reader.ReadInt32();
				float time = reader.ReadSingle();
				ReplayEvent e = (ReplayEvent)constructorMethods[typeCode].Invoke(new object[] { reader });
				e.typeCode = typeCode;
				e.frame = frame;
				e.time = time;
				return e;
			} else {
				throw new System.Exception("REPLAY ERROR: DESERIALIZER NOT AVAILABLE FOR TYPE " + typeCode);
			}
		}

		public void RecordTouchEvent(TouchInfo.Phase phase, int id, Vector3 rawPosition) {
			StoreEvent(new TouchReplayEvent() {
				type = phase,
				id = id, 
				rawPosition = rawPosition
			});
		}

		public AsyncCall InitAsyncCallRecording(AsyncCallback callback) {
			AsyncCall asyncCall = null;
			if (isReplaying) {
				asyncCall = new AsyncCall();
				asyncCall.id = asyncCallId++;
				asyncCall.callback = callback;
				asyncCallMap[asyncCall.id] = asyncCall;
			} else {
				asyncCall = new RecordedAsyncCall();
				asyncCall.id = asyncCallId++;
				asyncCall.callback = callback;
				asyncCall.queue = asyncCallQueue;
			}
			return asyncCall;
		}

		void ExecuteAsyncCall(AsyncCall asyncCall, AsyncCallReplayEvent e) {
			if (isRecording) {

				int slot = ReserveEventSlot();

				AsyncCallReplayEvent replayEvent = asyncCall.callback(e);

				if (replayEvent == null) {
					replayEvent = new AsyncCallReplayEvent();
				}
				replayEvent.id = asyncCall.id;

				StoreEvent(slot, replayEvent);
			} else {
				asyncCall.callback(e);
			}

		}

		void CallSlotDelegate(int id, byte[] userData) {
			if (isRecording) {
				StoreEvent(new SignalReplayEvent() {
					id = id,
					userData = userData
				});
			}

			if (userData == null) {
				slotDelegates[id](null);
			} else {
				using (var reader = new BinaryReader(new MemoryStream(userData))) {
					slotDelegates[id](reader);
				}
			}
		}

		public void RecordSemanticEvent(string message, string userData = "") {
			if (isRecording) {
				semanticQueue.Enqueue(new SemanticReplayEvent() {
					message = message,
					userData = userData,
					time = Time.realtimeSinceStartup
				});
			}
		}

		void FlushAsyncQueue() {
			while (asyncCallQueue.Count > 0) {
				ExecuteAsyncCall(asyncCallQueue.Dequeue(), null);
			}
		}

		void FlushSignalQueue() {
			while (signalsQueue.Count > 0) {
				var signal = signalsQueue.Dequeue();
				CallSlotDelegate(signal.id, signal.userData);
			}
		}

		void FlushSemanticQueue() {
			while (semanticQueue.Count > 0) {
				var semanticEvent = semanticQueue.Dequeue();
				StoreEvent(semanticEvent);
				Debug.Log(semanticEvent.message);
			}
		}

		public void FinishRecording() {
			FlushSemanticQueue();
		}

		public void DeterministicStep() {
			if (isReplaying) {
				foreach (ReplayEvent re in EventsForCurrentFrame) {
					if (re is AsyncCallReplayEvent) {
						var e = (AsyncCallReplayEvent)re;
						AsyncCall asyncCall = null;
						if (asyncCallMap.TryGetValue(e.id, out asyncCall)) {
							if (asyncCall.hasFinished) {
								ExecuteAsyncCall(asyncCallMap[e.id], e);
								ConsumeEvent(re);
							} else {
								break;
							}
						}
					}

					if (re is SignalReplayEvent) {
						var e = (SignalReplayEvent)re;
						CallSlotDelegate(e.id, e.userData);
						ConsumeEvent(re);
					}

					if (re is SemanticReplayEvent) {
						var e = (SemanticReplayEvent)re;
						Debug.Log(e.message);
						ConsumeEvent(re);
					}
				}
			} else {
				FlushAsyncQueue();
				FlushSignalQueue();
				FlushSemanticQueue();
			}
		}
	}
}
