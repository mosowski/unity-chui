﻿#region Using statements

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#endregion

namespace Chui
{

    public abstract class VirtualElementLayout : MonoBehaviour 
    {

        public class ElementData
        {
            public Vector3 localPosition;
            public Volume localVolume = new Volume();

            public void RecalculateVolume(Vector3 size)
            {
                localVolume.xMin = localPosition.x - 0.5f * size.x;
                localVolume.xMax = localPosition.x + 0.5f * size.x;
                localVolume.yMin = localPosition.y - 0.5f * size.y;
                localVolume.yMax = localPosition.y + 0.5f * size.y;
            }
        }

        public class Data
        {
            public bool overrideWidth = false;
            public float contentWidth = 0.0f;

            public bool overrideHeight = false;
            public float contentHeight = 0.0f;
            public Dictionary<VirtualElement, ElementData> elements;
        }

        public abstract Data GenerateLayout(VirtualElementGroup group);

    }

}