﻿using UnityEngine;
using System;
using System.Collections;

namespace Chui {
    public class VirtualElement {

        public delegate void CustomInitializeDelegate(VirtualElementGroup group, VirtualElement virtualElement, GameObject gameObject, Chui.Widget widget);
        public delegate Vector3 GetSizeDelegate(VirtualElement virtualElement);

        private VirtualElementGroup group;
        private GameObject prefab;
        private GetSizeDelegate getSizeAlgorithm;

        private CustomInitializeDelegate customInitialize;
        private CustomInitializeDelegate customRelease;
        private Hashtable customData = new Hashtable();

        public VirtualElement(VirtualElementGroup group, GameObject prefab) {
            this.group = group;
            this.prefab = prefab;
            this.getSizeAlgorithm = group.GetSizeFromInstance;
        }

        public VirtualElementGroup Group {
            get { return group; }
        }

        public GameObject Prefab {
            get { return prefab; }
        }

        public GetSizeDelegate GetSizeAlgorithm {
            get { return getSizeAlgorithm; }
            set { getSizeAlgorithm = value; }
        }

        public Hashtable CustomData {
            get { return customData; }
        }

        public CustomInitializeDelegate CustomInitialize {
            get { return customInitialize; }
            set { customInitialize = value; }
        }         

        public CustomInitializeDelegate CustomRelease {
            get { return customRelease; }
            set { customRelease = value; }
        }

        public Vector3 GetSize() {
            return GetSizeAlgorithm(this);
        }
    }
}
