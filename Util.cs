using System;
using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	// Do not use it as Dictionary<,> key type, C# compiler will booom segfault!
	[System.Serializable]
	public struct Pair<T, U> {
		public T st;
		public U nd;

		public Pair(T s, U n) {
			st = s;
			nd = n;
		}
	}

	[System.Serializable]
	public class SerializableVector3 {
		public float x;
		public float y;
		public float z;

		public SerializableVector3(float x = 0.0f, float y = 0.0f, float z = 0.0f) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		static public implicit operator Vector3(SerializableVector3 v) {
			return new Vector3(v.x, v.y, v.z);
		}

		static public implicit operator SerializableVector3(Vector3 v) {
			return new SerializableVector3(v.x, v.y, v.z);
		}
	}
	
	public interface ISerializableDictionaryItem<K> {
		K GetDictionaryKey();
	}

	//NOTE:
	// You may ask: Why the duck one would want use list of delegates instead 
	// of simply using C# events? The answer is because I needed track the
	// handlers return value.
	//
	public class EventHandler<T> {
		public delegate bool EventHandlerDelegate(T arg);

		List<EventHandlerDelegate> invocationList;

		public void Add(EventHandlerDelegate d) {
			if (invocationList == null) {
				invocationList = new List<EventHandlerDelegate>();
			}
			invocationList.Add(d);
		}

		public void Remove(EventHandlerDelegate d) {
			if (invocationList != null) {
				invocationList.Remove(d);
			}
		}
		
		public void Set(EventHandlerDelegate d) {
			Clear();
			Add(d);
		}

		public bool Invoke(T arg) {
			bool result = false;
			if (invocationList != null) {
				for (int i = 0; i < invocationList.Count; ++i) {
					result = result || invocationList[i](arg);
				}
			}
			return result;
		}
		
		public void Clear() {
			if (invocationList != null) {
				invocationList.Clear();
			}
		}
	}

	public static class Pools {
		static Dictionary<int, List<Mesh>> meshesBySize = new Dictionary<int, List<Mesh>>();

		public static bool RestoreMeshBySize(int numVertices, out Mesh mesh) {
			List<Mesh> pool = null;
			if (meshesBySize.TryGetValue(numVertices, out pool)) {
				if (pool.Count > 0) {
					mesh = pool[pool.Count - 1];
					pool.RemoveAt(pool.Count - 1);
					return mesh != null;
				}
			}
			mesh = null;
			return false;
		}

		public static void StoreMesh(Mesh mesh) {
			List<Mesh> pool = null;
			if (mesh.vertices != null && mesh.vertexCount != 0) {
				if (!meshesBySize.TryGetValue(mesh.vertexCount, out pool)) {
					meshesBySize[mesh.vertexCount] = pool = new List<Mesh>();
				}
				pool.Add(mesh);
			}
		}
	}
	
	public static class Caches {
		
		struct MaterialShaderPair {
			public Material material;
			public Shader shader;
			
			public override int GetHashCode() {
				return material.GetHashCode() * shader.GetHashCode();
			}
		}
		static Dictionary<MaterialShaderPair, Material> materialsByShader = new Dictionary<MaterialShaderPair, Material>();

		public static Material GetMaterialWithShader(Material baseMaterial, Shader shader) {
			Material material = null;
			if (!materialsByShader.TryGetValue(new MaterialShaderPair() { material = baseMaterial, shader = shader }, out material)) {
				material = GameObject.Instantiate(baseMaterial);
				material.shader = shader;
				materialsByShader[new MaterialShaderPair() { material = baseMaterial, shader = shader }] = material;
			}
			return material;
		}	
	}

	public static class TextTagInjector {
		public delegate string TagInjectorDelegate(List<string> args);

		private static Dictionary<string, TagInjectorDelegate> map = new Dictionary<string, TagInjectorDelegate>();

		public static void SetMapping(string tagId, TagInjectorDelegate callback) {
			map[tagId] = callback;
		}

		public static string GetInjectedText(string id, List<string> args) {
			TagInjectorDelegate callback = null;
			if (map.TryGetValue(id, out callback)) {
				return callback(args);
			} else {
				return "(missing callback: " + id + ")";
			}
		}
	}

	public static class Shaders {

		public enum Blending {
			Opaque,
			Transparent,
			Additive
		}

		public struct Family {
			public Blending blending;

			override public int GetHashCode() {
				return blending.GetHashCode();
			}
		}

		public delegate Shader ShaderSetterDelegate(Shader shader);

		static Dictionary<string, Family> families = new Dictionary<string, Family>() {
			{ "Chui/Opaque", new Family() { blending = Blending.Opaque } },
			{ "Chui/Transparent", new Family() { blending = Blending.Transparent } },
			{ "Chui/Additive", new Family() { blending = Blending.Additive } },
			{ "Chui/Desaturate Opaque", new Family() { blending = Blending.Opaque} },
			{ "Chui/Desaturate Transparent", new Family() { blending = Blending.Transparent} },
			{ "Chui/Desaturate Additive", new Family() { blending = Blending.Additive } },
			{ "Chui/Colored Opaque", new Family() { blending = Blending.Opaque} },
			{ "Chui/Colored Transparent", new Family() { blending = Blending.Transparent} },
			{ "Chui/Circle Transparent", new Family() { blending = Blending.Transparent} },
		};

		static string BlendingSuffix(Blending b) {
			return b.ToString();
		}

		static string FamilySuffix(Family f) {
			return BlendingSuffix(f.blending);
		}

		static Family GetShaderFamily(Shader shader) {
			Family family;
			if (!families.TryGetValue(shader.name, out family)) {
				Debug.LogWarning("[Chui.Shaders] Shader family not known for: " + shader.name);
				return new Family() { blending = Blending.Transparent };
			} else {
				return family;
			}
		}

		static public Shader Desaturate(Shader shader) {
			return Shader.Find("Chui/Desaturate " + FamilySuffix(GetShaderFamily(shader)));
		}

		static public Shader Standard(Shader shader) {
			return Shader.Find("Chui/" + FamilySuffix(GetShaderFamily(shader)));
		}

		static public void RegisterCustomShader(Shader shader, Family family) {
			families[shader.name] = family;
		}
	}

	public static class Util {

		static public DragListener GetDragListener(GameObject go) {
			DragListener dl = go.GetComponent<DragListener>();
			if (dl == null) {
				dl = go.AddComponent<DragListener>();
			}
			return dl;
		}

		static public GestureListener GetGestureListener(GameObject go) {
			GestureListener dl = go.GetComponent<GestureListener>();
			if (dl == null) {
				dl = go.AddComponent<GestureListener>();
			}
			return dl;
		}


		static public EventListener GetEventListener(GameObject go) {
			EventListener el = go.GetComponent<EventListener>();
			if (el == null) {
				el = go.AddComponent<EventListener>();
			}
			return el;
		}

		static public EventListener.Handlers GetEventHandlers(GameObject go) {
			return GetBubblingHandlers(go);
		}

		static public EventListener.Handlers GetBubblingHandlers(GameObject go) {
			return GetEventListener(go).bubbling;
		}

		static public EventListener.Handlers GetCapturingHandlers(GameObject go) {
			return GetEventListener(go).capturing;
		}

		static public Vector3 GetScreenPos(GameObject go) {
			return GetScreenPos(go.transform.position);
		}

		static public Vector3 GetScreenPos(Vector3 worldPos) {
			return Root.GetInstance().MainCamera.WorldToScreenPoint(worldPos);
		}

		static public Vector3 GetUnitPos(GameObject go) {
			return GetUnitPos(go.transform.position);
		}

		static public Vector3 GetUnitPos(Vector3 worldPos) {
			return Root.GetInstance().transform.InverseTransformPoint(worldPos);
		}

		static public Vector3 GetFlatUnitPos(GameObject go) {
			return GetFlatUnitPos(go.transform.position);
		}

		static public Vector3 GetFlatUnitPos(Vector3 worldPos) {
			Vector3 p = GetUnitPos(worldPos);
			p.z = 0;
			return p;
		}

		static public void SerializeDictionary<K, V>(Dictionary<K, V> d, out List<V> l) where V : ISerializableDictionaryItem<K> {
			l = new List<V>();
			foreach (KeyValuePair<K, V> item in d) {
				l.Add(item.Value);
			}
		}

		static public void SerializeDictionary<K, V>(Dictionary<K, List<V>> d, out List<V> l) where V : ISerializableDictionaryItem<K> {
			l = new List<V>();
			foreach (KeyValuePair<K, List<V>> item in d) {
				foreach (var v in item.Value) {
					l.Add(v);
				}
			}
		}

		/// <summary>
		/// Convert list to dictionary </summary>
		/// <param name="l">List of values to convert</param>
		/// <param name="d">Output dictionary</param>
		static public void DeserializeDictionary<K, V>(List<V> l, out Dictionary<K, V> d) where V : ISerializableDictionaryItem<K> {
			d = new Dictionary<K, V>();
			for (int i = 0; i < l.Count; ++i) {
				d[l[i].GetDictionaryKey()] = l[i];
			}
		}

		/// <summary>
		/// Convert list to dictionary of lists</summary>
		/// <param name="l">List of values to convert</param>
		/// <param name="d">Output dictionary</param>
		static public void DeserializeDictionary<K, V>(List<V> l, out Dictionary<K, List<V>> d) where V : ISerializableDictionaryItem<K> {
			d = new Dictionary<K, List<V>>();
			for (int i = 0; i < l.Count; ++i) {
				K key = l[i].GetDictionaryKey();
				List<V> vs = null;
				if (!d.TryGetValue(key, out vs)) {
					vs = new List<V>();
					d[key] = vs;
				}
				vs.Add(l[i]);
			}
		}

		static public void CopyPixels(Color32[] src, int srcWidth, int srcX, int srcY, Color32[] dest, int destWidth, int destX, int destY, int width, int height, bool border = false) {
			int srcHeight = src.Length / srcWidth;
			int destHeight = dest.Length / destWidth;
			for (int j = 0; j < height; ++j) {
				for (int i = 0; i < width; ++i) {
					Color32 s = src[(srcHeight - (j + srcY + 1)) * srcWidth + i + srcX];
					dest[(destHeight - (j + destY + 1)) * destWidth + i + destX] = s;
				}
			}
		}
		static public T GetChildComponent<T>(Component c, string name) where T : Component {
			GameObject ch = GetChild(c.gameObject, name);
			if (ch != null) {
				return ch.GetComponent<T>();
			} else {
				return null;
			}
		}

		static public T GetChildComponent<T>(GameObject go, string name) where T : Component {
			GameObject ch = GetChild(go, name);
			if (ch != null) {
				return ch.GetComponent<T>();
			} else {
				return null;
			}
		}

		static public GameObject GetChild(Component c, string name) {
			return GetChild(c.gameObject, name);
		}

		static public GameObject GetChild(GameObject go, string name) {
			if (go.name == name) {
				return go;
			}

			Transform t = go.transform;
			for (int i = 0; i < t.childCount; ++i) {
				GameObject ch = GetChild(t.GetChild(i).gameObject, name);
				if (ch != null) {
					return ch;
				}
			}
			return null;
		}

		/// <summary>
		/// Converts color from integer to Color32 without alpha</summary>
		static public Color32 IntToColor32(int intColor) {
			return new Color32(
				(byte)((intColor >> 16) & 0xFF),
				(byte)((intColor >> 8) & 0xFF),
				(byte)((intColor) & 0xFF),
				255
			);
		}
		
		/// <summary>
		/// Sets material based on shader to object and all children.
		/// QuadContent and MeshRenderer are affected.</summary>
		/// <param name="go">Top game object</param>
		/// <param name="shaderSetter">Function to convert shader</param>
		static public void SetShaderRecursive(GameObject go, Shaders.ShaderSetterDelegate shaderSetter) {
			bool chuiContent = false;
			QuadContent qc = go.GetComponent<QuadContent>();
			if (qc != null) {
				chuiContent = true;
				qc.material = Caches.GetMaterialWithShader(qc.material, shaderSetter(qc.material.shader));
			}
			TextContent tc = go.GetComponent<TextContent>();
			if (tc != null) {
				chuiContent = true;
				//TODO: TextContent material overriding
				//tc.material = Caches.GetMaterialWithShader(tc.material, shaderSetter(tc.material.shader));
			}

			if (!chuiContent) {
				MeshRenderer renderer = go.GetComponent<MeshRenderer>();
				if (renderer != null) {
					renderer.material = Caches.GetMaterialWithShader(renderer.material, shaderSetter(renderer.material.shader));
				}
			}
			
			Transform t = go.transform;
			for (int i = 0; i < t.childCount; ++i) {
				SetShaderRecursive(t.GetChild(i).gameObject, shaderSetter);
			}
		}

		static public GameObject CreateAnchoredWidget(GameObject parent, float leftRelative = 0, float bottomRelative = 0, float rightRelative = 1, float topRelative = 1) {
			GameObject go = new GameObject();
			go.transform.SetParent(parent.transform, false);
			AnchoredWidget widget = go.AddComponent<AnchoredWidget>();
			widget.leftAnchor.relative = leftRelative;
			widget.leftAnchor.offset = 0;
			widget.bottomAnchor.relative = bottomRelative;
			widget.bottomAnchor.offset = 0;
			widget.rightAnchor.relative = rightRelative;
			widget.rightAnchor.offset = 0;
			widget.topAnchor.relative = topRelative;
			widget.topAnchor.offset = 0;
			return go;
		}

		static public FixedWidget ConvertToFixedWidget(Widget widget) {
			var fixedWidget = widget.gameObject.AddComponent<FixedWidget>();
			var localPosition = widget.transform.localPosition;
			fixedWidget.pivot = widget.pivot;
			fixedWidget.Width = widget.Width;
			fixedWidget.Height = widget.Height;
			fixedWidget.Depth = widget.Depth;
			fixedWidget.color = widget.color;
			fixedWidget.isColliderEnabled = widget.isColliderEnabled;
			fixedWidget.isColliderAutoResizeEnabled = widget.isColliderAutoResizeEnabled;
			fixedWidget.isStatic = widget.isStatic;
			fixedWidget.Revalidate();
			fixedWidget.transform.localPosition = localPosition;
			return fixedWidget;
		}

		static public void ConvertHierarchyToFixedWidgets(Transform t) {
			var anchored = t.GetComponent<AnchoredWidget>();
			if (anchored != null) {
				ConvertToFixedWidget(anchored);
#if UNITY_EDITOR
				if (Application.isPlaying) {
					GameObject.Destroy(anchored);
				} else {
					GameObject.DestroyImmediate(anchored);
				}
#else
				GameObject.Destroy(anchored);
#endif
			}

			for (int i = 0; i < t.childCount; ++i) {
				ConvertHierarchyToFixedWidgets(t.GetChild(i));
			}
		}

		static public void SetHierarchyStatic(GameObject t, bool s, bool includeThis = true) {
			SetHierarchyStatic(t.transform, s, includeThis);
		}

		static public void SetHierarchyStatic(Transform t, bool s, bool includeThis = true) {
			if (includeThis) {
				Widget w = t.GetComponent<Widget>();
				if (w != null) {
					w.isStatic = s;
				}
			}
			for (int i = 0; i < t.childCount; ++i) {
				SetHierarchyStatic(t.GetChild(i), s);
			}
		}

		static public void DestroyDirectChildren(GameObject gameObject) {
			List<Transform> children = new List<Transform>();
			for (int i = 0; i < gameObject.transform.childCount; ++i) {
				children.Add(gameObject.transform.GetChild(i));
			}
			for (int i = 0; i < children.Count; ++i) {
				children[i].transform.SetParent(null, false);
				GameObject.Destroy(children[i].gameObject);
			}
		}

		static public void DestroyImmediateDirectChildren(GameObject gameObject) {
			List<Transform> children = new List<Transform>();
			for (int i = 0; i < gameObject.transform.childCount; ++i) {
				children.Add(gameObject.transform.GetChild(i));
			}
			for (int i = 0; i < children.Count; ++i) {
				children[i].transform.SetParent(null, false);
				GameObject.DestroyImmediate(children[i].gameObject);
			}
		}

		/// <summary>
		/// Setups typical handlers for long touch (and pressure) and click on scroll panel list item.</summary>
		/// <param name="item">Clickable item (with collider)</param>
		/// <param name="scrollPanel">Scroll Panel which items belongs to</param>
		/// <param name="onClick">Click handler</param>
		/// <param name="onLongTouch">Long Touch handler</param>
		/// <param name="longTouchDistance">Max move distance for long touch</param>
		/// <param name="longTouchTime">Min long touch time</param>
		/// <param name="longTouchPressure">Min long pressure</param>
		static public void SetupLongTouchAndClickScrollPanelItemHandlers(GameObject item, GameObject scrollPanel, Action onClick, Action onLongTouch, float longTouchDistance, float longTouchTime, float longTouchPressure) {
			bool isLong = false;
			TouchInfo.UpdateHandler onTouchUpdate;
			onTouchUpdate = (ti) => {
				if (ti.phase == TouchInfo.Phase.End || scrollPanel.GetComponent<Scroll>().IsDragging || !Root.GetInstance().CheckHit(ti, item)) {
					ti.onUpdate -= onTouchUpdate;
					return;
				}
				if (ti.IsLongTouch(longTouchDistance, longTouchTime) || ti.pressure > longTouchPressure) {
					isLong = true;
					scrollPanel.GetComponent<Scroll>().DiscardDrag();
					scrollPanel.GetComponent<Scroll>().StopMovement();
					ti.onUpdate -= onTouchUpdate;

					onLongTouch();
				}
			};

			Util.GetEventHandlers(item).onTouchBegan.Set((ti) =>  {
				isLong = false;
				ti.onUpdate += onTouchUpdate; 
				return true;
			});

			Util.GetEventHandlers(item).onClicked.Set((ti) => {
				if (isLong) {
					isLong = false;
					return true;
				}

				if (Root.GetInstance().CheckHit(ti, scrollPanel)) {
					onClick();
				}
				return true;
			});
		}

	}
}
