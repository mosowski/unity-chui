﻿using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Chui {
	[ExecuteInEditMode]
	public class SpriteQuadContent : QuadContent {
		[System.Serializable]
		public class Settings {
			public Shader defaultShader;
		}

		private static int mainTexPropertyId = -1;
		private static int colorPropertyId = -1;

		private static Material defaultMaterial;
		private static Dictionary<Shader, Material> materials = new Dictionary<Shader, Material>();

		[SerializeField]
		private Sprite sprite;
		[SerializeField]
		private float scale9SizeModifier = 1.0f;

		private Sprite last_sprite;
		private float last_scale9Modifier = 1.0f;
		private MaterialPropertyBlock materialPropertyBlock;

		private static int MainTexPropertyId {
			get {
				if (mainTexPropertyId == -1) {
					mainTexPropertyId = Shader.PropertyToID("_MainTex");
				}
				return mainTexPropertyId;
			}
		}

		private static int ColorPropertyId {
			get {
				if (colorPropertyId == -1) {
					colorPropertyId = Shader.PropertyToID("_Color");
				}
				return colorPropertyId;
			}
		}

		public Sprite Sprite {
			get {
				return sprite;
			} set {
				if (sprite != value) {
					sprite = value;
					Revalidate();
				}
			}
		}

		public override void UpdateContent(Widget widget) {
			if (sprite != last_sprite) {
				last_sprite = sprite;
				hasChanged = true;
			}
			if (last_scale9Modifier != scale9SizeModifier) {
				last_scale9Modifier = scale9SizeModifier;
				hasChanged = true;
			}
			base.UpdateContent(widget);
		}

		public override void Revalidate() {
			hasChanged = true;
			base.Revalidate();
		}

		protected override void PrepareMesh() {
			base.PrepareMesh();
			if (sprite != null) {
				MaterialPropertyBlock.SetTexture(MainTexPropertyId, sprite.texture);
			}
			MaterialPropertyBlock.SetColor(ColorPropertyId, derivedColor);
			meshRenderer.SetPropertyBlock(MaterialPropertyBlock);
		}

		protected override void BuildGeometry() {
			if (sprite != null) {
				UpdateBorder();
				Vector4 outerUV = UnityEngine.Sprites.DataUtility.GetOuterUV(sprite);
				uvRect.x = outerUV.x;
				uvRect.y = outerUV.y;
				uvRect.width = outerUV.z - outerUV.x;
				uvRect.height = outerUV.w - outerUV.y;
				if (geometryMode == QuadGeometryMode.Scale9) {
					Vector4 innerUV = UnityEngine.Sprites.DataUtility.GetInnerUV(sprite);
					borderSize.uvLeft = Mathf.Clamp01(innerUV.x - outerUV.x) / uvRect.width;
					borderSize.uvBottom = Mathf.Clamp01(innerUV.y - outerUV.y) / uvRect.height;
					borderSize.uvRight = Mathf.Clamp01(outerUV.z - innerUV.z) / uvRect.width;
					borderSize.uvTop = Mathf.Clamp01(outerUV.w - innerUV.w) / uvRect.height;
					borderSize.realLeft = sprite.border.x * sprite.pixelsPerUnit * scale9SizeModifier / 100;
					borderSize.realBottom = sprite.border.y * sprite.pixelsPerUnit * scale9SizeModifier / 100;
					borderSize.realRight = sprite.border.z * sprite.pixelsPerUnit * scale9SizeModifier / 100;
					borderSize.realTop = sprite.border.w * sprite.pixelsPerUnit * scale9SizeModifier / 100;
				}
			}
			base.BuildGeometry();
		}

		private void UpdateBorder() {
			border.x = sprite.textureRectOffset.x;
			border.y = sprite.textureRectOffset.y;
			border.z = sprite.rect.width - sprite.textureRect.width - sprite.textureRectOffset.x;
			border.w = sprite.rect.height - sprite.textureRect.height - sprite.textureRectOffset.y;
		}

		protected override Shader CurrentShader {
			get {
				return base.CurrentShader;
			} set {
				Material material;
				if (!materials.TryGetValue(value, out material)) {
					material = materials[value] = new Material(value);
				}
				Material = material;
			}
		}

		protected override float TextureWidth {
			get {
				return sprite != null ? sprite.rect.width : widget.Width;
			}
		}

		protected override float TextureHeight {
			get {
				return sprite != null ? sprite.rect.height : widget.Width;
			}
		}

		private void Awake() {
			if (material == null) {
				material = DefaultMaterial;
			}
		}

		public Material DefaultMaterial {
			get {
				if (defaultMaterial == null) {
					defaultMaterial = new Material(Root.GetInstance().spriteQuadContentSettings.defaultShader);
				}
				return defaultMaterial;
			}
		}

		private MaterialPropertyBlock MaterialPropertyBlock {
			get {
				if (materialPropertyBlock == null) {
					materialPropertyBlock = new MaterialPropertyBlock();
				}
				return materialPropertyBlock;
			}
		}
	}
}
