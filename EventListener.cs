﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	public class EventListener : MonoBehaviour {

		public class Handlers {
			public EventHandler<TouchInfo> onTouchBegan = new EventHandler<TouchInfo>();
			public EventHandler<TouchInfo> onTouchMoved = new EventHandler<TouchInfo>();
			public EventHandler<TouchInfo> onTouchEnded = new EventHandler<TouchInfo>();
			public EventHandler<TouchInfo> onPressed = new EventHandler<TouchInfo>();
			public EventHandler<TouchInfo> onReleased = new EventHandler<TouchInfo>();
			public EventHandler<TouchInfo> onClicked = new EventHandler<TouchInfo>();

			public List<TouchInfo> capturedTouches = new List<TouchInfo>();
			public List<TouchInfo> pressedTouches = new List<TouchInfo>();

			public void UpdateTouch(TouchInfo ti) {
				if (ti.phase == TouchInfo.Phase.End) {
					if (pressedTouches.Contains(ti)) {
						onReleased.Invoke(ti);
						pressedTouches.Remove(ti);
					}
					capturedTouches.Remove(ti);
					ti.onUpdate -= UpdateTouch;
				}
			}
		}

		public Handlers bubbling = new Handlers();
		public Handlers capturing = new Handlers();
	}
}
