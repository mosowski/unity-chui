﻿Shader "Chui/Transparent Mask1" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
	}

	SubShader {
		LOD 200

		Stencil {
			Ref 1
			Comp Equal
		}

		Tags {
			"Queue" = "Transparent"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite Off
			ZTest Less
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex basicVertex
			#pragma fragment basicFragment

			#include "UnityCG.cginc"

			sampler2D _MainTex;

			#include "ChuiIncludeCG.cginc"

			ENDCG
		}
	}
}

