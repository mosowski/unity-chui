﻿Shader "Chui/GradientMap Opaque Diffuse" 
{
    Properties 
    {
		_MainTex ("Base (RGB)", 2D) = "black" {}
		_GradTex ("Gradient (RGB)", 2D) = "black" {}
    }

    SubShader 
    {
		Tags { "RenderType"="Geometry" }
		LOD 200
		Cull Back
		ZWrite On
		Blend One Zero

	    CGPROGRAM
	    #pragma surface surf Lambert

	    sampler2D _MainTex;
	    sampler2D _GradTex;

	    struct Input 
	    {
		    float2 uv_MainTex;
	    };

	    void surf (Input IN, inout SurfaceOutput o) 
	    {
		    fixed4 s = tex2D(_MainTex, IN.uv_MainTex);
		    o.Albedo = tex2D(_GradTex, half2(s.x, 0)).rgb;
		    o.Alpha = 1;
	    }
	    ENDCG
	}

Fallback "VertexLit"
}
