﻿#define CHUI_INCLUDE_CG

struct basicVertexInput {
	float4 vertex : POSITION;
	half4 color : COLOR;
	float2 uv : TEXCOORD0;
};

struct basicFragmentInput {
	float4 vertex : POSITION;
	half4 color : COLOR;
	float2 uv : TEXCOORD0;
};

basicFragmentInput basicVertex(basicVertexInput i) {
	basicFragmentInput o;
	o.vertex = mul(UNITY_MATRIX_MVP, i.vertex);
	o.color = i.color;
	o.uv = i.uv;

	return o;
}

half4 basicFragment(basicFragmentInput i) : COLOR {
	return tex2D(_MainTex, i.uv) * i.color;
}

half4 desaturate(half4 color) {
	half d = dot(color.xyz, half3(0.22, 0.707, 0.071));
	return half4(d, d, d, color.a);
}


