﻿Shader "Chui/Distance Transparent" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
		_GradientTex ("Gradient (RGB)", 2D) = "black" {}
		_Params("Threshold, Blur, Gradient size", Vector) = (0.5, 0.1, 1.0, 0.0)
	}

	SubShader {
		LOD 200

		Tags {
			"Queue" = "Transparent"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite Off
			ZTest Less
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex basicVertex
			#pragma fragment distanceFragment

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _GradientTex;
			half4 _Params;

			#include "ChuiIncludeCG.cginc"

			half4 distanceFragment(basicFragmentInput i) : COLOR {
				half blur = _Params.y * 1500.0 / _ScreenParams.x;
				half d = tex2D(_MainTex, i.uv).x - (_Params.x - blur * 0.5);
				half blend = saturate(d / blur);
				half gd = saturate(d / _Params.z );
				half4 color = tex2D(_GradientTex, half2(gd, 0.0));
				color.a *= blend;
				return color * i.color;
			}

			ENDCG
		}
	}
}

