﻿Shader "Chui/Distance Shadow Transparent" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
		_GradientTex ("Gradient (RGBA)", 2D) = "black" {}
		_ShadowTex ("Shadow (RGBA)", 2D) = "black" {}
		_ShadowParams("U Offset, V Offset, Threshold, Blur", Vector) = (0.1, 0.1, 0.5, 0.1)
		_Params("Threshold, Blur, Gradient size", Vector) = (0.5, 0.1, 1.0, 0.0)
	}

	SubShader {
		LOD 200

		Tags {
			"Queue" = "Transparent"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite Off
			ZTest Less
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex basicVertex
			#pragma fragment distanceFragment

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _GradientTex;
			sampler2D _ShadowTex;
			half4 _ShadowParams;
			half4 _Params;

			#include "ChuiIncludeCG.cginc"

			half4 distanceFragment(basicFragmentInput i) : COLOR {
				half blur = _Params.y * 1500.0 / _ScreenParams.x;

				half d = tex2D(_MainTex, i.uv).x - (_Params.x - blur * 0.5);
				half blend = min(max((d / blur), 0.0), 1.0);
				half gd = min(max((d / _Params.z ), 0.0), 1.0);
				half4 color = tex2D(_GradientTex, half2(gd, 0.0));
				color.a *= blend;

				half shadowDist = tex2D(_MainTex, i.uv - _ShadowParams.xy).x - (_ShadowParams.z - _ShadowParams.w * 0.5);
				shadowDist = min(max(shadowDist, 0.0), _ShadowParams.w);
				half shadowBlend = shadowDist / _ShadowParams.w;
				half4 shadowColor = tex2D(_ShadowTex, half2(shadowBlend, 0.5));

				half4 finalColor = half4(color.xyz * color.a + shadowColor.xyz * (1.0 - color.a), max(color.a, shadowColor.a));

				return finalColor * i.color;
			}

			ENDCG
		}
	}
}

