﻿Shader "Chui/Mask1" {
	Properties {
	}

	SubShader {
		LOD 200
		ColorMask 0

		Stencil {
			Ref 1
			Comp Always
			Pass Replace
		}

		Tags {
			"Queue" = "Geometry-1"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite Off
			ZTest Less

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct vertexInput {
				float4 vertex : POSITION;
			};

			struct fragmentInput {
				float4 vertex : POSITION;
			};

			fragmentInput vert (vertexInput i) {
				fragmentInput o;
				o.vertex = mul(UNITY_MATRIX_MVP, i.vertex);
	
				return o;
			}

			half4 frag (fragmentInput i) : COLOR {
				return half4(0,0,1,1);
			}
			ENDCG
		}
	}
}

