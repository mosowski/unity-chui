﻿Shader "Chui/Opaque" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
	}

	SubShader {
		LOD 200

		Tags {
			"Queue" = "Geometry"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite On
			ZTest Less
			Blend One Zero

			CGPROGRAM
			#include "UnityCG.cginc"

			#pragma vertex basicVertex
			#pragma fragment basicFragment

			sampler2D _MainTex;

			#include "ChuiIncludeCG.cginc"

			ENDCG
		}
	}
}

