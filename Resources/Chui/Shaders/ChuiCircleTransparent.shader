﻿Shader "Chui/Circle Transparent" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
		_Params("X, Y, Radius, Edge", Vector) = (0.5, 0.5, 0.49, 0.01)
	}

	SubShader {
		LOD 200

		Tags {
			"Queue" = "Transparent"
		}
		
		Pass {
			Cull Off
			Lighting Off
			ZWrite Off
			ZTest Less
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex basicVertex
			#pragma fragment circleFragment

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _Params;

			#include "ChuiIncludeCG.cginc"

			half4 circleFragment(basicFragmentInput i) : COLOR {
				half4 color = tex2D(_MainTex, i.uv);
				float dist = length(i.uv - _Params.xy);
				float edge = _Params.z - _Params.w;
				float mask = saturate(1.0 - (dist - edge) / _Params.w);
				color.a *= mask;
				return color * i.color;
			}


			ENDCG
		}
	}
}

