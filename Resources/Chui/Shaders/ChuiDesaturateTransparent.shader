﻿Shader "Chui/Desaturate Transparent" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
		_Params ("R, G, B, Weight", Vector) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader {
		LOD 200

		Tags {
			"Queue" = "Transparent"
		}
		
		Pass {
			Cull Off
			Lighting Off
			ZWrite Off
			ZTest Less
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#include "UnityCG.cginc"

			#pragma vertex basicVertex
			#pragma fragment desaturateFragment

			sampler2D _MainTex;
			float4 _Params;

			#include "ChuiIncludeCG.cginc"
			
			half4 desaturateFragment(basicFragmentInput i) : COLOR {
				half4 baseColor = tex2D(_MainTex, i.uv);
				half4 desaturatedColor = desaturate(baseColor) * half4(_Params.xyz, 1.0);
				return lerp(baseColor, desaturatedColor, _Params.w) * i.color;
			}

			ENDCG
		}
	}
}

