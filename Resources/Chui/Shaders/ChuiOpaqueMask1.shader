﻿Shader "Chui/Opaque Mask1" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
	}

	SubShader {
		LOD 200

		Stencil {
			Ref 1
			Comp Equal
		}

		Tags {
			"Queue" = "Geometry"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite On
			ZTest Less
			Blend One Zero

			CGPROGRAM
			#include "UnityCG.cginc"

			#pragma vertex basicVertex
			#pragma fragment basicFragment

			sampler2D _MainTex;

			#include "ChuiIncludeCG.cginc"

			ENDCG
		}
	}
}

