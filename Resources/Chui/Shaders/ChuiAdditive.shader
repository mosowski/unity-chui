﻿Shader "Chui/Additive" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
	}

	SubShader {
		LOD 200

		Tags {
			"Queue" = "Transparent"
		}
		
		Pass {
			Cull Off
			Lighting Off
			ZWrite Off
			ZTest Less
			Blend One One

			CGPROGRAM
			#pragma vertex basicVertex
			#pragma fragment basicFragment

			#include "UnityCG.cginc"

			sampler2D _MainTex;

			#include "ChuiIncludeCG.cginc"

			ENDCG
		}
	}
}

