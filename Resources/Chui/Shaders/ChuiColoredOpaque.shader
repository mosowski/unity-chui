﻿Shader "Chui/Colored Opaque" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "black" {}
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader {
		LOD 200

		Tags {
			"Queue" = "Transparent"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite On
			ZTest Less
			Blend One Zero

			CGPROGRAM
			#pragma vertex basicVertex
			#pragma fragment coloredFragment

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _Color;

			#include "ChuiIncludeCG.cginc"

			half4 coloredFragment(basicFragmentInput i) : COLOR {
				return tex2D(_MainTex, i.uv) * i.color * _Color;
			}

			ENDCG
		}
	}
}

