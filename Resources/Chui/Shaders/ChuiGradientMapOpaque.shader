﻿Shader "Chui/GradientMap Opaque" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "black" {}
		_GradTex ("Gradient (RGB)", 2D) = "black" {}
	}

	SubShader {
		LOD 200
		
		Tags {
			"Queue" = "Geometry"
		}
		
		Pass {
			Cull Back
			Lighting Off
			ZWrite On
			ZTest Less
			Blend One Zero

			CGPROGRAM
			#pragma vertex basicVertex
			#pragma fragment gradientMapFragment 

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _GradTex;			
			
			#include "ChuiIncludeCG.cginc"
			
			half4 gradientMapFragment(basicFragmentInput i) : COLOR {
				half4 s = tex2D(_MainTex, i.uv);
				half4 o = tex2D(_GradTex, half2(s.x, 0));
				return o;
			}

			ENDCG
		}
	}
}
