﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

namespace Chui {

	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Injector")]
	public class Injector : MonoBehaviour {
		public GameObject prefab;

		public bool autoSetInstanceLayers = true;
		public bool autoSetInstanceStatic = false;

		[SerializeField]
		GameObject instance;

		public GameObject Instance {
			get {
				return instance;
			}
		}

		virtual protected void Awake() {
			if (Application.isPlaying) {
				if (instance != null) {
					GameObject.Destroy(instance);
				}
				InitInstance();
			} else {
				GameObject.DestroyImmediate(instance);
				InitInstance();
			}
		}

		void InitInstance() {
			if (prefab != null) {
				if (!Application.isPlaying) {
#if UNITY_EDITOR
					instance = Instantiate(prefab) as GameObject;
					instance.transform.SetParent(Root.InjectorsContainer.transform, false);
					instance.transform.localScale = Vector3.one;
					foreach (Transform child in instance.GetComponentInChildren<Transform>()) {
						child.gameObject.hideFlags = HideFlags.NotEditable;
					}
					instance.hideFlags = HideFlags.NotEditable;

					AnchoredWidget injectedWidget = instance.GetComponent<AnchoredWidget>();
					Widget parentWidget = GetComponent<Widget>();
					if (injectedWidget  != null) {
						injectedWidget.leftAnchor.target = parentWidget;
						injectedWidget.rightAnchor.target = parentWidget;
						injectedWidget.topAnchor.target = parentWidget;
						injectedWidget.bottomAnchor.target = parentWidget;
						injectedWidget.backAnchor.target = parentWidget;
						injectedWidget.frontAnchor.target = parentWidget;
					}
					UnityEditor.EditorApplication.delayCall += () => {
						injectedWidget.RevalidateTopology();
					};
#endif
				} else {
					//TODO: provide external instantiation method
					instance = Instantiate(prefab) as GameObject;
					instance.transform.SetParent(transform, false);
					instance.name = prefab.name + "(Injected)";
					SetInstanceLayers();
					gameObject.GetComponent<Widget>().RevalidateHierarchy();
					if (autoSetInstanceStatic) {
						Util.SetHierarchyStatic(instance.transform, true);
					}
				}
			}
		}

		void SetInstanceLayers() {
			if (!autoSetInstanceLayers) {
				return;
			}
			instance.layer = gameObject.layer;
			foreach (Transform t in instance.GetComponentsInChildren<Transform>()) {
				t.gameObject.layer = gameObject.layer;
			}
		}

		protected virtual void Update() {
#if UNITY_EDITOR
			if (!Application.isPlaying) {
				if (instance == null) {
					InitInstance();
				} 

				if (instance != null) {
					instance.transform.rotation = transform.rotation;
					instance.GetComponent<Widget>().color = GetComponent<Widget>().derivedColor;
					SetInstanceLayers();
				}
			}
#endif
		}

		void OnEnable() {
			if (instance != null) {
				instance.SetActive(true);
			}
		}

		void OnDisable() {
			if (instance != null) {
				instance.SetActive(false);
			}
		}

		void DestroyInstance() {
#if UNITY_EDITOR
			if (!Application.isPlaying) {
				GameObject i = instance;
				if (i != null) {
					EditorApplication.delayCall += () => {
						if (i != null) {
							Object.DestroyImmediate(i);
						}
					};
				}
			} else {
				if (instance != null) {
					Object.Destroy(instance);
				}
			}
#else
			if (instance != null) {
				Object.Destroy(instance);
			}
#endif
		}

		void OnDestroy() {
			DestroyInstance();
		}
		
		public virtual void Revalidate() {
			DestroyInstance();
			InitInstance();
		}

		public void SetPrefab(GameObject prefab) {
			if (this.prefab != prefab) {
				this.prefab = prefab;
				Revalidate();
			}
		}

		public T GetInstanceComponent<T>() {
			return Instance.GetComponent<T>();
		}
	}
}
