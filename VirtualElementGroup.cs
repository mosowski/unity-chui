﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

    public class VirtualElementGroup : MonoBehaviour {

        private class Pool {

            private Transform root;
            private Dictionary<GameObject, Stack<GameObject>> data = new Dictionary<GameObject, Stack<GameObject>>();
            private int createdCount = 0;

            public Pool(Transform root) {
                this.root = root;
            }

            public GameObject Pop(GameObject prefab) {
                Stack<GameObject> stack = null;

                if(!data.TryGetValue(prefab, out stack)) {
                    stack = new Stack<GameObject>();
                    data.Add(prefab, stack);
                }

                GameObject result = null;

                if(stack.Count > 0) {
                    result = stack.Pop();
                } else {
                    ++createdCount;
                    result = GameObject.Instantiate(prefab) as GameObject;
                    result.name = prefab.name + "-pool" + createdCount.ToString();
                }

                result.gameObject.SetActive(true);
                return result;
            }

            public void Push(GameObject prefab, GameObject gameObject) {
                Stack<GameObject> stack = null;

                if(!data.TryGetValue(prefab, out stack))
                return;

                if(stack.Contains(gameObject))
                return;

                gameObject.SetActive(false);
                gameObject.transform.SetParent(root, false);
                stack.Push(gameObject);
            }

        }            

        private Pool pool = null;
        private Dictionary<GameObject, Vector3> cachedPrefabSize = new Dictionary<GameObject, Vector3>();
        private Dictionary<VirtualElement, GameObject> createdElements = new Dictionary<VirtualElement, GameObject>();

        private bool isValidated = false;
        private VirtualElementLayout.Data layoutData = null;

        private List<VirtualElement> toCreate = new List<VirtualElement>(30);
        private List<VirtualElement> toRemove = new List<VirtualElement>(30);

        public VirtualElementLayout layout;
        public Widget window;
        public Widget elementParent;

        public List<VirtualElement> elements = new List<VirtualElement>();

        public void Update() {
            UpdateVirtualElements();
        }

        public void Revalidate() {
            elementParent.GetComponent<Widget>().Revalidate();

            layoutData = layout.GenerateLayout(this);
            if(layoutData.overrideWidth) {
                elementParent.Width = layoutData.contentWidth;
            }
            if(layoutData.overrideHeight) {
                elementParent.Height = layoutData.contentHeight;
            }

            elementParent.GetComponent<Widget>().RevalidateHierarchy();
            UpdateVirtualElements();
            isValidated = true;
        }

        public void UpdateVirtualElements() {
            if(!isValidated) {
                return;
            }

            toCreate.Clear();
            toRemove.Clear();

            for(int i = 0; i < elements.Count; ++i) {
                if(IsRequired(elements[i])) {
                    if(!createdElements.ContainsKey(elements[i])) {
                        toCreate.Add(elements[i]);
                    }
                } else {
                    if(createdElements.ContainsKey(elements[i])) {
                        toRemove.Add(elements[i]);
                    }
                }
            }

            for(int i = 0; i < toRemove.Count; ++i) {
                RemoveVirtualElement(toRemove[i]);
            }

            for(int i = 0; i < toCreate.Count; ++i)
            {
                VirtualElement e = toCreate[i];
                InitializeVirtualElement(e, true);
            }
        }            

        public bool autoRemove = true;
        public bool autoCreate = true;

        public bool IsRequired(VirtualElement element) {
            if (elementParent.GetLocalPosition().y + layoutData.elements[element].localVolume.yMin > window.volume.yMax) {
                return false;
            }
            if(elementParent.GetLocalPosition().y + layoutData.elements[element].localVolume.yMax < window.volume.yMin) {
                return false;
            }
            return true;
        }           

        public Vector3 GetSizeFromPrefab(VirtualElement element) {
            Vector3 size;
            if(cachedPrefabSize.TryGetValue(element.Prefab, out size)) {
                return size;
            }

            size = GetSizeFromInstance(element);
            cachedPrefabSize.Add(element.Prefab, size);

            return size;
        }

        public Vector3 GetSizeFromInstance(VirtualElement element) {
            GameObject gameObject = InitializeVirtualElement(element, false);

            Widget widget = gameObject.GetComponent<Widget>();
            widget.OnUpdate();
            Vector3 size = new Vector3(widget.Width, widget.Height, widget.Depth);

            if(element.CustomRelease != null) {
                element.CustomRelease(this, element, gameObject, widget);
            }
            pool.Push(element.Prefab, gameObject);

            return size;
        }

        public VirtualElement GetVirtualElement(GameObject gameObject) {
            foreach(KeyValuePair<VirtualElement, GameObject> pair in createdElements) {
                if(pair.Value == gameObject) {
                    return pair.Key;
                }
            }
            return null;
        }

        private GameObject InitializeVirtualElement(VirtualElement element, bool register = true) {
            if(pool == null) {
                pool = new Pool(transform);
            }

            GameObject gameObject = null;
            if(!register || !createdElements.TryGetValue(element, out gameObject)) {
                gameObject = pool.Pop(element.Prefab);
                if(register) {
                    createdElements.Add(element, gameObject);
                }
            }

            gameObject.transform.SetParent(elementParent.transform, false);
            gameObject.transform.localScale = Vector3.one;
            gameObject.transform.localRotation = Quaternion.identity;           

            Widget widget = gameObject.GetComponent<Widget>();
            widget.Revalidate();

            if(element.CustomInitialize != null) {
                element.CustomInitialize(this, element, gameObject, widget);
            }

            if(register && layoutData != null) {
                VirtualElementLayout.ElementData elementData = null;
                if(layoutData.elements.TryGetValue(element, out elementData)) {                    
                    widget.SetLocalPosition(elementData.localPosition);
                    widget.RevalidateHierarchy();
                }
            }                

            return gameObject;
        }

        private void RemoveVirtualElement(VirtualElement e) {
            GameObject o = createdElements[e];

            if(e.CustomRelease != null) {
                e.CustomRelease(this, e, o, o.GetComponent<Widget>());
            }
            createdElements.Remove(e);
            pool.Push(e.Prefab, o);
        }
    }
}