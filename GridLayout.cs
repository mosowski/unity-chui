﻿using UnityEngine;
using System.Collections;

namespace Chui {
	[ExecuteInEditMode]
	public class GridLayout : MonoBehaviour {
		public enum Order {
			Row,
			Column
		}

		public int numColumns = 1;
		public int numRows = 1;
		public float fixedCellWidth = 0;
		public float fixedCellHeight = 0;
		public Order order = Order.Row;
		public Vector3 originAnchor = new Vector3(0, 1, 0);
		public bool revalidateOnUpdate = false;

		public void Revalidate() {
			if (numColumns == 0 || numRows == 0) {
				return;
			}
			Transform t = transform;
			int childCount = t.childCount;
			Widget widget = GetComponent<Widget>();
			Vector3[] positions = new Vector3[t.childCount];

			Vector3 origin = widget.volume.GetPoint(originAnchor);
			Vector3 corner = widget.volume.GetPoint(Vector3.one - originAnchor);

			float cellWidth = Mathf.Approximately(fixedCellWidth, 0) ? (corner.x - origin.x) / numColumns : (fixedCellWidth * Mathf.Sign(corner.x - origin.x));
			float cellHeight = Mathf.Approximately(fixedCellHeight, 0) ? (corner.y - origin.y) / numRows : (fixedCellHeight * Mathf.Sign(corner.y - origin.y));

			for (int i = 0; i < childCount; ++i) {
				GameObject item = t.GetChild(i).gameObject;
				Widget itemWidget = item.GetComponent<Widget>();
				if (itemWidget == null || !item.activeSelf) {
					continue;
				}

				int row, column;
				if (order == Order.Row) {
					row = i / numColumns;
					column = i % numColumns;
				} else {
					row = i % numRows;
					column = i / numRows;
				}
				itemWidget.SetLocalPosition(origin + new Vector3(cellWidth * 0.5f + column * cellWidth, cellHeight * 0.5f + row * cellHeight, 0));
				itemWidget.RevalidateHierarchy();
			}
		}

		public void ResizeToItems() {
			if (numColumns == 0 || numRows == 0) {
				return;
			}
			Transform t = transform;
			int childCount = t.childCount;
			int numValidItems = 0;
			for (int i = 0; i < t.childCount; ++i) {
				GameObject item = t.GetChild(i).gameObject;
				Widget itemWidget = item.GetComponent<Widget>();
				if (itemWidget == null || !item.activeSelf) {
					continue;
				}
				numValidItems++;
			}

			Widget widget = GetComponent<Widget>();
			Vector3 origin = widget.volume.GetPoint(originAnchor);
			Vector3 corner = widget.volume.GetPoint(Vector3.one - originAnchor);

			float cellWidth = Mathf.Approximately(fixedCellWidth, 0) ? (corner.x - origin.x) / numColumns : (fixedCellWidth * Mathf.Sign(corner.x - origin.x));
			float cellHeight = Mathf.Approximately(fixedCellHeight, 0) ? (corner.y - origin.y) / numRows : (fixedCellHeight * Mathf.Sign(corner.y - origin.y));

			if (order == Order.Row) {
				numRows = (numValidItems + numColumns - 1) / numColumns;
				widget.Height = numRows * Mathf.Abs(cellHeight);
			} else {
				numColumns = (numValidItems + numRows - 1) / numRows;
				widget.Width = numColumns * Mathf.Abs(cellWidth);
			}
		}

		void Update() {
			if (revalidateOnUpdate || !Application.isPlaying) {
				Revalidate();
			}
		}
	}
}