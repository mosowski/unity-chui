using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	[AddComponentMenu("Chui/Font Info")]
	public class FontInfo : ScriptableObject, ISerializationCallbackReceiver {

		[System.Serializable]
		public class GlyphInfo : ISerializableDictionaryItem<int> {
			public int id;
			public Rect uv;
			public Rect geom;
			public float advance;
			public bool isColorEnabled;
			public Material material;

			public int GetDictionaryKey() {
				return id;
			}
		}

		[System.Serializable]
		public class IconSource {
			public int id;
			public string code;
			public float scale = 1;
			public bool isColorEnabled = false;
			public Vector2 offset = Vector2.zero;
			public Texture2D texture;
			public Shader shader;
		}

		[System.Serializable]
		public class GlyphSource {
			public TextAsset description;
			public Texture2D texture;
			public Shader shader;
		}

		[System.Serializable]
		public class Kerning : ISerializableDictionaryItem<int> {
			public int from;
			public int to;
			public float amount;

			public int GetDictionaryKey() {
				return from;
			}
		}

		public Color color = Color.white;
		public float fontSize = 1;
		public float skew = 0;

		public List<GlyphSource> glyphSources = new List<GlyphSource>();
		public List<IconSource> iconSources = new List<IconSource>();

		public List<GlyphInfo> glyphsList = new List<GlyphInfo>();
		public List<Kerning> kerningsList = new List<Kerning>();

		[HideInInspector]
		public float lineHeight = 1;
		[HideInInspector]
		public float baseSize = 1;
		[HideInInspector]
		public Dictionary<int, GlyphInfo> glyphs = new Dictionary<int, GlyphInfo>();
		[HideInInspector]
		public Dictionary<int, List<Kerning>> kernings = new Dictionary<int, List<Kerning>>();

		[HideInInspector]
		public bool useSquareAtlas = true;

		public void OnBeforeSerialize() {
			SerializeData();
		}

		public void OnAfterDeserialize() {
			DeserializeData();
		}

		public void DeserializeData() {
			glyphs.Clear();
			Util.DeserializeDictionary(glyphsList, out glyphs);
			kernings.Clear();
			Util.DeserializeDictionary(kerningsList, out kernings);
		}

		public void SerializeData() {
			glyphsList.Clear();
			Util.SerializeDictionary(glyphs, out glyphsList);

			kerningsList.Clear();
			Util.SerializeDictionary(kernings, out kerningsList);
		}

		public float GetKerning(int from, int to) {
			List<Kerning> k = null;
			if (kernings.TryGetValue(from, out k)) {
				for (int i = 0; i < k.Count; ++i) {
					if (k[i].to == to) {
						return k[i].amount;
					}
				}
			}
			return 0;
		}
	}

}
