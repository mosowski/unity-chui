﻿using UnityEngine;
using System.Collections;

namespace Chui {
	public class TouchBlocker : MonoBehaviour {

		public bool blockClicked = true;
		public bool blockTouchBegan = true;
		public bool blockTouchMoved = true;
		public bool blockTouchEnded = true;

		void Awake() {
			if (blockClicked) {
				Util.GetEventHandlers(gameObject).onClicked.Set(ti => true);
			}
			if (blockTouchBegan) {
				Util.GetEventHandlers(gameObject).onTouchBegan.Set(ti => true);
			}
			if (blockTouchMoved) {
				Util.GetEventHandlers(gameObject).onTouchMoved.Set(ti => true);
			}
			if (blockTouchEnded) {
				Util.GetEventHandlers(gameObject).onTouchEnded.Set(ti => true);
			}
		}
	}
}
