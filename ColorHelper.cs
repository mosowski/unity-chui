﻿#region Using statements

using UnityEngine;
using System.Collections;

#endregion

public static class ColorHelper 
{

    public static string ToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public static Color FromHex(string hex)
    {
        return FromHex(hex, Color.white);
    }

    public static Color FromHex(string hex, Color defaultColor)
    {
        if(string.IsNullOrEmpty(hex.Trim()))
            return defaultColor;

        byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r,g,b, 255);
    }

}
