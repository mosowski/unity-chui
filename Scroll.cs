using UnityEngine;
using System.Collections;

namespace Chui {

	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Scroll")]
	public class Scroll : Content {

		public Widget content;

		public bool isXScrollEnabled = false;
		public bool isYScrollEnabled = true;
		public bool isZScrollEnabled = false;
		public bool isAxisLockingEnabled = true;
		public bool useGestures = false;
		public Vector3 scrollInertia;
		public float inertiaVelocityThreshold = 1;
		public bool isLimitingEnabled = true;
		public bool revalidateHierarchyOnScroll = true;
		public float dragThreshold = 0.1f;

		protected Vector3 scrollShift;
		protected Vector3 scrollVelocity;
		protected bool isDragging;
		protected float fixedTimeStep;

		DragListener dragListener;
		GestureListener gestureListener;
		Vector3 dragContentOrigin;
		Vector3 localDragContentOrigin;
		Vector3 dragStartPosition;

		Vector3 AxisMask {
			get {
				return new Vector3(
					isXScrollEnabled ? 1 : 0,
					isYScrollEnabled ? 1 : 0,
					isZScrollEnabled ? 1 : 0
				);
			}
		}
		
		public bool IsDragging {
			get {
				return isDragging;
			}
		}

		void Awake() {
			SetupDragListeners();
			widget = GetComponent<Widget>();
			fixedTimeStep = 1.0f / Root.GetInstance().deterministicFrameRate;
		}

		public void SetupDragListeners() {
			if (dragListener != null) {
				dragListener.onDragStarted.Clear();
				dragListener.onDragMoved.Clear();
				dragListener.onDragEnded.Clear();
			}
			if (gestureListener != null) {
				gestureListener.onHorizontalDragStarted.Clear();
				gestureListener.onHorizontalDragMoved.Clear();
				gestureListener.onHorizontalDragEnded.Clear();
				gestureListener.onVerticalDragStarted.Clear();
				gestureListener.onVerticalDragMoved.Clear();
				gestureListener.onVerticalDragEnded.Clear();
				gestureListener.onFreeDragStarted.Clear();
				gestureListener.onFreeDragMoved.Clear();
				gestureListener.onFreeDragEnded.Clear();
			}
			if (useGestures) {
				gestureListener = Util.GetGestureListener(gameObject);
				if (isAxisLockingEnabled) {
					if (isXScrollEnabled) {
						gestureListener.onHorizontalDragStarted.Set(OnDragStarted);
						gestureListener.onHorizontalDragMoved.Set(OnDragMoved);
						gestureListener.onHorizontalDragEnded.Set(OnDragEnded);
					}
					if (isYScrollEnabled) {
						gestureListener.onVerticalDragStarted.Set(OnDragStarted);
						gestureListener.onVerticalDragMoved.Set(OnDragMoved);
						gestureListener.onVerticalDragEnded.Set(OnDragEnded);
					}
				} else {
					gestureListener.onFreeDragStarted.Set(OnDragStarted);
					gestureListener.onFreeDragMoved.Set(OnDragMoved);
					gestureListener.onFreeDragEnded.Set(OnDragEnded);
				}
			} else {
				dragListener = Util.GetDragListener(gameObject);
				dragListener.onDragStarted.Set(OnDragStarted);
				dragListener.onDragMoved.Set(OnDragMoved);
				dragListener.onDragEnded.Set(OnDragEnded);
				dragListener.dragThreshold = dragThreshold;
			}
		}

		public void DiscardDrag() {
			dragListener.DiscardDrag();
		}

		public void StopMovement() {
			scrollVelocity = Vector3.zero;
		}

		protected virtual bool OnDragStarted(DragListener dl) {
			dragContentOrigin = content.transform.localPosition;
			localDragContentOrigin = transform.InverseTransformPoint(content.transform.TransformPoint(Vector3.zero));
			dragStartPosition = dl.touchInfo.startPosition;

			isDragging = true;
			scrollVelocity = Vector3.zero;
			scrollShift = Vector3.zero;
			return true;
		}

		protected virtual bool OnDragMoved(DragListener dl) {
			Vector3 newScrollShift = transform.InverseTransformPoint(dl.touchInfo.position) - transform.InverseTransformPoint(dragStartPosition);
			scrollShift = newScrollShift;

			SetDragScrollShift(scrollShift);
			return true;
		}

		protected virtual bool OnDragEnded(DragListener dl) {
			Vector3 newScrollShift = transform.InverseTransformPoint(dl.touchInfo.position) - transform.InverseTransformPoint(dragStartPosition);
			Vector3 lastSwipe = transform.InverseTransformVector(dl.Swipe);
			scrollVelocity = lastSwipe / (dl.SwipeTicks * fixedTimeStep);
			scrollShift = newScrollShift;
			isDragging = false;

			SetDragScrollShift(scrollShift);
			return true;
		}

		void SetDragScrollShift(Vector3 v) {
			Vector3 axisMask = AxisMask;
			scrollShift = v;
			scrollShift.Scale(axisMask);

			if (isLimitingEnabled) {
				scrollShift = LimitScrollShift(axisMask, localDragContentOrigin, scrollShift);
			}

			content.SetLocalPosition(dragContentOrigin + scrollShift);
			if (revalidateHierarchyOnScroll) {
				widget.RevalidateHierarchy();
			}
		}

		Vector3 LimitScrollShift(Vector3 axisMask, Vector3 origin, Vector3 shift) {
			Vector3 minScroll = widget.volume.Min;
			Vector3 maxScroll = widget.volume.Max;

			Vector3 minPredicted = origin + shift + content.volume.Min;
			Vector3 maxPredicted = origin + shift + content.volume.Max;

			Vector3 minOverlap = minPredicted - minScroll;
			Vector3 minOverlapAdjust = minScroll - minPredicted;
			minOverlapAdjust.Scale(new Vector3(minOverlap.x > 0 ? 1 : 0, minOverlap.y > 0 ? 1 : 0, minOverlap.z > 0 ? 1 : 0));
			minOverlapAdjust.Scale(axisMask);

			Vector3 maxOverlap = maxScroll - maxPredicted;
			Vector3 maxOverlapAdjust = maxScroll - maxPredicted;
			maxOverlapAdjust.Scale(new Vector3(maxOverlap.x > 0 ? 1 : 0, maxOverlap.y > 0 ? 1 : 0, maxOverlap.z > 0 ? 1 : 0));
			maxOverlapAdjust.Scale(axisMask);

			shift += minOverlapAdjust;
			shift += maxOverlapAdjust;

			Vector3 room = content.volume.Size - widget.volume.Size;
			Vector3 liberty = new Vector3(room.x >= 0 ? 1 : 0, room.y >= 0 ? 1 : 0, room.z >= 0 ? 1 : 0);
			shift.Scale(liberty);

			return shift;
		}

		public void FixScrollPositionAfterResize() {
			SetDragScrollShift(Vector3.zero);
		}

		override public void DeterministicStep(Widget widget) {
			if (scrollVelocity.magnitude > inertiaVelocityThreshold && !isDragging) {
				scrollShift = scrollShift + scrollVelocity * fixedTimeStep;

				scrollVelocity.y *= Mathf.Pow(scrollInertia.y, fixedTimeStep);
				scrollVelocity.z *= Mathf.Pow(scrollInertia.z, fixedTimeStep);

				SetDragScrollShift(scrollShift);
			}
		}

		public void SetLocalScrollPosition(Vector3 contentPosition, bool limit = true) {
			Vector3 shift = contentPosition - content.transform.localPosition;
			shift.Scale(AxisMask);
			if (limit) {
				shift = LimitScrollShift(AxisMask, content.transform.localPosition, shift);
			}

			content.SetLocalPosition(content.transform.localPosition + shift);

			scrollVelocity = Vector3.zero;
			scrollShift = Vector3.zero;
			if (revalidateHierarchyOnScroll) {
				widget.RevalidateHierarchy();
			}
		}

		public void SetScrollPosition(Vector3 worldPosition) {
			Vector3 worldShift = -(worldPosition - transform.position);
			Vector3 shift = transform.InverseTransformVector(worldShift);
			shift.Scale(AxisMask);
			shift = LimitScrollShift(AxisMask, content.transform.localPosition, shift);

			content.SetLocalPosition(content.transform.localPosition + shift);

			scrollVelocity = Vector3.zero;
			scrollShift = Vector3.zero;
			if (revalidateHierarchyOnScroll) {
				widget.RevalidateHierarchy();
			}
		}

		public Vector3 GetScrollContentRelativeOffset() {
			Vector3 absoluteOffset = (content.transform.localPosition + content.volume.Max) - widget.volume.Max;
			Vector3 size = content.volume.Size - widget.volume.Size;
			if (size.x != 0) {
				size.x = 1.0f / size.x;
			}
			if (size.y != 0) {
				size.y = 1.0f / size.y;
			}
			if (size.z != 0) {
				size.z = 1.0f / size.z;
			}
			absoluteOffset.Scale(size);
			return absoluteOffset;
		}

		public Vector3 GetScrollContentRelativeSize() {
			Vector3 absoluteSize = widget.volume.Size;
			Vector3 size = content.volume.Size;
			if (size.x != 0) {
				size.x = 1.0f / size.x;
			}
			if (size.y != 0) {
				size.y = 1.0f / size.y;
			}
			if (size.z != 0) {
				size.z = 1.0f / size.z;
			}
			absoluteSize.Scale(size);
			return absoluteSize;
		}
	}
}

