﻿using UnityEngine;
using System.Collections;

namespace Chui {

	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Auto Scale")]
	[RequireComponent(typeof(Widget))]
	public class AutoScale : MonoBehaviour {
		Widget widget;

		public Widget targetWidget;
		public GameObject targetGameObject;

		void Awake() {
			widget = GetComponent<Widget>();
		}
		
		void Update() {
			if (targetWidget == null) {
				if (targetGameObject == null) {
					return;
				} else {
					targetWidget = targetGameObject.GetComponent<Widget>();
					if (targetWidget == null) {
						return;
					}
				}
			}
			if (widget == null) {
				widget = GetComponent<Widget>();
			}

			widget.SetLocalPosition(targetWidget.volume.GetPoint(widget.pivot) - targetWidget.volume.GetPoint(targetWidget.pivot));

			Vector3 s = Vector3.one;
			if (widget.Width != 0) {
				s.x = targetWidget.volume.Width / widget.Width;
			}
			if (widget.Height != 0) {
				s.y = targetWidget.volume.Height / widget.Height;
			}
			if (widget.Depth != 0) {
				s.z = targetWidget.volume.Depth / widget.Depth;
			}

			transform.localScale = s;
		}
	}
}
