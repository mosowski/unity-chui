﻿using UnityEngine;
using System.Collections;

namespace Chui {
	public enum ScrollBarLayout {
		Vertical,
		Horizontal
	}

	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Scroll Bar")]
	public class ScrollBar : MonoBehaviour {

		public ScrollBarLayout layout;
		public Widget parentWidget;
		public Widget knobWidget;

		public bool useRelativeKnobSize = true;
		public float relativeKnobSize = 0.2f;

		public float value = 0.0f;

		public Scroll scroll = null;

		float last_value = -1.0f;
		float last_relativeKnobSize = -1.0f;
		bool last_useRelativeKnobSize = true;
		Scroll last_scroll = null;

		public void Revalidate() {
			if (parentWidget == null || knobWidget == null) {
				return;
			}
			value = Mathf.Clamp01(value);
			relativeKnobSize = Mathf.Clamp01(relativeKnobSize);

			Vector2 mask = layout == ScrollBarLayout.Horizontal ? new Vector2(1.0f, 0.0f) : new Vector2(0.0f, 1.0f);
			float parentSize = parentWidget.Width * mask.x + parentWidget.Height * mask.y;

			float knobSize = useRelativeKnobSize ? relativeKnobSize : (knobWidget.Width * mask.x + knobWidget.Height * mask.y) / parentSize;
			float offset = value * (1.0f - knobSize) + knobSize * 0.5f;

			Vector3 position = knobWidget.transform.localPosition;
			if (layout == ScrollBarLayout.Horizontal) {
				position.x = parentWidget.volume.xMin + offset * parentSize;
				if (useRelativeKnobSize) {
					knobWidget.Width = parentSize * relativeKnobSize;
				}
			} else {
				position.y = parentWidget.volume.yMin + offset * parentSize;
				if (useRelativeKnobSize) {
					knobWidget.Height = parentSize * relativeKnobSize;
				}
			}

			knobWidget.SetLocalPosition(position);
			parentWidget.RevalidateHierarchy();
		}

		void Update () {
			bool hasChanged = false;

			if (scroll != last_scroll) {
				last_scroll = scroll;
				hasChanged = true;
			}

			if (scroll != null) {
				if (layout == ScrollBarLayout.Horizontal) {
					value = 1 - scroll.GetScrollContentRelativeOffset().x;
					if (useRelativeKnobSize) {
						relativeKnobSize = scroll.GetScrollContentRelativeSize().x;
					}
				} else {
					value = 1 - scroll.GetScrollContentRelativeOffset().y;
					if (useRelativeKnobSize) {
						relativeKnobSize = scroll.GetScrollContentRelativeSize().y;
					}
				}
			}

			if (value != last_value) {
				last_value = value;
				hasChanged = true;
			}

			if (useRelativeKnobSize != last_useRelativeKnobSize) {
				last_useRelativeKnobSize = useRelativeKnobSize;
				hasChanged = true;
			}

			if (relativeKnobSize != last_relativeKnobSize) {
				last_relativeKnobSize = relativeKnobSize;
				hasChanged = true;
			}

			if (hasChanged) {
				Revalidate();
			}
		}
	}
}
