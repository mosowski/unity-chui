using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Chui {
	public enum QuadGeometryMode {
		Quad,
		Scale9,
		UvTiled
	}

	[System.Serializable]
	public struct QuadBorderSize {
		public float uvLeft;
		public float uvRight;
		public float uvBottom;
		public float uvTop;
		public float realLeft;
		public float realRight;
		public float realBottom;
		public float realTop;

		public bool IsEqual(ref QuadBorderSize s) {
			return uvLeft == s.uvLeft
				&& uvRight == s.uvRight
				&& uvBottom == s.uvBottom
				&& uvTop == s.uvTop
				&& realLeft == s.realLeft
				&& realRight == s.realRight
				&& realBottom == s.realBottom
				&& realTop == s.realTop;
		}
	}

	[System.Serializable]
	public struct QuadTileSettings {
		public enum Origin {
			TopLeft,
			TopRight,
			BottomLeft,
			BottomRight
		}

		public float xSize;
		public float ySize;
		public float xOffset;
		public float yOffset;
		public Origin origin;

		public bool IsEqual(ref QuadTileSettings s) {
			return xSize == s.xSize
				&& ySize == s.ySize
				&& xOffset == s.xOffset
				&& yOffset == s.yOffset
				&& origin == s.origin;
		}
	}

	[AddComponentMenu("Chui/Quad Content")]
	public class QuadContent : Content {

		public QuadGeometryMode geometryMode = QuadGeometryMode.Quad;

		public Material material;

		// for GeometryMode.Scale9
		public QuadBorderSize borderSize;
		public bool fillBorderCenter = true;

		// for GeometryMode.Tiled
		public QuadTileSettings tileSettings;

		public bool uFlip;
		public bool vFlip;

		public Color color = Color.white;

		public Rect uvRect = new Rect(0, 0, 1, 1);

		[SerializeField]
		protected bool hasChanged;
		[SerializeField]
		protected Vector4 border;

		Mesh mesh;
		MeshFilter meshFilter;
		protected MeshRenderer meshRenderer;
		protected Color derivedColor;

		QuadTileSettings last_tileSettings;
		Color last_derivedColor;
		QuadGeometryMode last_quadGeometryMode;
		Material last_material;
		QuadBorderSize last_borderSize;
		bool last_fillBorderCenter;
		bool last_uFlip;
		bool last_vFlip;
		Rect last_uvRect;
		Vector4 last_border;

		int[] indices = new int[] { 0, 2, 1, 2, 0, 3 };
		Vector3[] vertices = new Vector3[] { Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero };
		Vector2[] uvs = new Vector2[] { Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero };
		Color32[] colors = new Color32[] { Color.white, Color.white, Color.white, Color.white };

		public Material Material { get { return material; } set { material = value; Revalidate(); } }

		protected virtual void PrepareMesh() {
			if (mesh == null) {
				int neededSize = 4;
				switch (geometryMode) {
					case QuadGeometryMode.Quad: neededSize = 4; break;
					case QuadGeometryMode.Scale9: neededSize = fillBorderCenter ? 36 : 32; break;
					case QuadGeometryMode.UvTiled: neededSize = 4; break;
				}

				if (Pools.RestoreMeshBySize(neededSize, out mesh)) {
					vertices = mesh.vertices;
					colors = mesh.colors32;
					uvs = mesh.uv;
					indices = mesh.triangles;
				} else {
					mesh = new Mesh();
				}
			}

			if (meshFilter == null) {
				meshFilter = GetComponent<MeshFilter>();
				if (meshFilter == null) {
					meshFilter = gameObject.AddComponent<MeshFilter>();
					meshFilter.hideFlags = HideFlags.NotEditable;
				}
			}
			meshFilter.sharedMesh = mesh;

			if (meshRenderer == null) {
				meshRenderer = GetComponent<MeshRenderer>();
				if (meshRenderer == null) {
					meshRenderer = gameObject.AddComponent<MeshRenderer>();
					meshRenderer.hideFlags = HideFlags.NotEditable;
				}
			}
		}

		void ResizeArrays(int numVertices) {
			if (vertices.Length != numVertices) {
				vertices = new Vector3[numVertices];
				uvs = new Vector2[numVertices];
				colors = new Color32[numVertices];
				indices = new int[numVertices * 6 / 4];
				for (int i = 0; i < numVertices; i += 4) {
					int v = i * 6 / 4;
					indices[v + 0] = i + 0;
					indices[v + 1] = i + 2;
					indices[v + 2] = i + 1;
					indices[v + 3] = i + 2;
					indices[v + 4] = i + 0;
					indices[v + 5] = i + 3;
				}
			}
		}

		void SetQuadGeometry(int id, Vector4 p, float z, Vector4 u) {
			if (uFlip) {
				u.x = 1 - u.x;
				u.z = 1 - u.z;
			}

			if (vFlip) {
				u.y = 1 - u.y;
				u.w = 1 - u.w;
			}

			u.x = uvRect.x + uvRect.width * u.x;
			u.y = uvRect.y + uvRect.height * u.y;
			u.z = uvRect.x + uvRect.width * u.z;
			u.w = uvRect.y + uvRect.height * u.w;

			id *= 4;

			vertices[id + 0].x = p.x;
			vertices[id + 0].y = p.y;
			vertices[id + 0].z = z;
			uvs[id + 0].x = u.x;
			uvs[id + 0].y = u.y;
			colors[id + 0] = derivedColor;

			vertices[id + 1].x = p.z;
			vertices[id + 1].y = p.y;
			vertices[id + 1].z = z;
			uvs[id + 1].x = u.z;
			uvs[id + 1].y = u.y;
			colors[id + 1] = derivedColor;

			vertices[id + 2].x = p.z;
			vertices[id + 2].y = p.w;
			vertices[id + 2].z = z;
			uvs[id + 2].x = u.z;
			uvs[id + 2].y = u.w;
			colors[id + 2] = derivedColor;

			vertices[id + 3].x = p.x;
			vertices[id + 3].y = p.w;
			vertices[id + 3].z = z;
			uvs[id + 3].x = u.x;
			uvs[id + 3].y = u.w;
			colors[id + 3] = derivedColor;

		}

		protected virtual float TextureWidth { get { return material != null && material.mainTexture != null ? material.mainTexture.width : widget.Width; } }
		protected virtual float TextureHeight { get { return material != null && material.mainTexture != null ? material.mainTexture.height : widget.Width; } }

		void BuildQuadGeometry() {
			ResizeArrays(4);
			Vector4 resizedBorder = CalculateResizedBorder();
			SetQuadGeometry(0,
				new Vector4(
					-widget.volume.Width * widget.pivot.x + resizedBorder.x,
					-widget.volume.Height * widget.pivot.y + resizedBorder.y,
					widget.volume.Width * (1 - widget.pivot.x) - resizedBorder.z,
					widget.volume.Height * (1 - widget.pivot.y) - resizedBorder.w
				),
				0,
				new Vector4(0, 0, 1, 1)
			);
		}

		void BuildScale9Geometry() {
			if (fillBorderCenter) {
				ResizeArrays(36);
			} else {
				ResizeArrays(32);
			}
			float uvLeft = borderSize.uvLeft;
			float uvRight = borderSize.uvRight;
			float realLeft = borderSize.realLeft;
			float realRight = borderSize.realRight;
			if (uFlip) {
				uvLeft = borderSize.uvRight;
				uvRight = borderSize.uvLeft;
				realLeft = borderSize.realRight;
				realRight = borderSize.realLeft;
			}

			if (widget.Width < borderSize.realLeft) {
				float ratio = widget.Width / borderSize.realLeft;
				uvLeft *= ratio;
				realLeft *= ratio;
				realRight = 0.0f;
			} else if (widget.Width < borderSize.realLeft + borderSize.realRight) {
				realRight = widget.Width - realLeft;
				uvRight *= realRight / borderSize.realRight;
			}

			float uvBottom = borderSize.uvBottom;
			float uvTop = borderSize.uvTop;
			float realBottom = borderSize.realBottom;
			float realTop = borderSize.realTop;
			if (vFlip) {
				uvBottom = borderSize.uvRight;
				uvTop = borderSize.uvBottom;
				realBottom = borderSize.realTop;
				realTop = borderSize.realBottom;
			}


			if (widget.Height < borderSize.realBottom) {
				float ratio = widget.Height / borderSize.realBottom;
				uvBottom *= ratio;
				realBottom *= ratio;
				realTop = 0.0f;
			} else if (widget.Height < borderSize.realBottom + borderSize.realTop) {
				realTop = widget.Height - realBottom;
				uvTop *= realTop / borderSize.realTop;
			}

			// bottom left, center, right
			SetQuadGeometry(0,
				new Vector4(
					-widget.volume.Width * widget.pivot.x + border.x,
					-widget.volume.Height * widget.pivot.y + border.y,
					-widget.volume.Width * widget.pivot.x + realLeft,
					-widget.volume.Height * widget.pivot.y + realBottom
				),
				0,
				new Vector4(0, 0, uvLeft, uvBottom)
			);

			SetQuadGeometry(1,
				new Vector4(
					-widget.volume.Width * widget.pivot.x + realLeft,
					-widget.volume.Height * widget.pivot.y + border.y,
					widget.volume.Width * (1 - widget.pivot.x) - realRight,
					-widget.volume.Height * widget.pivot.y + realBottom
				),
				0,
				new Vector4(uvLeft, 0, 1 - uvRight, uvBottom)
			);

			SetQuadGeometry(2,
				new Vector4(
					widget.volume.Width * (1 - widget.pivot.x) - realRight,
					-widget.volume.Height * widget.pivot.y + border.y,
					widget.volume.Width * (1 - widget.pivot.x) - border.z,
					-widget.volume.Height * widget.pivot.y + realBottom
				),
				0,
				new Vector4(1 - uvRight, 0, 1, uvBottom)
			);

			// middle left, right
			SetQuadGeometry(3,
				new Vector4(
					-widget.volume.Width * widget.pivot.x + border.x,
					-widget.volume.Height * widget.pivot.y + realBottom,
					-widget.volume.Width * widget.pivot.x + realLeft,
					widget.volume.Height * (1 - widget.pivot.y) - realTop
				),
				0,
				new Vector4(0, uvBottom, uvLeft, 1 - uvTop)
			);

			SetQuadGeometry(4,
				new Vector4(
					widget.volume.Width * (1 - widget.pivot.x) - realRight,
					-widget.volume.Height * widget.pivot.y + realBottom,
					widget.volume.Width * (1 - widget.pivot.x) - border.z,
					widget.volume.Height * (1 - widget.pivot.y) - realTop
				),
				0,
				new Vector4(1 - uvRight, uvBottom, 1, 1 - uvTop)
			);

			// top left, center, right
			SetQuadGeometry(5,
				new Vector4(
					-widget.volume.Width * widget.pivot.x + border.x,
					widget.volume.Height * (1 - widget.pivot.y) - realTop,
					-widget.volume.Width * widget.pivot.x + realLeft,
					widget.volume.Height * (1 - widget.pivot.y) - border.w
				),
				0,
				new Vector4(0, 1 - uvTop, uvLeft, 1)
			);

			SetQuadGeometry(6,
				new Vector4(
					-widget.volume.Width * widget.pivot.x + realLeft,
					widget.volume.Height * (1 - widget.pivot.y) - realTop,
					widget.volume.Width * (1 - widget.pivot.x) - realRight,
					widget.volume.Height * (1 - widget.pivot.y) - border.w
				),
				0,
				new Vector4(uvLeft, 1 - uvTop, 1 - uvRight, 1)
			);

			SetQuadGeometry(7,
				new Vector4(
					widget.volume.Width * (1 - widget.pivot.x) - realRight,
					widget.volume.Height * (1 - widget.pivot.y) - realTop,
					widget.volume.Width * (1 - widget.pivot.x) - border.z,
					widget.volume.Height * (1 - widget.pivot.y) - border.w
				),
				0,
				new Vector4(1 - uvRight, 1 - uvTop, 1, 1)
			);

			if (fillBorderCenter) {
				SetQuadGeometry(8,
					new Vector4(
						-widget.volume.Width * widget.pivot.x + realLeft,
						-widget.volume.Height * widget.pivot.y + realBottom,
						widget.volume.Width * (1 - widget.pivot.x) - realRight,
						widget.volume.Height * (1 - widget.pivot.y) - realTop
					),
					0,
					new Vector4(uvLeft, uvBottom, 1 - uvRight, 1 - uvTop)
				);
			}
		}

		void BuildUvTiledGeometry() {
			ResizeArrays(4);

			float uLoops = 1;
			float vLoops = 1;
			if (tileSettings.xSize != 0) {
				uLoops = widget.volume.Width / tileSettings.xSize;
			}
			if (tileSettings.ySize != 0) {
				vLoops = widget.volume.Height / tileSettings.ySize;
			}

			Vector4 uv = Vector4.zero;

			if (tileSettings.origin == QuadTileSettings.Origin.BottomLeft) {
				uv = new Vector4(0, 0, uLoops, vLoops);
			} else if (tileSettings.origin == QuadTileSettings.Origin.TopLeft) {
				uv = new Vector4(0, -vLoops, uLoops, 0);
			} else if (tileSettings.origin == QuadTileSettings.Origin.BottomRight) {
				uv = new Vector4(-uLoops, 0, 0, vLoops);
			} else if (tileSettings.origin == QuadTileSettings.Origin.TopRight) {
				uv = new Vector4(-uLoops, -vLoops, 0, 0);
			}

			Vector4 resizedBorder = CalculateResizedBorder();

			SetQuadGeometry(0,
				new Vector4(
					-widget.volume.Width * widget.pivot.x + resizedBorder.x,
					-widget.volume.Height * widget.pivot.y + resizedBorder.y,
					widget.volume.Width * (1 - widget.pivot.x) - resizedBorder.z,
					widget.volume.Height * (1 - widget.pivot.y) - resizedBorder.w
				),
				0,
				new Vector4(uv.x + tileSettings.xOffset, uv.y + tileSettings.yOffset, uv.z + tileSettings.xOffset, uv.w + tileSettings.yOffset)
			);
		}

		protected virtual void BuildGeometry() {
			switch (geometryMode) {
				case QuadGeometryMode.Quad: BuildQuadGeometry(); break;
				case QuadGeometryMode.Scale9: BuildScale9Geometry(); break;
				case QuadGeometryMode.UvTiled: BuildUvTiledGeometry(); break;
			}

			mesh.Clear();
			mesh.vertices = vertices;
			mesh.uv = uvs;
			mesh.colors32 = colors;
			mesh.triangles = indices;

			mesh.RecalculateBounds();
		}

		void OnDestroy() {
			if (mesh != null) {
				Pools.StoreMesh(mesh);
			}
#if UNITY_EDITOR

			//NOTE
			// When pressing play button, Unity calls OnDestroy on edit-mode instance of component just before 
			// calling Awake on play-mode instance. The meshFilter and meshRenderer are for some reason shared 
			// between edit and play mode instances, so delayedCall for edit-mode would destroy play-mode components.

			bool wasPlaying = Application.isPlaying;
			if (meshFilter != null) {
				EditorApplication.delayCall += () => {
					if (Application.isPlaying == wasPlaying && meshFilter) {
						Object.DestroyImmediate(meshFilter);
					}
				};
			}
			if (meshRenderer != null) {
				EditorApplication.delayCall += () => {
					if (Application.isPlaying == wasPlaying && meshRenderer) {
						Object.DestroyImmediate(meshRenderer);
					}
				};
			}
#else
			if (meshFilter != null) {
				Object.Destroy(meshFilter);
			}
			if (meshRenderer != null) {
				Object.Destroy(meshRenderer);
			}
#endif

		}

		public override void UpdateContent(Widget widget) {
			base.UpdateContent(widget);

			if (geometryMode != last_quadGeometryMode) {
				last_quadGeometryMode = geometryMode;
				hasChanged = true;
			}

			if (geometryMode == QuadGeometryMode.UvTiled) {
				if (!tileSettings.IsEqual(ref last_tileSettings)) {
					hasChanged = true;
					last_tileSettings = tileSettings;
				}
			} else if (geometryMode == QuadGeometryMode.Scale9) {
				if (!borderSize.IsEqual(ref last_borderSize)) {
					hasChanged = true;
					last_borderSize = borderSize;
				}

				if (fillBorderCenter != last_fillBorderCenter) {
					hasChanged = true;
					last_fillBorderCenter = fillBorderCenter;
				}
			}

			derivedColor = widget.derivedColor * color;
			if (derivedColor != last_derivedColor) {
				hasChanged = true;
				last_derivedColor = derivedColor;
			}

			if (material != last_material) {
				hasChanged = true;
				last_material = material;
			}

			if (uFlip != last_uFlip || vFlip != last_vFlip) {
				hasChanged = true;
				last_uFlip = uFlip;
				last_vFlip = vFlip;
			}

			if (uvRect != last_uvRect) {
				hasChanged = true;
				last_uvRect = uvRect;
			}

			if (border != last_border) {
				hasChanged = true;
				last_border = border;
			}

			if (hasChanged || widget.changes != 0) {
				hasChanged = false;

				if (material != null) {
					PrepareMesh();
					BuildGeometry();
					meshRenderer.material = material;
				}
			}
		}

		public override void ReloadContent() {
		}

		private Vector4 CalculateResizedBorder() {
			return new Vector4(
				border.x * widget.volume.Width / TextureWidth,
				border.y * widget.volume.Height / TextureHeight,
				border.z * widget.volume.Width / TextureWidth,
				border.w * widget.volume.Height / TextureHeight
			);
		}

		protected virtual Shader CurrentShader {
			get {
				return material.shader;
			} set {
				material.shader = value;
			}
		}
	}
}
