﻿using UnityEngine;
using System.Collections;

namespace Chui {

	public class EventInfo {
	}

	public class TouchInfo : EventInfo {
		public enum Phase {
			Begin = 1,
			Move = 2,
			End = 3,
			Invalid = -1
		}

		public static readonly int LeftMouseButton = 0;

		public int id;
		public Phase phase;
		public Vector3 startPosition;
		public int startStep;

		// world coordinates placed on Root plane
		public Vector3 position;
		public Vector3 rawPosition;
		public Vector3 velocity;
		public float pressure;

		public delegate void UpdateHandler(TouchInfo ti);
		public event UpdateHandler onUpdate;

		int currentTouchDuration = 0;
		float moveDistance = 0;
		bool hasMoved;

		public void Update(int currentStep) {
			if (onUpdate != null) {
				onUpdate(this);
			}
			currentTouchDuration = currentStep - startStep;
		}

		public void UpdatePosition(Vector3 newPosition) {
			velocity = newPosition - position;
			position = newPosition;
			if (hasMoved) {
				moveDistance += velocity.magnitude;
			} else {
				hasMoved = true;
			}
		}

		public int Duration {
			get {
				return currentTouchDuration;
			}
		}

		public float DistanceFromStart {
			get {
				return Vector3.Distance(startPosition, position);
			}
		}

		public float MoveDistance {
			get {
				return moveDistance;
			}
		}

		public bool IsLongTouch(float maxDistance, float duration) {
			return MoveDistance < maxDistance && Duration > duration;
		}

	}

}
