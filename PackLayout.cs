﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	[ExecuteInEditMode]
	public class PackLayout : MonoBehaviour {
		public float horizontalAlign = 0;
		public float verticalAlign = 1;
		public float horizontalOverpackTolerance = 0.1f;
		public bool revalidateOnUpdate = false;

		public void Revalidate() {
			Transform t = transform;
			Widget widget = GetComponent<Widget>();
			Vector3[] positions = new Vector3[t.childCount];

			float x = 0;
			float y = 0;
			float rowHeight = 0;
			float totalWidth = 0;
			float totalHeight = 0;

			for (int i = 0; i < t.childCount; ++i) {
				GameObject item = t.GetChild(i).gameObject;
				Widget itemWidget = item.GetComponent<Widget>();
				if (itemWidget == null || !item.activeSelf) {
					continue;
				}

				itemWidget.Revalidate();

				float rightEdge = x + itemWidget.Width;
				if (rightEdge > widget.Width + horizontalOverpackTolerance) {
					x = 0;
					y -= rowHeight;
					rowHeight = itemWidget.Height;
				}

				if (itemWidget.Height > rowHeight) {
					rowHeight = itemWidget.Height;
				}

				positions[i] = new Vector3(x + itemWidget.Width * itemWidget.pivot.x, y - itemWidget.Height * (1 - itemWidget.pivot.y), itemWidget.transform.localPosition.z);
				x += itemWidget.Width;
				if (x > totalWidth) {
					totalWidth = x;
				}
			}

			totalHeight = -(y - rowHeight);
			
			Vector3 offset = new Vector3(
				horizontalAlign * (widget.Width - totalWidth),
				totalHeight + verticalAlign * (widget.Height - totalHeight),
				0
			) + new Vector3(-widget.Width * widget.pivot.x, -widget.Height * widget.pivot.y, 0);

			for (int i = 0; i < positions.Length; ++i) {
				GameObject item = t.GetChild(i).gameObject;
				Widget itemWidget = t.GetChild(i).GetComponent<Widget>();
				if (itemWidget == null || !item.activeSelf) {
					continue;
				}

				positions[i] += offset;
				itemWidget.SetLocalPosition(positions[i]);
				itemWidget.RevalidateHierarchy();
			}
		}

		public void ResizeHeightToChildren(float minHeight = 0, float maxHeight = float.MaxValue) {
			float height = 0;
			Transform t = transform;
			for (int i = 0; i < t.childCount; ++i) {
				GameObject item = t.GetChild(i).gameObject;
				Widget itemWidget = item.GetComponent<Widget>();
				if (itemWidget == null || !item.activeSelf) {
					continue;
				}
				var h = itemWidget.GetLocalPosition().y - itemWidget.Height * itemWidget.pivot.y;
				height = Mathf.Max(height, -h);

			}
			var packWidget = GetComponent<Widget>();
			packWidget.Height = Mathf.Clamp(height, minHeight, maxHeight);
		}

		void Update() {
			if (revalidateOnUpdate || !Application.isPlaying) {
				Revalidate();
			}
		}
	}
}
