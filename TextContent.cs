using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Chui {

	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Text Content")]
	public class TextContent : Content {

		protected class LetterInfo {
			public float x;
			public float y;
			public float scale;
			public int index;
			public FontInfo.GlyphInfo glyph;

			public LetterInfo(float x, float y, float scale, int index, FontInfo.GlyphInfo glyph) {
				this.x = x;
				this.y = y;
				this.scale = scale;
				this.glyph = glyph;
				this.index = index;
			}
		}

		protected class RowInfo {
			public List<LetterInfo> letters = new List<LetterInfo>();
			public float width = 0;
		}

		protected class ColorTag {
			public enum Type {
				Push,
				Pop
			}

			public Type type;
			public int position;
			public Color32 color;
		}

		protected class SubmeshData {
			public int index;
			public int numCharacters;
			public int currentIndex;
			public Material material;

			public int[] indices;
		}

		public enum HorizontalAlign {
			Left,
			Center,
			Right
		}

		public enum VerticalAlign {
			Top,
			Center,
			Bottom
		}

		public enum ScaleMode {
			ScaleContent,
			ResizeWidget
		}

		[UnityEngine.Serialization.FormerlySerializedAs("rawText")]
		public string text;
		public FontInfo fontInfo;
		public bool isWordWrapEnabled;
		public bool isJustifyEnabled;
		public bool isTagEnabled;
		public float fontSize;
		public float lineSpacing = 1;
		public Color color = Color.white;
		public HorizontalAlign hAlign;
		public VerticalAlign vAlign;
		public ScaleMode scaleMode = ScaleMode.ScaleContent;

		string last_text;
		FontInfo last_fontInfo;
		bool last_isWordWrapEnabled;
		bool last_isJustifyEnabled;
		bool last_isTagEnabled;
		float last_fontSize;
		float last_lineSpacing;
		Color last_derivedColor;
		HorizontalAlign last_hAlign;
		VerticalAlign last_vAlign;
		ScaleMode last_scaleMode;

		protected Vector3[] vertices;
		protected Vector2[] uvs;
		protected Color32[] colors;

		float currentFontSize;
		Color derivedColor;
		string processedText = "";
		protected List<RowInfo> rows;
		protected List<ColorTag> colorTags;

		protected Dictionary<Material, SubmeshData> submeshesData = new Dictionary<Material, SubmeshData>();
		protected int numVertices;

		MeshFilter meshFilter;
		MeshRenderer meshRenderer;
		Mesh mesh;

		FontInfo.GlyphInfo spaceGlyph;

		public override void UpdateContent(Widget widget) {
			base.UpdateContent(widget);
			if (fontInfo == null) {
				return;
			}

			bool hasChanged = false;
			UpdateChanges(hasChanged);
		}


		protected virtual void UpdateChanges(bool hasChanged) {
			derivedColor = color * fontInfo.color * widget.derivedColor;

			if (text != last_text) {
				hasChanged = true;
				last_text = text;
			}

			if (fontInfo != last_fontInfo) {
				hasChanged = true;
				last_fontInfo = fontInfo;
				spaceGlyph = fontInfo.glyphs[(int)(' ')];
			}

			if (fontSize != last_fontSize || derivedColor != last_derivedColor || lineSpacing != last_lineSpacing) {
				hasChanged = true;
				last_fontSize = fontSize;
				last_derivedColor = derivedColor;
				last_lineSpacing = lineSpacing;
			}

			if (isWordWrapEnabled != last_isWordWrapEnabled || isJustifyEnabled != last_isJustifyEnabled || isTagEnabled != last_isTagEnabled) {
				hasChanged = true;
				last_isWordWrapEnabled = isWordWrapEnabled;
				last_isJustifyEnabled = isJustifyEnabled;
				last_isTagEnabled = isTagEnabled;
			}

			if (hAlign != last_hAlign || vAlign != last_vAlign || scaleMode != last_scaleMode) {
				hasChanged = true;
				last_hAlign = hAlign;
				last_vAlign = vAlign;
				last_scaleMode = scaleMode;
			}

			if (hasChanged || (widget.changes != 0)) {
				if (fontInfo != null) {
					hasChanged = false;

					colorTags = new List<ColorTag>();
					colorTags.Add(new ColorTag() { type = ColorTag.Type.Push, position = 0, color = derivedColor });

					if (isTagEnabled) {
						ParseText();
					} else {
						processedText = text;
					}
					Rebuild();
				}
			}
		}

		void ParseText() {
			Stack<string> parseStack = new Stack<string>();
			StringBuilder resultBuilder = new StringBuilder();
			parseStack.Push(text);
			while (parseStack.Count > 0) {
				string currentText = parseStack.Pop();

				string tag = "";
				bool isEscaping = false;
				bool isTag = false;
				List<string> args = new List<string>();
				StringBuilder argBuilder = new StringBuilder();
				StringBuilder b = new StringBuilder();
				for (int i = 0; i < currentText.Length; ++i) {
					if (isTag) {
						if (isEscaping) {
							argBuilder.Append(currentText[i]);
							isEscaping = false;
						} else if (currentText[i] == ':') {
							args.Add(argBuilder.ToString());
							argBuilder.Length = 0;
						} else if (currentText[i] == ']') {
							args.Add(argBuilder.ToString());
							argBuilder.Length = 0;
							isTag = false;

							if (args.Count == 2 && args[0] == "icon") {
								FontInfo.IconSource icon = fontInfo.iconSources.Find(e => e.code == args[1]);
								if (icon != null) {
									b.Append((char)(icon.id));
								}
							} else if (args.Count == 2 && args[0] == "color") {
								int intColor = 0;
								if (int.TryParse(args[1], System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out intColor)) {

									colorTags.Add(new ColorTag() { type = ColorTag.Type.Push, position = resultBuilder.Length + b.Length, color = Util.IntToColor32(intColor) });
								}
							} else if (args.Count == 1 && args[0] == "/color") {
								colorTags.Add(new ColorTag() { type = ColorTag.Type.Pop, position = resultBuilder.Length + b.Length });
							} else if (args.Count >= 2 && args[0] == "inject") {
								parseStack.Push(currentText.Substring(i + 1));
								parseStack.Push(TextTagInjector.GetInjectedText(args[1], args));
								break;
							}

							args.Clear();
						} else if (currentText[i] == '\\') {
							isEscaping = true;
						} else {
							argBuilder.Append(currentText[i]);
						}
					} else {
						if (isEscaping) {
							b.Append(currentText[i]);
							isEscaping = false;
						} else if (currentText[i] == '[') {
							isTag = true;
						} else if (currentText[i] == '\\') {
							isEscaping = true;
						} else {
							b.Append(currentText[i]);
						}
					}
				}
				resultBuilder.Append(b);
			}
			processedText = resultBuilder.ToString();
		}

		protected virtual void PrepareSubmeshes() {
			submeshesData.Clear();
			for (int i = 0; i < rows.Count; ++i) {
				for (int j = 0; j < rows[i].letters.Count; ++j) {
					LetterInfo l = rows[i].letters[j];
					FontInfo.GlyphInfo g = l.glyph;
					if (g == null) {
						continue;
					}

					SubmeshData sd = null;
					if (!submeshesData.TryGetValue(g.material, out sd)) {
						sd = new SubmeshData();
						sd.index = submeshesData.Count;
						sd.material = g.material;
						submeshesData[g.material] = sd;
					}
					sd.numCharacters++;
				}
			}
			mesh.subMeshCount = submeshesData.Count;

			numVertices = 0;
			foreach (var item in submeshesData) {
				item.Value.indices = new int[item.Value.numCharacters * 6];
				numVertices += item.Value.numCharacters * 4;
			}

			vertices = new Vector3[numVertices];
			uvs = new Vector2[numVertices];
			colors = new Color32[numVertices];
		}

		public virtual void PrepareMaterials() {
			Material[] materials = new Material[submeshesData.Count];
			foreach (var item in submeshesData) {
				materials[item.Value.index] = item.Value.material;
			}
			meshRenderer.materials = materials;
		}

		void PrepareMesh() {
			if (mesh == null) {
				mesh = new Mesh();
			}
			mesh.Clear();

			if (meshFilter == null) {
				meshFilter = GetComponent<MeshFilter>();
				if (meshFilter == null) {
					meshFilter = gameObject.AddComponent<MeshFilter>();
					meshFilter.hideFlags = HideFlags.NotEditable;
				}
			}
			meshFilter.sharedMesh = mesh;

			if (meshRenderer == null) {
				meshRenderer = GetComponent<MeshRenderer>();
				if (meshRenderer == null) {
					meshRenderer = gameObject.AddComponent<MeshRenderer>();
					meshRenderer.hideFlags = HideFlags.NotEditable;
				}
			}

			PrepareSubmeshes();
		}

		class WordWrapInfo {
			public enum Type {
				Undefined,
				Text,
				WeakWhiteSpace,
				HardWhiteSpace,
				NewLines
			}

			public Type type;
			public int begin;
			public int end;
			public float width;
		}

		List<WordWrapInfo> words;

		void PrepareWordWrappedRows(int scaleIterations) {
			if (spaceGlyph == null) {
				return;
			}

			if (words == null) {
				words = new List<WordWrapInfo>();
			} else {
				words.Clear();
			}

			// Split string to words and blocks of hard whitespace (newline or more than one space)
			WordWrapInfo.Type state = WordWrapInfo.Type.Undefined;
			int lastItemBegin = 0;
			for (int i = 0; i < processedText.Length; ++i) {
				switch (state) {
					case WordWrapInfo.Type.Text:
						if (char.IsWhiteSpace(processedText[i])) {
							words.Add(new WordWrapInfo() { type = state, begin = lastItemBegin, end = i });
							lastItemBegin = i;
							if (processedText[i] == ' ') {
								state = WordWrapInfo.Type.WeakWhiteSpace;
							} else if (processedText[i] == '\n') {
								state = WordWrapInfo.Type.NewLines;
							} else {
								state = WordWrapInfo.Type.HardWhiteSpace;
							}
						}
						break;
					case WordWrapInfo.Type.WeakWhiteSpace:
						if (char.IsWhiteSpace(processedText[i])) {
							if (processedText[i] == '\n') {
								state = WordWrapInfo.Type.NewLines;
							} else {
								state = WordWrapInfo.Type.HardWhiteSpace;
							}
						} else {
							lastItemBegin = i;
							state = WordWrapInfo.Type.Text;
						}
						break;
					case WordWrapInfo.Type.HardWhiteSpace:
						if (processedText[i] == '\n') {
							state = WordWrapInfo.Type.NewLines;
						} else if (!char.IsWhiteSpace(processedText[i])) {
							words.Add(new WordWrapInfo() { type = state, begin = lastItemBegin, end = i });
							lastItemBegin = i;
							state = WordWrapInfo.Type.Text;
						}
						break;
					case WordWrapInfo.Type.NewLines:
						if (!char.IsWhiteSpace(processedText[i])) {
							words.Add(new WordWrapInfo() { type = state, begin = lastItemBegin, end = i });
							lastItemBegin = i;
							state = WordWrapInfo.Type.Text;
						}
						break;
					case WordWrapInfo.Type.Undefined:
						if (char.IsWhiteSpace(processedText[i])) {
							if (processedText[i] == ' ') {
								state = WordWrapInfo.Type.WeakWhiteSpace;
							} else if (processedText[i] == '\n') {
								state = WordWrapInfo.Type.NewLines;
							} else {
								state = WordWrapInfo.Type.HardWhiteSpace;
							}
						} else {
							state = WordWrapInfo.Type.Text;
						}
						break;
				}
			}
			if (state != WordWrapInfo.Type.WeakWhiteSpace) {
				words.Add(new WordWrapInfo() { type = state, begin = lastItemBegin, end = processedText.Length });
			}

			FontInfo.GlyphInfo glyph;

			for (int i = 0; i < words.Count; ++i) {
				if (words[i].type != WordWrapInfo.Type.NewLines) {
					float wx = 0;
					float wordWidth = 0;
					for (int j = words[i].begin; j < words[i].end; ++j) {
						if (fontInfo.glyphs.TryGetValue((int)(processedText[j]), out glyph)) {
							wordWidth = wx + glyph.geom.width;
							wx += glyph.advance;
						}
					}
					words[i].width = wordWidth;
				}
			}

			float lowerFontSize = 0;
			float upperFontSize = fontSize * fontInfo.fontSize;
			currentFontSize = upperFontSize;
			while (scaleIterations-- > 0) {
				rows.Clear();
				RowInfo row = new RowInfo();
				rows.Add(row);

				bool needsSpace = false;
				float spaceWidth = spaceGlyph.advance * currentFontSize;
				float px = 0;
				float py = 0;
				int previousCharacter = -1;
				for (int i = 0; i < words.Count; ++i) {
					needsSpace = needsSpace && words[i].type == WordWrapInfo.Type.Text;
					float wordWidth = words[i].width * currentFontSize;

					bool wordFits = false;
					if (wordWidth > widget.volume.Width) {
						wordFits = true;
					} else if (px + wordWidth + (needsSpace ? spaceWidth : 0) <= widget.volume.Width) {
						wordFits = true;
					}

					if (wordFits) {
						if (needsSpace) {
							row.letters.Add(new LetterInfo(px, py, currentFontSize, 0, null));
							px += spaceWidth;
						}

						needsSpace = words[i].type == WordWrapInfo.Type.Text;

						for (int j = words[i].begin; j < words[i].end; ++j) {
							if (processedText[j] == '\n') {
								px = 0;
								py -= fontInfo.baseSize * currentFontSize * lineSpacing;
								needsSpace = false;
								row = new RowInfo();
								rows.Add(row);
								previousCharacter = -1;
							} else if (fontInfo.glyphs.TryGetValue((int)(processedText[j]), out glyph)) {
								float kerning = 0;
								if (previousCharacter != -1) {
									kerning = fontInfo.GetKerning(previousCharacter, (int)processedText[i]);
								}
								px += kerning * currentFontSize;
								row.letters.Add(new LetterInfo(px, py, currentFontSize, j, glyph));
								px += glyph.advance * currentFontSize;
								previousCharacter = (int)processedText[j];
							}
						}
					} else {
						px = 0;
						py -= fontInfo.baseSize * currentFontSize * lineSpacing;
						needsSpace = false;
						row = new RowInfo();
						rows.Add(row);
						--i;
					}
				}

				float height = rows.Count * fontInfo.baseSize * currentFontSize;
				if (height != 0 && widget.volume.Height != 0 && scaleIterations > 0) {
					if (height > widget.volume.Height) {
						if (currentFontSize != upperFontSize) {
							upperFontSize = currentFontSize;
						}
					} else {
						if (currentFontSize == upperFontSize) {
							break;
						} else {
							lowerFontSize = currentFontSize;
						}
					}
					currentFontSize = (lowerFontSize + upperFontSize) * 0.5f;
				} else {
					break;
				}
			}
		}

		void PreparePlainRows() {
			if (spaceGlyph == null) {
				return;
			}
			if (processedText == null) {
				return;
			}

			RowInfo row = new RowInfo();
			rows.Add(row);

			currentFontSize = fontSize * fontInfo.fontSize;

			float px = 0;
			float py = 0;
			int previousCharacter = -1;
			for (int i = 0; i < processedText.Length; ++i) {
				FontInfo.GlyphInfo glyph;
				if (processedText[i] == ' ') {
					row.letters.Add(new LetterInfo(px, py, currentFontSize, i, null));
					px += spaceGlyph.advance * currentFontSize;
					previousCharacter = -1;
				} else if (processedText[i] == '\n') {
					px = 0;
					py -= fontInfo.baseSize * currentFontSize * lineSpacing;
					row = new RowInfo();
					rows.Add(row);
					previousCharacter = -1;
				} else if (fontInfo.glyphs.TryGetValue((int)(processedText[i]), out glyph)) {
					float kerning = 0;
					if (previousCharacter != -1) {
						kerning = fontInfo.GetKerning(previousCharacter, (int)processedText[i]);
					}
					px += kerning * currentFontSize;
					row.letters.Add(new LetterInfo(px, py, currentFontSize, i, glyph));
					px += glyph.advance * currentFontSize;
					previousCharacter = (int)processedText[i];
				}
			}
		}

		void JustifyRows() {
			for (int i = 0; i < rows.Count; ++i) {
				int numSpaces = 0;
				for (int j = 0; j < rows[i].letters.Count; ++j) {
					if (rows[i].letters[j].glyph == null) {
						numSpaces++;
					}
				}
				if (numSpaces > 0) {
					float extraSpace = (widget.volume.Width - rows[i].width) / numSpaces;
					float shift = 0;
					for (int j = 0; j < rows[i].letters.Count; ++j) {
						rows[i].letters[j].x += shift;
						if (rows[i].letters[j].glyph == null) {
							shift += extraSpace;
						}
					}
					rows[i].width = widget.volume.Width;
				}
			}
		}

		Pair<float, float> CalculateRowsBounds() {
			bool first = true;
			float yMin = 0;
			float yMax = 0;
			int numChecks = 0;
			for (int i = 0; i < rows.Count; ++i) {
				if (rows[i].letters.Count > 0) {
					for (int j = 0; j < rows[i].letters.Count; ++j) {
						LetterInfo l = rows[i].letters[j];
						if (l.glyph != null) {
							if (first) {
								yMax = l.y + l.glyph.geom.yMax * l.scale;
								first = false;
							} else {
								float y = l.y + l.glyph.geom.yMax * l.scale;
								if (y > yMax) {
									yMax = y;
								}
							}
							numChecks++;
						}
					}
					if (!first) {
						break;
					}
				}
			}

			first = true;
			for (int i = rows.Count - 1; i >= 0; --i) {
				if (rows[i].letters.Count > 0) {
					for (int j = 0; j < rows[i].letters.Count; ++j) {
						LetterInfo l = rows[i].letters[j];
						if (l.glyph != null) {
							if (first) {
								yMin = l.y + l.glyph.geom.yMin * l.scale;
								first = false;
							} else {
								float y = l.y + l.glyph.geom.yMin * l.scale;
								if (y < yMin) {
									yMin = y;
								}
							}
							numChecks++;
						}
					}
					if (!first) {
						break;
					}
				}
			}
			return new Pair<float, float>(yMin, yMax);
		}

		void PrepareLetters() {
			if (rows == null) {
				rows = new List<RowInfo>();
			} else {
				rows.Clear();
			}

			if (isWordWrapEnabled) {
				//NOTE:
				// scaleIteration of 1 means no height-based adjustment at all
				int scaleIterations = 1;
				if (scaleMode == ScaleMode.ScaleContent) {
					scaleIterations = 8;
				}
				PrepareWordWrappedRows(scaleIterations);
			} else {
				PreparePlainRows();
			}

			float maxWidth = 0;
			for (int i = 0; i < rows.Count; ++i) {
				for (int j = rows[i].letters.Count - 1; j >= 0; --j) {
					LetterInfo lastLetter = rows[i].letters[j];
					if (lastLetter.glyph != null) {
						rows[i].width = lastLetter.x + lastLetter.glyph.geom.width * lastLetter.scale;
						if (rows[i].width > maxWidth) {
							maxWidth = rows[i].width;
						}
						break;
					}
				}
			}

			Pair<float, float> rowsBounds = CalculateRowsBounds();
			float height = rowsBounds.nd - rowsBounds.st;
			float scale = 1;

			if (scaleMode == ScaleMode.ScaleContent) {
				scale = Mathf.Min(1, Mathf.Min(widget.volume.Width / maxWidth, widget.volume.Height / height));
				height *= scale;
				if (scale != 1) {
					for (int i = 0; i < rows.Count; ++i) {
						rows[i].width *= scale;
						for (int j = 0; j < rows[i].letters.Count; ++j) {
							rows[i].letters[j].x *= scale;
							rows[i].letters[j].y *= scale;
							rows[i].letters[j].scale *= scale;
						}
					}
				}

			} else if (scaleMode == ScaleMode.ResizeWidget) {
				if (isWordWrapEnabled) {
					if (Mathf.Abs(widget.volume.Height - height) > 0.01f) {
						widget.Height = height;
						widget.RevalidateHierarchy();
						return;
					}
				} else if (Mathf.Abs(widget.volume.Width - maxWidth) > 0.01f || Mathf.Abs(widget.volume.Height - height) > 0.01f) {
					widget.Width = maxWidth;
					widget.Height = height;
					widget.RevalidateHierarchy();
					//NOTE:
					// Letters are already reseted and formatted in recursive revalidation.
					// Formatting them once more without a reset would mess them up!
					return;
				}
			}

			if (isJustifyEnabled) {
				JustifyRows();
			}

			float yoffset = 0;
			if (vAlign == VerticalAlign.Center) {
				yoffset = -(widget.volume.Height - height) / 2;
			} else if (vAlign == VerticalAlign.Bottom) {
				yoffset = -(widget.volume.Height - height);
			}
			yoffset += widget.volume.yMax - rowsBounds.nd * scale;

			for (int i = 0; i < rows.Count; ++i) {
				float xoffset = 0;
				if (hAlign == HorizontalAlign.Center) {
					xoffset = (widget.volume.Width - rows[i].width) / 2;
				} else if (hAlign == HorizontalAlign.Right) {
					xoffset = widget.volume.Width - rows[i].width;
				}
				xoffset += widget.volume.xMin;
				for (int j = 0; j < rows[i].letters.Count; ++j) {
					rows[i].letters[j].x += xoffset;
					rows[i].letters[j].y += yoffset;
				}
			}
		}

		virtual protected void BuildGeometry() {
			Stack<Color32> colorStack = new Stack<Color32>();
			int currentColorTag = 0;

			int index = 0;
			int vertex = 0;
			for (int i = 0; i < rows.Count; ++i) {
				for (int j = 0; j < rows[i].letters.Count; ++j) {
					LetterInfo l = rows[i].letters[j];
					FontInfo.GlyphInfo glyph = l.glyph;

					while (currentColorTag < colorTags.Count && l.index >= colorTags[currentColorTag].position) {
						ColorTag ct = colorTags[currentColorTag++];
						if (ct.type == ColorTag.Type.Push) {
							colorStack.Push(ct.color);
						} else {
							if (colorStack.Count > 1) { // disable overpop
								colorStack.Pop();
							}
						}
					}

					if (glyph == null) {
						continue;
					}

					Color32 color = Color.white;
					if (glyph.isColorEnabled) {
						color = colorStack.Peek();
					} else {
						color.a = colorStack.Peek().a;
					}

					SubmeshData sd = submeshesData[glyph.material];

					if (sd.currentIndex + 5 > sd.indices.Length) {
						Debug.Log("Fuckup " + sd.currentIndex + " ; " + sd.indices.Length, gameObject);
						return;
					}
					sd.indices[sd.currentIndex++] = vertex + 0;
					sd.indices[sd.currentIndex++] = vertex + 1;
					sd.indices[sd.currentIndex++] = vertex + 2;
					sd.indices[sd.currentIndex++] = vertex + 2;
					sd.indices[sd.currentIndex++] = vertex + 3;
					sd.indices[sd.currentIndex++] = vertex + 0;

					uvs[vertex] = new Vector2(glyph.uv.xMin, glyph.uv.yMin);
					colors[vertex] = color;
					vertices[vertex++] = new Vector3(l.x + glyph.geom.xMin * l.scale, l.y + glyph.geom.yMax * l.scale, 0);

					uvs[vertex] = new Vector2(glyph.uv.xMax, glyph.uv.yMin);
					colors[vertex] = color;
					vertices[vertex++] = new Vector3(l.x + glyph.geom.xMax * l.scale, l.y + glyph.geom.yMax * l.scale, 0);

					uvs[vertex] = new Vector2(glyph.uv.xMax, glyph.uv.yMax);
					colors[vertex] = color;
					vertices[vertex++] = new Vector3(l.x + (glyph.geom.xMax + fontInfo.skew) * l.scale, l.y + glyph.geom.yMin * l.scale, 0);

					uvs[vertex] = new Vector2(glyph.uv.xMin, glyph.uv.yMax);
					colors[vertex] = color;
					vertices[vertex++] = new Vector3(l.x + (glyph.geom.xMin + fontInfo.skew) * l.scale, l.y + glyph.geom.yMin * l.scale, 0);
				}
			}
		}

		void BuildMesh() {
			mesh.vertices = vertices;
			mesh.uv = uvs;
			mesh.colors32 = colors;

			foreach (var item in submeshesData) {
				mesh.SetTriangles(item.Value.indices, item.Value.index);
			}

			mesh.RecalculateBounds();
		}

		void UpdateSettings() {
		}

		void OnDestroy() {

#if UNITY_EDITOR

			//NOTE
			// When pressing play button, Unity calls OnDestroy on edit-mode instance of component just before
			// calling Awake on play-mode instance. The meshFilter and meshRenderer are for some reason shared
			// between edit and play mode instances, so delayedCall for edit-mode would destroy play-mode components.
			//
			bool wasPlaying = Application.isPlaying;
			if (meshFilter != null) {
				EditorApplication.delayCall += () => {
					if (Application.isPlaying == wasPlaying && meshFilter) {
						Object.DestroyImmediate(meshFilter);
					}
				};
			}
			if (meshRenderer != null) {
				EditorApplication.delayCall += () => {
					if (Application.isPlaying == wasPlaying && meshRenderer) {
						Object.DestroyImmediate(meshRenderer);
					}
				};
			}
#else
			if (meshFilter != null) {
				Object.Destroy(meshFilter);
			}
			if (meshRenderer != null) {
				Object.Destroy(meshRenderer);
			}
#endif

		}

		void Rebuild() {
			UpdateSettings();
			PrepareLetters();
			PrepareMesh();
			PrepareMaterials();
			BuildGeometry();
			BuildMesh();
		}

		public override void ReloadContent() {
		}
	}
}
