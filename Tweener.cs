﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Chui {
	[AddComponentMenu("Chui/Tweener")]
	public class Tweener : MonoBehaviour {
		static Tweener instance;
		
		Tween root;
		Tween lastTween;
		
		void Awake() {
			instance = this;
			root = new Tween();	
			lastTween = root;	
		}
		
		void Update() {
			Tween t = root.nextTween;
			while (t != null) {
				t.Update();
				t = t.nextTween;
			}
		}
		
		void AddTween(Tween tween) {
			lastTween.nextTween = tween;
			tween.previousTween = lastTween;
			lastTween = tween;
		}
		void RemoveTween(Tween tween) {
			tween.previousTween.nextTween = tween.nextTween;
			if (tween.nextTween != null) {
				tween.nextTween.previousTween = tween.previousTween;
			} else {
				lastTween = tween.previousTween;
			}
		}
		
		public static FixedPositionTween TweenPosition(GameObject target, float length, AnimationCurve curve, Vector3 begin, Vector3 end, Action callback = null) {
			FixedPositionTween tween = new FixedPositionTween();
			tween.BaseSetup(target, length, curve);
			tween.begin = begin;
			tween.end = end;
			tween.callback = callback;
			instance.AddTween(tween);

			return tween;
		}

		public static AlphaTween TweenAlpha(GameObject target, float length, AnimationCurve curve, float begin, float end, Action callback = null) {
			AlphaTween tween = new AlphaTween();
			tween.BaseSetup(target, length, curve);
			tween.begin = begin;
			tween.end = end;
			tween.callback = callback;
			instance.AddTween(tween);

			return tween;
		}
		
		public static void Kill(Tween tween) {
			if (tween.isActive) {
				tween.isActive = false;
				instance.RemoveTween(tween);
			}
		}
	
	}
	
	public class Tween {
		public Tween previousTween;
		public Tween nextTween;
		
		public bool isActive;
		public GameObject target;
		public Widget targetWidget;
		public AnimationCurve curve;
		public Action callback;
		protected float startTime;
		protected float endTime;	
		protected float progress;
		protected float value;	
		
		public void BaseSetup(GameObject target, float length, AnimationCurve curve) {
			this.target = target;
			this.curve = curve;
			targetWidget = target.GetComponent<Chui.Widget>();
			startTime = Time.time;
			endTime = startTime + length;
			isActive = true;
		}
		
		public void Update() {
			if (isActive) {
				progress = Mathf.Clamp01((Time.time - startTime) / (endTime - startTime));					
				value = curve.Evaluate(progress);
	
				Evaluate();		
			
				if (progress >= 1) {
					Tweener.Kill(this);
					if (callback != null) {
						callback();
					}
				}
			}
		}
		
		public void FinishImmediately() {
			if (isActive) {
				progress = 1;
				value = curve.Evaluate(1);
			
				Evaluate();
			
				Tweener.Kill(this);
			}
		}
		
		public void Kill() {
			Tweener.Kill(this);
		}
		
		protected virtual void Evaluate() {
			
		}
	}
	
	public class FixedPositionTween : Tween {
		public Vector3 begin;
		public Vector3 end;
		
		protected override void Evaluate() {
			base.Evaluate();
			
			Vector3 position = begin + (end - begin) * value;
			if (targetWidget != null) {
				targetWidget.Revalidate();
				targetWidget.SetWorldPosition(position);
			} else {
				target.transform.position = position;
			}
		}
		
	}

	public class AlphaTween : Tween {
		public float begin;
		public float end;

		public AlphaTween() {
		}

		protected override void Evaluate() {
			base.Evaluate();

			float alpha = begin + (end - begin) * value;
			if (targetWidget != null) {
				targetWidget.color.a = alpha;
			}
		}
	}
}
