﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Chui {

	[AddComponentMenu("Chui/Round Text Content")]
	public class RoundTextContent : TextContent {

		public float angle = 0f;
		public float radius = 400f;
		public float spacing = 1f;

		float last_angle;
		float last_radius;
		float last_spacing;

		protected override void UpdateChanges (bool hasChanged)
		{

			if (radius != last_radius) {
				hasChanged = true;
				last_radius = radius;
			}
			if (angle != last_angle) {
				hasChanged = true;
				last_angle = angle;
			}
			if (spacing != last_spacing) {
				hasChanged = true;
				last_spacing = spacing;
			}
			base.UpdateChanges (hasChanged);
		}

		override protected void BuildGeometry() {
			
			
			Stack<Color32> colorStack = new Stack<Color32>();
			int currentColorTag = 0;
			
			int index = 0;
			int vertex = 0;
			for (int i = 0; i < rows.Count; ++i) {
				
				for (int j = 0; j < rows[i].letters.Count; ++j) {
					LetterInfo l = rows[i].letters[j];
					FontInfo.GlyphInfo glyph = l.glyph;
					
					while (currentColorTag < colorTags.Count && l.index >= colorTags[currentColorTag].position) {
						ColorTag ct = colorTags[currentColorTag++];
						if (ct.type == ColorTag.Type.Push) {
							colorStack.Push(ct.color);
						} else {
							if (colorStack.Count > 1) { // disable overpop
								colorStack.Pop();
							}
						}
					}
					
					if (glyph == null) {
						continue;
					}
					
					Color32 color = Color.white;
					if (glyph.isColorEnabled) {
						color = colorStack.Peek();
					} else {
						color.a = colorStack.Peek().a;
					}
					
					SubmeshData sd = submeshesData[glyph.material];

					sd.indices[sd.currentIndex++] = vertex + 0;
					sd.indices[sd.currentIndex++] = vertex + 1;
					sd.indices[sd.currentIndex++] = vertex + 2;
					sd.indices[sd.currentIndex++] = vertex + 2;
					sd.indices[sd.currentIndex++] = vertex + 3;
					sd.indices[sd.currentIndex++] = vertex + 0;
					
					float w = (glyph.geom.xMax - glyph.geom.xMin) * l.scale;
					float x = l.x + glyph.geom.xMin * l.scale + w / 2;
					float letterAngle = angle * Mathf.Deg2Rad + Mathf.PI / 2 - spacing * x / radius;
					
					
					Vector3 v = new Vector3((radius + l.y) * Mathf.Cos(letterAngle), (radius + l.y)* Mathf.Sin(letterAngle), 0);
					Vector3 p0 = new Vector3(-w/2, glyph.geom.yMax * l.scale, 0f);
					Vector3 p1 = new Vector3(w/2, glyph.geom.yMax * l.scale, 0f);
					Vector3 p2 = new Vector3(w/2, glyph.geom.yMin * l.scale, 0f);
					Vector3 p3 = new Vector3(-w/2, glyph.geom.yMin * l.scale, 0f);
					Quaternion rot =  Quaternion.Euler(0, 0, Mathf.Rad2Deg * (letterAngle - Mathf.PI / 2));
					
					uvs[vertex] = new Vector2(glyph.uv.xMin, glyph.uv.yMin);
					colors[vertex] = color;
					vertices[vertex++] = v + rot * p0;
					
					uvs[vertex] = new Vector2(glyph.uv.xMax, glyph.uv.yMin);
					colors[vertex] = color;
					vertices[vertex++] = v + rot * p1;
					
					uvs[vertex] = new Vector2(glyph.uv.xMax, glyph.uv.yMax);
					colors[vertex] = color;
					vertices[vertex++] = v + rot * p2;
					
					uvs[vertex] = new Vector2(glyph.uv.xMin, glyph.uv.yMax);
					colors[vertex] = color;
					vertices[vertex++] = v + rot * p3;

				}
			}
		}
	}
}
