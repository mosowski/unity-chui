﻿using UnityEngine;
using System.Collections;

namespace Chui {

	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Prefab Content")]
	public class PrefabContent : Content {

		public enum ZScaleMode {
			Min,
			Depth
		}

		public bool isContainerLocked = false;

		public GameObject prefab;
		public GameObject prefabContainer;

		public bool isScaleXEnabled = true;
		public bool isScaleYEnabled = true;
		public bool isScaleZEnabled = false;
		public ZScaleMode zScaleMode = ZScaleMode.Depth;

		GameObject prefabInstance;
		PrefabContentInfo prefabContentInfo;

		public GameObject PrefabInstance {
			get {
				return prefabInstance;
			}
		}

		void CreatePrefabContainer() {
			if (prefabContainer == null) {
				Transform pt = transform.Find("@chui-prefab-content");
				if (pt != null) {
					prefabContainer = pt.gameObject;
				} else {
					prefabContainer = new GameObject();
					prefabContainer.name = "@chui-prefab-content";
					prefabContainer.transform.SetParent(transform, false);
				}
				prefabContainer.transform.localPosition = Vector3.zero;
			}
		}

		void CreatePrefabInstance() {
			if (prefabInstance == null) {
				if (prefabContainer.transform.childCount > 0) {
					prefabInstance = prefabContainer.transform.GetChild(0).gameObject;
				} else {
					if (prefab != null) {
						prefabInstance = Instantiate(prefab) as GameObject;
					}
				}
				if (prefabInstance != null) {
					prefabInstance.transform.SetParent(prefabContainer.transform, false);
					prefabInstance.transform.localPosition = Vector3.zero;
					prefabContentInfo = prefabInstance.GetComponent<PrefabContentInfo>();
				}
			}
		}

		void ScalePrefabContainer() {
			if (prefabInstance != null) {
				if (prefabContentInfo.height != 0 && prefabContentInfo.width != 0) {
					Transform pt = prefabContainer.transform;

					float targetWidth = isScaleXEnabled ? widget.volume.Width : prefabContentInfo.width;
					float targetHeight = isScaleYEnabled ? widget.volume.Height : prefabContentInfo.height;
					float shiftX = isScaleXEnabled ? -widget.pivot.x * targetWidth : 0;
					float shiftY = isScaleYEnabled ? -widget.pivot.y * targetHeight : 0;

					pt.localPosition = new Vector3(
						shiftX,
						shiftY,
						-widget.pivot.z * widget.volume.Depth
					);

					Vector3 localScale = new Vector3(
						targetWidth / prefabContentInfo.width,
						targetHeight / prefabContentInfo.height,
						1
					);

					if (isScaleZEnabled) {
						if (zScaleMode == ZScaleMode.Min) {
							localScale.z = Mathf.Min(localScale.x, localScale.y);
						} else if (zScaleMode == ZScaleMode.Depth) {
							localScale.z = Mathf.Abs(widget.volume.Depth / prefabContentInfo.depth);
						}
					}

					pt.localScale = localScale;
				}
			}
		}

		public override void UpdateContent(Widget widget) {
			base.UpdateContent(widget);

			CreatePrefabContainer();
			CreatePrefabInstance();
			ScalePrefabContainer();
		}

		public override void ReloadContent() {
			if (prefabContainer != null && !isContainerLocked) {
				if (Application.isPlaying) {
					Destroy(prefabContainer);
					prefabContainer = null;
					prefabInstance = null;
				} else {
					DestroyImmediate(prefabContainer);
					prefabContainer = null;
					prefabInstance = null;
				}
			}
		}

		public void SetPrefabInstance(GameObject instance) {
			prefabInstance = instance;
			CreatePrefabContainer();
			prefabInstance.transform.SetParent(prefabContainer.transform, false);
			prefabContentInfo = prefabInstance.GetComponent<PrefabContentInfo>();
			widget = GetComponent<Widget>();
			ScalePrefabContainer();
		}

		void OnDestroy() {
		}
	}
}
