﻿using UnityEngine;
using UnityEditor;
using Chui;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(SLBXButton))]
public class SLBXButtonEditor : Editor {
	
	SLBXButton button;
	bool eventFoldout;
	bool textFoldout = true;

	public override void OnInspectorGUI() {
		button = target as SLBXButton;

		
		SLBXButtonSettings settings = (SLBXButtonSettings)EditorGUILayout.ObjectField("Settings", button.settings, typeof(SLBXButtonSettings), false);
		if (settings != button.settings) {
			Undo.RecordObject(button, "Set Settings");
			button.settings = settings;
			button.Refresh();
			EditorUtility.SetDirty(button);
		}

		if (settings != null) {
			GUILayout.Label("State:");
			
			string[] options = settings.states.Select(s => s.id).ToArray();
			EditorGUI.BeginChangeCheck();
			int selected = settings.states.FindIndex(s => s.id == button.State);
			selected = EditorGUILayout.Popup(selected, options);
			if (EditorGUI.EndChangeCheck()) {
				button.State = options[selected];
			}
		}

		bool isDisabled = EditorGUILayout.Toggle("Is Disabled", button.isDisabled);
		if (isDisabled != button.isDisabled) {
			Undo.RecordObject(button, "Toggle Disabled");
            button.isDisabled = isDisabled;
			button.Refresh();           
			EditorUtility.SetDirty(button);
		}

		bool isTouchEnabled = EditorGUILayout.Toggle("Is Touch Enabled", button.isTouchEnabled);
		if (isTouchEnabled != button.isTouchEnabled) {
			Undo.RecordObject(button, "Toggle Touch Enabled");
			button.isTouchEnabled = isTouchEnabled;
			button.Refresh();
			EditorUtility.SetDirty(button);
		}

		EditorGUILayout.LabelField("Button text");
		string text = EditorGUILayout.TextArea(button.text);
		if (text != button.text) {
			Undo.RecordObject(button, "Set Text Value");
			button.text = text;
			button.Refresh();
			EditorUtility.SetDirty(button);
		}

		EditorGUILayout.ObjectField("Animator (read only)", button.animator, typeof(Animator), false);

        if(button.Instance != null)
        {
            SLBXButtonParameters param = button.Instance.GetComponent<SLBXButtonParameters>();
            if(param != null)
            {
                SLBXButtonParametersEditor parameterEditor = SLBXButtonParametersEditor.GetEditor(param);
                if(parameterEditor != null)
                    parameterEditor.ShowParametersUI(button, param);
            }
        }

		if (button.prefab != null) {
			if (GUILayout.Button("Revalidate Prefab")) {
				button.Revalidate();
			}
		}


	}


}
