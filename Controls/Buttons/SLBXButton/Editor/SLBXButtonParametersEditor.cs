﻿#region Using statements

using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Reflection;

#endregion

[AttributeUsage(AttributeTargets.Class)]
public class SLBXParameters : System.Attribute
{

    public SLBXParameters(Type type)
    {
        ParameterType = type;
    }

    public Type ParameterType
    {
        get; private set;
    }

}

public class SLBXButtonParametersEditor 
{

    #region Virtual methods

    public virtual void ShowParametersUI(SLBXButton button, SLBXButtonParameters parameters)
    {
    }

    #endregion

    #region Static methods

    public static SLBXButtonParametersEditor GetEditor(SLBXButtonParameters parameters)
    {
        Type parType = parameters.GetType();

        Type baseType = typeof(SLBXButtonParametersEditor);
        Assembly assembly = baseType.Assembly;
        Type[] allTypes = assembly.GetTypes();

        for(int i = 0; i < allTypes.Length; ++i)
        {
            if(!allTypes[i].IsSubclassOf(baseType))
                continue;

            object[] allAttributes = allTypes[i].GetCustomAttributes(typeof(SLBXParameters), false);
            for(int j = 0; j < allAttributes.Length; ++j)
            {
                SLBXParameters p = allAttributes[j] as SLBXParameters;
                if(p != null && p.ParameterType == parType)
                    return Activator.CreateInstance(allTypes[i]) as SLBXButtonParametersEditor;
            }
        }

        return null;
    }

    #endregion

    #region Protected methods

    protected void ShowTextParameterField(SLBXButton button, string key, string label, string defaultValue = "")
    {
        string value = button.parameterContainer.Get(key, defaultValue);
        value = EditorGUILayout.TextField(label, value);
        if(button.parameterContainer.Set(key, value))
            EditorUtility.SetDirty(button);
    }

    protected void ShowColorParameterField(SLBXButton button, string key, string label)
    {
        ShowColorParameterField(button, key, label, Color.white);
    }

    protected void ShowColorParameterField(SLBXButton button, string key, string label, Color defaultColor)
    {
        string hex = button.parameterContainer.Get(key, "");
        Color color = ColorHelper.FromHex(hex, defaultColor);
        color = EditorGUILayout.ColorField(label, color);
        hex = ColorHelper.ToHex(color);

        if(button.parameterContainer.Set(key, hex))
            EditorUtility.SetDirty(button);
    }

    protected void ShowObjectParameter<T>(SLBXButton button, string key, string label) where T : UnityEngine.Object
    {
        string oldValue = button.parameterContainer.Get(key, "");
        T obj = Resources.Load<T>(oldValue) as T;
        T newObj = EditorGUILayout.ObjectField(label, obj, typeof(T), false) as T;
        string newValue = "";
        if(newObj != null)
        {
            string assetPath = AssetDatabase.GetAssetPath(newObj);
            string resPath = assetPath.Replace("Assets/Resources/", "");

            string dir = Path.GetDirectoryName(resPath);
            string name = Path.GetFileNameWithoutExtension(resPath);
            newValue = Path.Combine(dir, name);
        }

        if(button.parameterContainer.Set(key, newValue))
            EditorUtility.SetDirty(button);
    }

    protected void ShowFloatParameter(SLBXButton button, string key, string label, float defaultValue = 0.0f)
    {
        float oldValue = button.parameterContainer.GetFloat(key, defaultValue);
        float newFloat = EditorGUILayout.FloatField(label, oldValue);
        if(button.parameterContainer.SetFloat(key, newFloat))
            EditorUtility.SetDirty(button);
    }

    protected void ShowBoolParameter(SLBXButton button, string key, string label, bool defaultValue = false)
    {
        bool oldValue = button.parameterContainer.GetBool(key, defaultValue);
        bool newValue = EditorGUILayout.Toggle(label, oldValue);
        if(button.parameterContainer.SetBool(key, newValue))
            EditorUtility.SetDirty(button);
    }

    #endregion

}
