﻿using UnityEngine;
using System.Collections;
using Chui;

[ExecuteInEditMode]
public class SLBXButtonSignature : MonoBehaviour {
	public TextContent label;
	public Animator animator;

	public event SLBXButton.TouchEventHandler onClicked;

	public virtual void OnClicked(TouchInfo touchInfo) {
		if (onClicked != null) {
			onClicked(touchInfo);
		}
	}
}
