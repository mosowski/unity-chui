﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SLBXButtonSettings : MonoBehaviour {

	[System.Serializable]
	public class StateInfo {
		public string id;
		public GameObject prefab;
	}

	public List<StateInfo> states = new List<StateInfo>();
	public float fontSize = 50;

	public string defaultState = "Default";
	public string disabledState = "Disabled";

	public string pressedTrigger = "Pressed";
	public string releasedTrigger = "Released";
}
