﻿using UnityEngine;
using System.Collections;
using Chui;

[ExecuteInEditMode]
public class SLBXButton : Injector {

	public delegate void TouchEventHandler(TouchInfo ti);

	public SLBXButtonSettings settings;
    public SLBXButtonParameters custom;

	public bool isDisabled = false;
	public bool isTouchEnabled = true;

	public event TouchEventHandler onClicked;
	public event TouchEventHandler onPressed;
	public event TouchEventHandler onReleased;
	public event TouchEventHandler onTouchBegan;
	public event TouchEventHandler onTouchEnded;
	public event TouchEventHandler onTouchMoved;

	public string text = "";

	int lastClickFrame = -1;
	public string enabledState = "Default";
	public Animator animator;

    public StringDictionary parameterContainer;

	public string Text {
		get {
			return text;
		}

		set {
			text = value;
			Refresh();
		}
	}

	public string State {
		get {
			return enabledState;
		}
		set {
			if (enabledState != value) {
				enabledState = value;
				Refresh();
			}
		}
	}

	public bool IsEnabled {
		get {
			return !isDisabled; 
		}
		set {
			if (value != isDisabled) {
				return;
			}
			isDisabled = !value;
			Refresh();
            parameterContainer.GenerateUpdateEvents();
		}
	}

	void Awake() {
		base.Awake();
		Refresh();
		InitHandlers();
		custom = this.Instance.GetComponent<SLBXButtonParameters>();
		parameterContainer.SetListener(OnParameterUpdated, true);
	}

	void InitHandlers() {
		var handlers = Chui.Util.GetEventHandlers(gameObject);

		handlers.onClicked.Add(OnClicked);
		handlers.onPressed.Add(OnPressed);
		handlers.onReleased.Add(OnReleased);
		handlers.onTouchBegan.Add(InternalOnTouchBegan);
		handlers.onTouchBegan.Add(OnTouchBegan);
		handlers.onTouchEnded.Add(OnTouchEnded);
		handlers.onTouchMoved.Add(OnTouchMoved);
	}

	bool InternalOnTouchBegan(TouchInfo ti) {
		if (isTouchEnabled) {
			ti.onUpdate += UpdateTouch;
			PressDown();
			return true;
		}
		return false;
	}

	void Start() {
	}
	
	void PressDown() {
		if (animator != null) {
			animator.SetTrigger(settings.pressedTrigger);
		}
	}
	
	void PressUp() {
		if (animator != null) {
			animator.SetTrigger(settings.releasedTrigger);
		}
	}
	
	void UpdateTouch(TouchInfo ti) {
		if (ti.phase == TouchInfo.Phase.End) {
			PressUp();
			ti.onUpdate -= UpdateTouch;
		}
	}


	void Update() {
		base.Update();

	}

	public void SetClickListener(TouchEventHandler handler) {
		onClicked = handler;
	}

	public void SetPressListener(TouchEventHandler handler) {
		onPressed = handler;
	}

	public void SetReleaseListener(TouchEventHandler handler) {
		onReleased = handler;
	}

	TextContent GetTextContent() {
		if (Instance != null) {
			SLBXButtonSignature bs = Instance.GetComponent<SLBXButtonSignature>();
			if (bs != null && bs.label != null) {
				return bs.label;
			}
		}

		return null;
	}

	Animator GetAnimator() {
		if (Instance != null) {
			SLBXButtonSignature bs = Instance.GetComponent<SLBXButtonSignature>();
			if (bs != null) {
				return bs.animator;
			}
		}
		
		return null;
	}

	void SetState(string id) {
		if (settings != null) {
			foreach (var s in settings.states) {
				if (s.id == id) {
					SetPrefab(s.prefab);
					break;
				}
			}
		}
            
        custom = this.Instance.GetComponent<SLBXButtonParameters>();
        parameterContainer.GenerateUpdateEvents();
	}

	public void Refresh() {
		if (settings == null) {
			return;
		}

		BoxCollider bc = GetComponent<BoxCollider>();
		Widget widget = GetComponent<Widget>();
		if (isDisabled) {
			if (bc != null) {
				bc.enabled = false;
			}
			if (widget != null) {
				widget.isColliderEnabled = false;
			}
			SetState(settings.disabledState);
		} else {
			if (bc != null) {
				bc.enabled = true;
			}
			if (widget != null) {
				widget.isColliderEnabled = true;
			}
			SetState(enabledState);
		}

		if (Instance != null) {
			TextContent tc = GetTextContent();
			if (tc != null) {
				tc.text = text;
				tc.fontSize = settings.fontSize;
			}
		}

		animator = GetAnimator();
	}

	bool OnClicked(TouchInfo ti) {
		if (isTouchEnabled) {
			if (IsEnabled) {
				if (Time.frameCount != lastClickFrame) {
					lastClickFrame = Time.frameCount;
					if (onClicked != null) {
						onClicked.Invoke(ti);
					}
				}
				Signature.OnClicked(ti);
			}
			return true;
		}
		return false;
	}

	SLBXButtonSignature Signature {
		get {
			if (Instance != null) {
				return Instance.GetComponent<SLBXButtonSignature>();
			} else {
				return null;
			}
		}
	}

	bool OnPressed(TouchInfo ti) {
		if (isTouchEnabled) {
			if (IsEnabled && onPressed != null) {
				onPressed.Invoke(ti);
			}
			return true;
		}
		return false;
	}

	bool OnReleased(TouchInfo ti) {
		if (isTouchEnabled) {
			if (IsEnabled && onReleased != null) {
				onReleased.Invoke(ti);
			}
			return true;
		}
		return false;
	}

	bool OnTouchBegan(TouchInfo ti) {
		if (isTouchEnabled) {
			if (IsEnabled && onTouchBegan != null) {
				onTouchBegan.Invoke(ti);
			}
			return true;
		}
		return false;
	}

	bool OnTouchEnded(TouchInfo ti) {
		if (isTouchEnabled) {
			if (IsEnabled && onTouchEnded != null) {
				onTouchEnded.Invoke(ti);
			}
			return true;
		}
		return false;
	}

	bool OnTouchMoved(TouchInfo ti) {
		if (isTouchEnabled) {
			if (IsEnabled && onTouchEnded != null) {
				onTouchMoved.Invoke(ti);
			}
			return true;
		}
		return false;
	}
	
	public void Revalidate() {
		base.Revalidate();
		Refresh();
        custom = this.Instance.GetComponent<SLBXButtonParameters>();
        parameterContainer.SetListener(OnParameterUpdated, true);
	}

    private void OnParameterUpdated(string key, string value)
    {
        if(custom != null)
            custom.OnParameterChanged(this, key, value);
    }

}
