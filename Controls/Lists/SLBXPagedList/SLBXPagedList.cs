﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Chui;

public class SLBXPagedList : MonoBehaviour {
	
	public List<GameObject> itemSlots;

	public GameObject nextButton;
	public GameObject previousButton;
	public bool alwaysShowButtons = true;
	public Dictionary<GameObject, object> userData = new Dictionary<GameObject, object>();

	Action<int, GameObject> itemSetupCallback;
	Func<int> collectionLengthCallback;
	Action<int, int> pageChangeCallback;

	int currentPage;

	public void Init(Action<int, GameObject> itemSetupCallback, Func<int> collectionLengthCallback, Action<int, int> pageChangeCallback = null) {
		this.itemSetupCallback = itemSetupCallback;
		this.collectionLengthCallback = collectionLengthCallback;
		this.pageChangeCallback = pageChangeCallback;

		if (nextButton != null) {
			if (nextButton.GetComponent<SLBXButton>() != null) {
				nextButton.GetComponent<SLBXButton>().SetClickListener(ti => {
					SetNextPage();
				});
			} else {
				Chui.Util.GetEventHandlers(nextButton).onClicked.Set((ti) => {
					SetNextPage();
					return true;
				});
			}
		}

		if (previousButton!= null) {
			if (previousButton.GetComponent<SLBXButton>() != null) {
				previousButton.GetComponent<SLBXButton>().SetClickListener(ti => {
					SetPreviousPage();
				});
			} else {
				Chui.Util.GetEventHandlers(previousButton).onClicked.Set((ti) => {
					SetPreviousPage();
					return true;
				});
			}
		}

		SetPage(0);
	}

	public int NumPages {
		get {
			return Mathf.CeilToInt((float)collectionLengthCallback() / itemSlots.Count);
		}
	}

	public void SetNextPage() {
		if (currentPage + 1 < NumPages) {
			SetPage(currentPage + 1);
		}
	}

	public void SetPreviousPage() {
		if (currentPage > 0) {
			SetPage(currentPage - 1);
		}
	}

	public void SetPage(int page) {
		currentPage = page;
		int offset = page * itemSlots.Count;
		int length = collectionLengthCallback();
		int numPages = NumPages;

		for (int i = offset; i < offset + itemSlots.Count; ++i) {
			if (i < length) {
				itemSlots[i - offset].SetActive(true);
				itemSetupCallback(i, itemSlots[i - offset]);
			} else {
				itemSlots[i - offset].SetActive(false);
			}
		}

		if (pageChangeCallback != null) {
			pageChangeCallback(currentPage, NumPages);
		}

		if (!alwaysShowButtons) {
			if (nextButton != null) {
				nextButton.SetActive(currentPage < numPages - 1);
			}
			if (previousButton != null) {
				previousButton.SetActive(currentPage > 0);
			}
		}
	}
}
