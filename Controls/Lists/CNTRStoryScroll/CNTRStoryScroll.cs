﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Chui;

public class CNTRStoryScroll : MonoBehaviour {

	public class Item {
		public Widget widget;
		public float offset;
		public float size;
	}

	public Widget scrollPanel;
	public Widget scrollContent;
	public float prePadding = 0;
	public float postPadding = 0;
	public bool canShrink = false;

	List<Item> items = new List<Item>();
	bool offsetsChanged = true;
	bool itemsRemoved = false;
	float maxTotalSize = 0;

	void Update () {
		if (offsetsChanged) {
			RevalidateWidgets();
			offsetsChanged = false;
		}
		if (canShrink && itemsRemoved) {
			scrollPanel.GetComponent<Scroll>().FixScrollPositionAfterResize();
			itemsRemoved = false;
		}
	}

	public Item AddItem(Widget itemWidget, float size = 0) {
		itemWidget.pivot = new Vector3(itemWidget.pivot.x, 1, itemWidget.pivot.z);
		itemWidget.Revalidate();

		Item item = new Item();
		item.widget = itemWidget;
		if (!Mathf.Approximately(size, 0)) {
			item.size = size;
		} else {
			item.size = itemWidget.Height;
		}

		item.widget.transform.SetParent(scrollContent.transform, false);

		items.Add(item);
		RevalidateItem(items.Count - 1);
		ExpandScroll();
		return item;
	}

	public void RemoveItem(Item item) {
		int index = items.IndexOf(item);
		RemoveItem(index);
	}

	public void RemoveItem(GameObject item) {
		RemoveItem(item.GetComponent<Widget>());
	}

	public void RemoveItem(int index) {
		if (index != -1) {
			var item = items[index];
			item.widget.gameObject.SetActive(false);
			items.RemoveAt(index);
			for (int i = index; i < items.Count; ++i) {
				RevalidateItem(i);
			}
			itemsRemoved = true;
		}
	}

	public void RemoveItem(Widget itemWidget) {
		int index = -1;
		for (int i = 0; i < items.Count; ++i) {
			if (items[i].widget == itemWidget) {
				RemoveItem(i);
				break;
			}
		}
	}

	public void RevalidateItem(int itemIndex) {
		var item = items[itemIndex];
		if (itemIndex == 0) {
			item.offset = prePadding;
		} else {
			item.offset = items[itemIndex - 1].offset + items[itemIndex - 1].size;
		}
		offsetsChanged = true;
	}

	public void RevalidateWidgets() {
		for (int i = 0; i < items.Count; ++i) {
			items[i].widget.SetLocalPosition(new Vector3(0, -items[i].offset, 0));
		}
	}

	public void ExpandScroll() {
		float totalSize = prePadding + postPadding;
		if (items.Count > 0) {
			totalSize += items[items.Count - 1].offset + items[items.Count - 1].size;
		}
		if (canShrink || maxTotalSize < totalSize) {
			maxTotalSize = totalSize;
		}
		scrollContent.Height = Mathf.Max(scrollPanel.Height, maxTotalSize);
	}
}
