﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum SLBXContextMenuOrientation {
	Up,
	Down
}

public class SLBXContextMenu : MonoBehaviour {

	public class Item {
		public GameObject content;
		public GameObject separator;
	}

	public GameObject pivotWidget;
	public GameObject downSection;
	public GameObject upSection;
	public GameObject downPackLayout;
	public GameObject upPackLayout;

	public GameObject itemContentPrefab;
	public GameObject itemSeparatorPrefab;

	public float frameBorderWidth;
	public float pointerOffset;

	GameObject currentPackLayout;

	SLBXContextMenuOrientation orientation = SLBXContextMenuOrientation.Down;
	List<Item> items = new List<Item>();

	public SLBXContextMenuOrientation Orientation {
		get {
			return orientation;
		}
		set {
			orientation = value;
			UpdateSize();
		}
	}

	public Item AddItem(string itemText, Action onClicked) {
		if (items.Count > 0) {
			if (itemSeparatorPrefab != null) {
				var separator = items[items.Count - 1].separator = GameObject.Instantiate(itemSeparatorPrefab);
				separator.transform.SetParent(currentPackLayout.transform, false);
			}
		}

		var item = new Item();
		item.content = GameObject.Instantiate(itemContentPrefab);
		item.content.transform.SetParent(currentPackLayout.transform, false);
		item.content.GetComponent<SLBXContextMenuItem>().SetLabelText(itemText);
		Chui.Util.GetEventHandlers(item.content).onClicked.Set((ti) => {
			onClicked();
			return true;
		});
		items.Add(item);

		UpdateSize();

		return item;
	}

	public void RemoveItem(Item item) {
		GameObject.Destroy(item.content);
		if (item.separator != null) {
			GameObject.Destroy(item.separator);
		}
		items.Remove(item);

		UpdateSize();
	}

	public void SetPosition(Vector3 worldPosition) {
		pivotWidget.GetComponent<Chui.Widget>().SetWorldPosition(worldPosition);
		UpdateSize();
	}

	void Update() {
	}

	void UpdateSize() {
		var oldPackLayout = currentPackLayout;

		if (orientation == SLBXContextMenuOrientation.Down) {
			currentPackLayout = downPackLayout;
		} else if (orientation == SLBXContextMenuOrientation.Up) {
			currentPackLayout = upPackLayout;
		}

		if (currentPackLayout != oldPackLayout) {
			foreach (var item in items) {
				item.content.transform.SetParent(currentPackLayout.transform, false);
				if (item.separator != null) {
					item.separator.transform.SetParent(currentPackLayout.transform, false);
				}
			}
		}

		downSection.SetActive(orientation == SLBXContextMenuOrientation.Down);
		upSection.SetActive(orientation == SLBXContextMenuOrientation.Up);
		
		float height = 0;
		foreach (var item in items) {
			height += item.content.GetComponent<Chui.Widget>().Height;
			if (item.separator != null) {
				height += item.separator.GetComponent<Chui.Widget>().Height;
			}
		}
		currentPackLayout.GetComponent<Chui.Widget>().Height = height;
		currentPackLayout.GetComponent<Chui.PackLayout>().Revalidate();
		gameObject.GetComponent<Chui.Widget>().RevalidateHierarchy();
	}
}
