﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Chui;

public class CNTRRoyaleMenu : Scroll {
	public List<GameObject> pages = new List<GameObject>();
	public float scrollVelocityPower = 0.75f;
	public float scrollVelocityMultiplier = 1.2f;
	public float snappingRatio = 0.1f;
	public int autoHidePageDistance = 1;	
	Vector3 snapPosition;
	int snapPage = -1;

	override protected bool OnDragEnded(DragListener dl) {
		base.OnDragEnded(dl);
		Vector3 rootedVelocity = scrollVelocity.normalized * Mathf.Pow(scrollVelocity.magnitude, scrollVelocityPower) * scrollVelocityMultiplier;
		Vector3 origin = transform.position - transform.TransformVector(rootedVelocity);
		float bestDist = float.PositiveInfinity;
		Vector3 bestPagePos = Vector3.zero;
		for (int i = 0; i < pages.Count; ++i) {
			Vector3 pagePos = pages[i].transform.position;
			float pageDist = Vector3.Distance(pagePos, origin);
			if (pageDist < bestDist) {
				bestDist = pageDist;
				bestPagePos = pagePos;
				snapPage = i;
			}
		}
		snapPosition = -content.transform.InverseTransformPoint(bestPagePos);
		return true;
	}

	override public void DeterministicStep(Widget widget) {		
		if (!isDragging) {
			Vector3 contentPos = content.GetLocalPosition();
			Vector3 newContentPos;
			if (Vector3.Distance(contentPos, snapPosition) < 1) {
				newContentPos = snapPosition;
			} else {
				newContentPos = contentPos + (snapPosition - contentPos) * snappingRatio;
			}
			content.SetLocalPosition(newContentPos);			
		}		

		if (snapPage != -1) {
			AutoHidePages();	
		}
	}

	void AutoHidePages() {
		if (autoHidePageDistance >= 0) {
			for (int i = 0; i < pages.Count; ++i) {
				if (System.Math.Abs(i - snapPage) > autoHidePageDistance) {
					pages[i].SetActive(false);
				} else {
					pages[i].SetActive(true);
				}
			}
		}
	}
}
