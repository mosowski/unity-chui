﻿using UnityEngine;
using System.Collections;
using Chui;

public class CNTRCheckbox : MonoBehaviour {

	public GameObject collider;
	public GameObject onView;
	public GameObject offView;

	public bool state;
	public float clickThreshold = 0.2f;

	public delegate void OnStateChanged(bool c);
	public event OnStateChanged onStateChanged;

	public bool State {
		get {
			return state;
		}
		set {
			if (state != value) {
				state = value;
				if (onStateChanged != null) {
					onStateChanged(state);
				}
			} else {
				state = value;
			}
			Revalidate();
		}
	}

	void Awake() {
		Util.GetEventHandlers(collider).onClicked.Set(ti => {
			if (ti.MoveDistance < clickThreshold) {
				State = !state;
			}
			return true;
		});
	}

	public void Revalidate() {
		onView.SetActive(state);
		offView.SetActive(!state);
	}
}
