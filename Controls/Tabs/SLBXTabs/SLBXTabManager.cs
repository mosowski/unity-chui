﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Chui;

public class SLBXTabManager : MonoBehaviour {
    public List<SLBXTab> tabs;

    public SLBXTab selectedTab;
    public System.Action<SLBXTabManager, SLBXTab> onSelect;

    public void Awake() {

        foreach (var t in tabs) {
            SLBXTab tab = t;
            Chui.Util.GetEventHandlers(tab.gameObject).onClicked.Add((ti) => {
                Select(tab);
                return true;
            });
        }

        Select(selectedTab);
    }

    public SLBXTab GetTabByName(string name) {
        foreach (var t in tabs) {
            if (t.gameObject.name == name) {
                return t;
            }
        }
        return null;
    }

    public void Select(SLBXTab tab) {
        foreach (var t in tabs) {
            t.IsSelected = t == tab;
        }
        if (selectedTab != tab) {
            selectedTab = tab;
            if(onSelect != null)
                onSelect(this, tab);
        }
    }



}
