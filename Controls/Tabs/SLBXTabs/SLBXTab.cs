﻿using UnityEngine;
using System.Collections;
using Chui;

public class SLBXTab : MonoBehaviour {

	public GameObject selectedView;
	public GameObject notSelectedView;
	public GameObject label;

	public GameObject content;

	public event System.Action<bool> onSelected;

	bool isSelected;

	void Awake() {
		Revalidate();
	}

	void Revalidate() {
		if (selectedView != null) {
			selectedView.SetActive(isSelected);
		}
		if (notSelectedView != null) {
			notSelectedView.SetActive(notSelectedView);
		}
		if (content != null) {
			content.SetActive(isSelected);
		}
		if (onSelected != null) {
			onSelected(isSelected);
		}
	}

	public bool IsSelected {
		get {
			return isSelected;
		}
		set {
			if (value != isSelected) {
				isSelected = value;
				Revalidate();
			}
		}
	}
}
