﻿using UnityEngine;
using System.Collections;

namespace Chui {

	[AddComponentMenu("Chui/Window")]
	public class Window : MonoBehaviour {
		public bool isFullscreen;
		public float depthVolume;

		public delegate void CloseHandler();
		public event CloseHandler onClosed;
		
		public delegate void ShowHandler();
		public event ShowHandler onShow;

		public delegate void HideHandler();
		public event HideHandler onHide;

		bool afterDisable = false;

		void Start() {
			Show();
		}

		public void Close() {
			if (onClosed != null) {
				onClosed();
			}
		}
		
		public void Show() {
			if (onShow != null) {
				onShow();
			}
		}

		public void Hide() {
			if (onHide != null) {
				onHide();
			}
		}

		void OnEnable() {
			if (afterDisable) {
				Show();
			}
		}

		void OnDisable() {
			Hide();
			afterDisable = true;
		}

	}
}
