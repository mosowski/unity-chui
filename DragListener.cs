﻿using UnityEngine;
using System.Collections;

namespace Chui {
	public class DragListener : MonoBehaviour {
		public EventHandler<DragListener> onDragStarted = new EventHandler<DragListener>();
		public EventHandler<DragListener> onDragMoved = new EventHandler<DragListener>();
		public EventHandler<DragListener> onDragEnded = new EventHandler<DragListener>();

		public float dragThreshold = 0.1f;
		public float swipeDotThreshold = 0.1f;
		public float swipeAccelerationThreshold = 0.3f;
		public TouchInfo touchInfo;

		bool isDragStarted;
		bool isDragDiscarded;

		Vector3 lastPosition;
		Vector3 lastVelocity;
		Vector3 swipe;
		Vector3 swipeOrigin;
		int swipeTicks;
		
		public bool IsDragStarted {
			get {
				return isDragStarted;
			}
		}

		public int SwipeTicks {
			get {
				return swipeTicks;
			}
		}

		public Vector3 Swipe {
			get {
				return swipe;
			}
		}

		void Awake() {
			Util.GetBubblingHandlers(gameObject).onTouchBegan.Add(OnTouchBegan);
			Util.GetBubblingHandlers(gameObject).onClicked.Add(OnClicked);
		}

		public void DiscardDrag() {
			if (touchInfo != null) {
				touchInfo.onUpdate -= OnUpdate;
			}
			isDragStarted = false;
			isDragDiscarded = true;
			touchInfo = null;
		}

		bool OnTouchBegan(TouchInfo ti) {
			if (touchInfo == null) {
				isDragDiscarded = false;
				lastPosition = ti.rawPosition;
				touchInfo = ti;
				touchInfo.onUpdate += OnUpdate;
				swipeOrigin = ti.startPosition;
				swipeTicks = 1;
			}
			return false;
		}

		float PhysicalDragThreshold {
			get {
				return dragThreshold * Root.GetInstance().physicalWidth;
			}
		}

		// to disable clicks while ending drags
		bool OnClicked(TouchInfo ti) {
			return touchInfo != null && Vector3.Distance(touchInfo.startPosition, touchInfo.position) > PhysicalDragThreshold;
		}

		void OnUpdate(TouchInfo ti) {
			if (touchInfo.phase == TouchInfo.Phase.End) {
				swipe = touchInfo.position - swipeOrigin;
				if (isDragStarted) {
					onDragEnded.Invoke(this);
				}
				DiscardDrag();
			} else {
				if (!isDragStarted) {
					if (Vector3.Distance(touchInfo.startPosition, touchInfo.position) > PhysicalDragThreshold) {
						if (onDragStarted.Invoke(this)) {
							isDragStarted = true;
						} else {
							DiscardDrag();
						}
					}
				} else {
					if (lastPosition != ti.rawPosition) {
						swipeTicks++;
						if (Vector3.Dot(touchInfo.velocity.normalized, lastVelocity.normalized) < swipeDotThreshold
							|| (lastVelocity.magnitude > 0 && touchInfo.velocity.magnitude / lastVelocity.magnitude < swipeAccelerationThreshold)) {
							swipeTicks = 1;
							swipeOrigin = touchInfo.position;
						}
						lastVelocity = touchInfo.velocity;

						if (!onDragMoved.Invoke(this)) {
							DiscardDrag();
						}
						lastPosition = ti.rawPosition;
					}
				}
			}
		}
	}
}
