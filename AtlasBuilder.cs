﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	public class AtlasBuilder {

		public class Source {
			public object owner;
			public int width;
			public int height;
			public int padding;

			public Node node;
			public int page;
		}

		public class Node {
			public Node left;
			public Node right;
			public bool isOccupied;
			public int x;
			public int y;
			public int width;
			public int height;
			public int padding;

			public Node(int x, int y, int width, int height, int padding) {
				this.x = x;
				this.y = y;
				this.width = width;
				this.height = height;
				this.padding = padding;
			}

			public void Clear() {
				left = null;
				right = null;
				isOccupied = false;
			}

			public int XPadded {
				get {
					return x + padding;
				}
			}

			public int YPadded {
				get {
					return y + padding;
				}
			}

			public Node Insert(int nodeWidth, int nodeHeight, int padding) {
				if (isOccupied) {
					Node node = left.Insert(nodeWidth, nodeHeight, padding);
					if (node == null) {
						node = right.Insert(nodeWidth, nodeHeight, padding);
					}
					return node;
				} else {
					if (nodeWidth + padding * 2 <= width && nodeHeight + padding * 2 <= height) {
						isOccupied = true;
						left = new Node(x, y + (nodeHeight + padding * 2), (nodeWidth + padding * 2), height - (nodeHeight + padding * 2), padding);
						right = new Node(x + (nodeWidth + padding * 2), y, width - (nodeWidth + padding * 2), height, padding);
						return this;
					}
					return null;
				}
			}
		}

		public int minSize = 256;
		public int maxSize = 2048;
		public List<Node> pages = new List<Node>();
		public List<Source> sources = new List<Source>();
		private bool useSquareAtlas = true;

		void ClearPages() {
			pages.ForEach(p => p.Clear());
		}

		void AddNewPage() {
			pages.Add(new Node(0, 0, minSize, minSize, 0));
		}

		bool TryInsertNode(Source src) {
			for (int i = 0; i < pages.Count; ++i) {
				src.node = pages[i].Insert(src.width, src.height, src.padding);
				if (src.node != null) {
					src.page = i;
					return true;
				}
			}
			return false;
		}

		bool TryEnlargePage() {
			for (int i = 0; i < pages.Count; ++i) {
				if (pages[i].width < maxSize && pages[i].height < maxSize) {
					if (useSquareAtlas) {
						pages[i].width *= 2;
						pages[i].height *= 2;
						return true;
					}
					else {
						if (pages[i].width < pages[i].height) {
							pages[i].width *= 2;
							return true;
						}
						else {
							pages[i].height *= 2;
							return true;
						}
					}
				}
			}
			return false;
		}


		public void Init(bool useSquareAtlas) {
			this.useSquareAtlas = useSquareAtlas;
			sources.Clear();
		}

		public void AddSource(object owner, int width, int height, int padding) {
			sources.Add(new Source() { owner = owner, width = width, height = height, padding = padding });
		}

		public Source GetSource(object owner) {
			return sources.Find(src => src.owner == owner);
		}

		public bool Build() {
			pages.Clear();

			sources.Sort((s1, s2) => s2.width - s1.width);

			int numInsertedSources = 0;
			while (numInsertedSources != sources.Count) {
				foreach (Source src in sources) {
					if (src.width > maxSize || src.height > maxSize) {
						return false;
					}
					if (TryInsertNode(src)) {
						++numInsertedSources;
					} else {
						if (!TryEnlargePage()) {
							AddNewPage();
						}
						ClearPages();
						numInsertedSources = 0;
						break;
					}
				}
			}
			return true;
		}
	}
}
