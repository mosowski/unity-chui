using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	[System.Serializable]
	public struct Anchor {
		public Widget target;
		public float relative;
		public float offset;

		public bool IsEqual(ref Anchor a) {
			return target == a.target
				&& relative == a.relative
				&& offset == a.offset;
		}
	}


	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Anchored Widget")]
	public class AnchoredWidget : Widget {

		public enum AnchorConstraint {
			None,
			AdjustHeight,
			AdjustWidth,
			Fill,
			Fit
		}

		public AnchorConstraint anchorConstraint = AnchorConstraint.None;
		public float fixedAspect = 1;
		public bool useCustomAnchorTargets = false;

		public Anchor leftAnchor = new Anchor();
		public Anchor rightAnchor = new Anchor() { offset = 100 };
		public Anchor topAnchor = new Anchor() { offset = 100 };
		public Anchor bottomAnchor = new Anchor();
		public Anchor backAnchor = new Anchor() { offset = 0, relative = 1 };
		public Anchor frontAnchor = new Anchor() { offset = -20, relative = 1 };

		Vector3 localPosition;

		Color last_color = Color.white;
		Anchor last_leftAnchor = new Anchor() { offset = -100000 };
		Anchor last_rightAnchor = new Anchor() { offset = -100000 };
		Anchor last_bottomAnchor = new Anchor() { offset = -100000 };
		Anchor last_topAnchor = new Anchor() { offset = -100000 };
		Anchor last_backAnchor = new Anchor() { offset = -100000 };
		Anchor last_frontAnchor = new Anchor() { offset = -100000 };

		[System.NonSerialized]
		Volume parentVolume;
		Volume newVolume = new Volume();

		public override float Width {
			get {
				return volume.Width;
			}
			set {
				if (value != volume.Width) {
					float d = value - volume.Width;
					leftAnchor.offset -= pivot.x * d;
					rightAnchor.offset += (1 - pivot.x) * d;
				}
			}
		}

		public override float Height {
			get {
				return volume.Height;
			}
			set {
				if (value != volume.Height) {
					float d = value - volume.Height;
					bottomAnchor.offset -= pivot.y * d;
					topAnchor.offset += (1 - pivot.y) * d;
				}
			}
		}

		public override float Depth {
			get {
				return volume.Depth;
			}
			set {
				if (value != volume.Depth) {
					float d = value - volume.Depth;
					backAnchor.offset -= pivot.z * d;
					frontAnchor.offset += (1 - pivot.z) * d;
				}
			}
		}

		public Volume ParentVolume {
			get {
				return parentVolume;
			}
		}

		void Awake() {
#if UNITY_EDITOR
			if (!Application.isPlaying) {
				if (transform.root.GetComponent<Root>() == null) {
					Root r = GameObject.FindObjectOfType<Root>();
					if (r != null) {
						transform.root.SetParent(r.transform, false);
					}
					r.RevalidateEverything();
				}
			}
#endif

			content = GetComponent<Content>();
			OnUpdate();
		}

		void ApplyHeightConstraint() {
			newVolume.yMin = newVolume.yMin + newVolume.Height * pivot.y - newVolume.Width / fixedAspect * pivot.y;
			newVolume.Height = newVolume.Width / fixedAspect;

			if (bottomAnchor.target == null) {
				bottomAnchor.offset = newVolume.yMin - (parentVolume.yMin + bottomAnchor.relative * parentVolume.Height);
			} else {
				Widget target = bottomAnchor.target;
				target.OnUpdate();
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(0, target.volume.yMin + target.volume.Height * bottomAnchor.relative, 0));
				bottomAnchor.offset = newVolume.yMin - edge.y;
			}
			if (topAnchor.target == null) {
				topAnchor.offset = newVolume.yMax - (parentVolume.yMin + topAnchor.relative * parentVolume.Height);
			} else {
				Widget target = topAnchor.target;
				target.OnUpdate();
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(0, target.volume.yMin + target.volume.Height * topAnchor.relative, 0));
				topAnchor.offset = newVolume.yMax - edge.y;
			}
		}

		void ApplyWidthConstraint() {
			newVolume.xMin = newVolume.xMin + newVolume.Width * pivot.x - newVolume.Height * fixedAspect * pivot.x;
			newVolume.Width = newVolume.Height * fixedAspect;

			if (leftAnchor.target == null) {
				leftAnchor.offset = newVolume.xMin - (parentVolume.xMin + leftAnchor.relative * parentVolume.Width);
			} else {
				Widget target = leftAnchor.target;
				target.OnUpdate();
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.xMin + target.volume.Width * leftAnchor.relative, 0, 0));
				leftAnchor.offset = newVolume.xMin - edge.x;
			}
			if (rightAnchor.target == null) {
				rightAnchor.offset = newVolume.xMax - (parentVolume.xMin + rightAnchor.relative * parentVolume.Width);
			} else {
				Widget target = rightAnchor.target;
				target.OnUpdate();
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.xMin + target.volume.Width * rightAnchor.relative, 0, 0));
				rightAnchor.offset = newVolume.xMax - edge.x;
			}
		}

		public void ApplyConstraint() {
			if (anchorConstraint == AnchorConstraint.AdjustHeight) {
				RecalculateLeftAnchor();
				RecalculateRightAnchor();
				RecalculateBottomAnchor();
				RecalculateTopAnchor();

				ApplyHeightConstraint();
			} else if (anchorConstraint == AnchorConstraint.AdjustWidth) {
				RecalculateLeftAnchor();
				RecalculateRightAnchor();
				RecalculateBottomAnchor();
				RecalculateTopAnchor();

				ApplyWidthConstraint();
			} else if (anchorConstraint == AnchorConstraint.Fill) {
				leftAnchor.offset = 0;
				rightAnchor.offset = 0;
				bottomAnchor.offset = 0;
				topAnchor.offset = 0;
				RecalculateLeftAnchor();
				RecalculateRightAnchor();
				RecalculateBottomAnchor();
				RecalculateTopAnchor();

				if (newVolume.Width / newVolume.Height >= fixedAspect) {
					ApplyHeightConstraint();
				} else {
					ApplyWidthConstraint();
				}
			} else if (anchorConstraint == AnchorConstraint.Fit) {
				leftAnchor.offset = 0;
				rightAnchor.offset = 0;
				bottomAnchor.offset = 0;
				topAnchor.offset = 0;
				RecalculateLeftAnchor();
				RecalculateRightAnchor();
				RecalculateBottomAnchor();
				RecalculateTopAnchor();

				if (newVolume.Width / newVolume.Height >= fixedAspect) {
					ApplyWidthConstraint();
				} else {
					ApplyHeightConstraint();
				}
			}
		}

		void RecalculateLeftAnchor() {
			if (leftAnchor.target == null) {
				newVolume.xMin = parentVolume.xMin + leftAnchor.relative * parentVolume.Width + leftAnchor.offset;
			} else {
				Widget target = leftAnchor.target;
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.xMin + target.volume.Width * leftAnchor.relative, 0, 0));
				newVolume.xMin = edge.x + leftAnchor.offset;
			}
		}

		void RecalculateRightAnchor() {
			if (rightAnchor.target == null) {
				newVolume.xMax = parentVolume.xMin + rightAnchor.relative * parentVolume.Width + rightAnchor.offset;
			} else {
				Widget target = rightAnchor.target;
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.xMin + target.volume.Width * rightAnchor.relative, 0, 0));
				newVolume.xMax = edge.x + rightAnchor.offset;
			}
		}

		void RecalculateBottomAnchor() {
			if (bottomAnchor.target == null) {
				newVolume.yMin = parentVolume.yMin + bottomAnchor.relative * parentVolume.Height + bottomAnchor.offset;
			} else {
				Widget target = bottomAnchor.target;
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(0, target.volume.yMin + target.volume.Height * bottomAnchor.relative, 0));
				newVolume.yMin = edge.y + bottomAnchor.offset;
			}
		}

		void RecalculateTopAnchor() {
			if (topAnchor.target == null) {
				newVolume.yMax = parentVolume.yMin + topAnchor.relative * parentVolume.Height + topAnchor.offset;
			} else {
				Widget target = topAnchor.target;
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(0, target.volume.yMin + target.volume.Height * topAnchor.relative, 0));
				newVolume.yMax = edge.y + topAnchor.offset;
			}
		}

		public void RecalculateBackAnchor() {
			if (backAnchor.target == null) {
				newVolume.zMin = parentVolume.zMin + backAnchor.relative * parentVolume.Depth + backAnchor.offset;
			} else {
				Widget target = backAnchor.target;
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(0, 0, target.volume.zMin + target.volume.Depth * backAnchor.relative));
				newVolume.zMin = edge.z + backAnchor.offset;
			}
		}

		public void RecalculateFrontAnchor() {
			if (frontAnchor.target == null) {
				newVolume.zMax = parentVolume.zMin + frontAnchor.relative * parentVolume.Depth + frontAnchor.offset;
			} else {
				Widget target = frontAnchor.target;
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(0, 0, target.volume.zMin + target.volume.Depth * frontAnchor.relative));
				newVolume.zMax = edge.z + frontAnchor.offset;
			}
		}

		protected override void UpdateVolume() {
			if ((changes & ChangeBits.Transform) == 0) {
				if (!(leftAnchor.IsEqual(ref last_leftAnchor)
				&& rightAnchor.IsEqual(ref last_rightAnchor)
				&& bottomAnchor.IsEqual(ref last_bottomAnchor)
				&& topAnchor.IsEqual(ref last_topAnchor)
				&& backAnchor.IsEqual(ref last_backAnchor)
				&& frontAnchor.IsEqual(ref last_frontAnchor))) {
					changes |= ChangeBits.Transform;
				}
			}

			if (color != last_color) {
				changes |= ChangeBits.NonTransform;
				last_color = color;
			}

			if ((changes & ChangeBits.Transform) == 0) {
				if ((leftAnchor.target != null && (leftAnchor.target.changes & ChangeBits.Transform) != 0)
					|| (rightAnchor.target != null && (rightAnchor.target.changes & ChangeBits.Transform) != 0)
					|| (bottomAnchor.target != null && (bottomAnchor.target.changes & ChangeBits.Transform) != 0)
					|| (topAnchor.target != null && (topAnchor.target.changes & ChangeBits.Transform) != 0)
					|| (backAnchor.target != null && (backAnchor.target.changes & ChangeBits.Transform) != 0)
					|| (frontAnchor.target != null && (frontAnchor.target.changes & ChangeBits.Transform) != 0)) {
					changes |= ChangeBits.Transform;
				}
			}

			if ((changes & ChangeBits.Transform) != 0) {
				if (parentWidget != null) {
					parentVolume = parentWidget.volume;
				} else {
					parentVolume = volume;
				}

				last_leftAnchor = leftAnchor;
				last_rightAnchor = rightAnchor;
				last_bottomAnchor = bottomAnchor;
				last_topAnchor = topAnchor;
				last_backAnchor = backAnchor;
				last_frontAnchor = frontAnchor;

				if (anchorConstraint != AnchorConstraint.None) {
					ApplyConstraint();
					RecalculateBackAnchor();
					RecalculateFrontAnchor();
				} else {
					RecalculateLeftAnchor();
					RecalculateRightAnchor();
					RecalculateBottomAnchor();
					RecalculateTopAnchor();
					RecalculateBackAnchor();
					RecalculateFrontAnchor();
				}

				Volume tmpVolume = volume;
				volume = newVolume;
				newVolume = tmpVolume;

				Vector3 localPivot = new Vector3(volume.xMin + volume.Width * pivot.x, volume.yMin + volume.Height * pivot.y, volume.zMin + volume.Depth * pivot.z);

				volume.X -= localPivot.x;
				volume.Y -= localPivot.y;
				volume.Z -= localPivot.z;

				transform.localPosition = localPivot;
				localPosition = localPivot;
			}
		}

		public override void AddTopology(List<Widget> order, int tag) {
			if (topologyUpdateTag != tag) {
				topologyUpdateTag = tag;

				if (leftAnchor.target != null) {
					leftAnchor.target.AddTopology(order, tag);
				}
				if (rightAnchor.target != null) {
					rightAnchor.target.AddTopology(order, tag);
				}
				if (bottomAnchor.target != null) {
					bottomAnchor.target.AddTopology(order, tag);
				}
				if (topAnchor.target != null) {
					topAnchor.target.AddTopology(order, tag);
				}
				if (backAnchor.target != null) {
					backAnchor.target.AddTopology(order, tag);
				}
				if (frontAnchor.target != null) {
					frontAnchor.target.AddTopology(order, tag);
				}

				order.Add(this);				
			}
		}

#if UNITY_EDITOR
		Root cachedRoot;

		void Update() {
			//NOTE: 
			// in edit-mode, children Update() are not going to be called when parent gets dirty.
			if (!Application.isPlaying) {
				if (cachedRoot == null) {
					cachedRoot = gameObject.GetComponentInParent<Root>();
				}
				if (cachedRoot != null) {
					cachedRoot.UpdateHierarchy();
				}
			}
		}
#else
#endif

		public override void Translate(float x, float y, float z = 0) {
			leftAnchor.offset += x;
			rightAnchor.offset += x;
			topAnchor.offset += y;
			bottomAnchor.offset += y;
			backAnchor.offset += z;
			frontAnchor.offset += z;
		}

		void SetNearestRelativeAnchor(ref Anchor anchor, float parentSize) {
			SetNearestRelativeAnchor(ref anchor, parentSize, anchor.relative * parentSize + anchor.offset);
		}

		void SetNearestRelativeAnchor(ref Anchor anchor, float parentSize, float edge) {
			if (edge > parentSize * 0.75f) {
				anchor.relative = 1.0f;
				anchor.offset = edge - parentSize;
			} else if (edge > parentSize * 0.25f) {
				anchor.relative = 0.5f;
				anchor.offset = edge - parentSize * 0.5f;
			} else {
				anchor.relative = 0;
				anchor.offset = edge;
			}
		}

		void SetRelativeAnchorHelper(ref Anchor anchor, Vector2 axisMask, Vector2 maxMask, float relative) {
			Transform targetTransform = anchor.target != null ? anchor.target.transform : transform.parent;
			Volume targetVolume = anchor.target != null ? anchor.target.volume : parentVolume;

			Matrix4x4 mtx = targetTransform.transform.worldToLocalMatrix * transform.localToWorldMatrix;
			Vector3 minEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMin, volume.yMin, volume.zMin));
			Vector3 maxEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMax, volume.yMax, volume.zMax));

			anchor.relative = relative;
			anchor.offset =  axisMask.x * (maxMask.x * minEdge.x + maxMask.y * maxEdge.x - targetVolume.xMin - targetVolume.Width * relative)
				+ axisMask.y * (maxMask.x * minEdge.y + maxMask.y * maxEdge.y - targetVolume.yMin - targetVolume.Height * relative);
		}

		public void SetLeftAnchor(float relative, float offset, Widget target = null) {
			leftAnchor.relative = relative;
			leftAnchor.offset = offset;
			leftAnchor.target = target;
		}

		public void SetRightAnchor(float relative, float offset, Widget target = null) {
			rightAnchor.relative = relative;
			rightAnchor.offset = offset;
			rightAnchor.target = target;
		}

		public void SetBottomAnchor(float relative, float offset, Widget target = null) {
			bottomAnchor.relative = relative;
			bottomAnchor.offset = offset;
			bottomAnchor.target = target;
		}

		public void SetTopAnchor(float relative, float offset, Widget target = null) {
			topAnchor.relative = relative;
			topAnchor.offset = offset;
			topAnchor.target = target;
		}

		public void SetLeftRelative(float relative) {
			SetRelativeAnchorHelper(ref leftAnchor, Vector2.right, Vector2.right, relative);
		}

		public void SetRightRelative(float relative) {
			SetRelativeAnchorHelper(ref rightAnchor, Vector2.right, Vector2.up, relative);
		}

		public void SetBottomRelative(float relative) {
			SetRelativeAnchorHelper(ref bottomAnchor, Vector2.up, Vector2.right, relative);
		}

		public void SetTopRelative(float relative) {
			SetRelativeAnchorHelper(ref topAnchor, Vector2.up, Vector2.up, relative);
		}


		public void SetNearestRelativeAnchors() {
			SetNearestRelativeAnchor(ref leftAnchor, parentVolume.Width);
			SetNearestRelativeAnchor(ref rightAnchor, parentVolume.Width);
			SetNearestRelativeAnchor(ref bottomAnchor, parentVolume.Height);
			SetNearestRelativeAnchor(ref topAnchor, parentVolume.Height);
			SetNearestRelativeAnchor(ref backAnchor, parentVolume.Depth);
			SetNearestRelativeAnchor(ref frontAnchor, parentVolume.Depth);
		}

		public override void SetParent(Widget newParent) {
			Matrix4x4 mtx = newParent.transform.worldToLocalMatrix * transform.localToWorldMatrix;
			Vector3 minEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMin, volume.yMin, volume.zMin));
			Vector3 maxEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMax, volume.yMax, volume.zMax));

			SetNearestRelativeAnchor(ref leftAnchor, newParent.volume.Width, minEdge.x - newParent.volume.xMin);
			SetNearestRelativeAnchor(ref rightAnchor, newParent.volume.Width, maxEdge.x - newParent.volume.xMin);
			SetNearestRelativeAnchor(ref bottomAnchor, newParent.volume.Height, minEdge.y - newParent.volume.yMin);
			SetNearestRelativeAnchor(ref topAnchor, newParent.volume.Height, maxEdge.y - newParent.volume.yMin);
			SetNearestRelativeAnchor(ref backAnchor, newParent.volume.Depth, minEdge.z - newParent.volume.zMin);
			SetNearestRelativeAnchor(ref frontAnchor, newParent.volume.Depth, maxEdge.z - newParent.volume.zMin);

			leftAnchor.target = newParent;
			rightAnchor.target = newParent;
			bottomAnchor.target = newParent;
			topAnchor.target = newParent;
			backAnchor.target = newParent;
			frontAnchor.target = newParent;

			transform.SetParent(newParent.transform, false);
		}

		public void SetLeftTarget(Widget target) {
			Matrix4x4 mtx = target.transform.worldToLocalMatrix * transform.localToWorldMatrix;
			Vector3 minEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMin, volume.yMin, volume.zMin));
			SetNearestRelativeAnchor(ref leftAnchor, target.volume.Width, minEdge.x - target.volume.xMin);
			leftAnchor.target = target;
		}

		public void SetRightTarget(Widget target) {
			Matrix4x4 mtx = target.transform.worldToLocalMatrix * transform.localToWorldMatrix;
			Vector3 maxEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMax, volume.yMax, volume.zMax));
			SetNearestRelativeAnchor(ref rightAnchor, target.volume.Width, maxEdge.x - target.volume.xMin);
			rightAnchor.target = target;
		}

		public void SetBottomTarget(Widget target) {
			Matrix4x4 mtx = target.transform.worldToLocalMatrix * transform.localToWorldMatrix;
			Vector3 minEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMin, volume.yMin, volume.zMin));
			SetNearestRelativeAnchor(ref bottomAnchor, target.volume.Height, minEdge.y - target.volume.yMin);
			bottomAnchor.target = target;
		}

		public void SetTopTarget(Widget target) {
			Matrix4x4 mtx = target.transform.worldToLocalMatrix * transform.localToWorldMatrix;
			Vector3 maxEdge = mtx.MultiplyPoint3x4(new Vector3(volume.xMax, volume.yMax, volume.zMax));
			SetNearestRelativeAnchor(ref topAnchor, target.volume.Height, maxEdge.y - target.volume.yMin);
			topAnchor.target = target;
		}

		public void SetRelativeAnchors(float left, float bottom, float right, float top) {
			leftAnchor.relative = left;
			bottomAnchor.relative = bottom;
			rightAnchor.relative = right;
			topAnchor.relative = top;
		}

		public void SetOffsetAnchors(float left, float bottom, float right, float top) {
			leftAnchor.offset = left;
			bottomAnchor.offset = bottom;
			rightAnchor.offset = right;
			topAnchor.offset = top;
		}

		public void ConvertLeftOffsetToRelative() {
			Widget target = leftAnchor.target;
			if (target == null) {
				leftAnchor.relative = (leftAnchor.relative * parentVolume.Width + leftAnchor.offset) / parentVolume.Width;
			} else {
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.xMin + target.volume.Width * leftAnchor.relative, 0, 0));
				leftAnchor.relative = (edge.x + leftAnchor.offset - target.volume.xMin) / target.volume.Width;
			}
			leftAnchor.offset = 0;
		}

		public void ConvertRightOffsetToRelative() {
			Widget target = rightAnchor.target;
			if (target == null) {
				rightAnchor.relative = (rightAnchor.relative * parentVolume.Width + rightAnchor.offset) / parentVolume.Width;
			} else {
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.xMin + target.volume.Width * rightAnchor.relative, 0, 0));
				rightAnchor.relative = (edge.x + rightAnchor.offset - target.volume.xMin) / target.volume.Width;
			}
			rightAnchor.offset = 0;
		}

		public void ConvertBottomOffsetToRelative() {
			Widget target = bottomAnchor.target;
			if (target == null) {
				bottomAnchor.relative = (bottomAnchor.relative * parentVolume.Height + bottomAnchor.offset) / parentVolume.Height;
			} else {
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.yMin + target.volume.Height * bottomAnchor.relative, 0, 0));
				bottomAnchor.relative = (edge.y + bottomAnchor.offset - target.volume.yMin) / target.volume.Height;
			}
			bottomAnchor.offset = 0;
		}

		public void ConvertTopOffsetToRelative() {
			Widget target = topAnchor.target;
			if (target == null) {
				topAnchor.relative = (topAnchor.relative * parentVolume.Height + topAnchor.offset) / parentVolume.Height;
			} else {
				if (target != this) {
					target.OnUpdate();
				}
				Matrix4x4 mtx = transform.parent.worldToLocalMatrix * target.transform.localToWorldMatrix;
				Vector3 edge = mtx.MultiplyPoint3x4(new Vector3(target.volume.yMin + target.volume.Height * topAnchor.relative, 0, 0));
				topAnchor.relative = (edge.y + topAnchor.offset - target.volume.yMin) / target.volume.Height;
			}
			topAnchor.offset = 0;
		}

		public void ConvertOffsetsToRelative() {
			ConvertLeftOffsetToRelative();
			ConvertRightOffsetToRelative();
			ConvertBottomOffsetToRelative();
			ConvertTopOffsetToRelative();
		}

		public override Vector3 GetLocalPosition() {
			return localPosition;
		}

		public override void SetLocalPosition(Vector3 pivotPosition) {
			//TODO: 
			// target-aware shift, z shift

			Vector3 shift = pivotPosition - localPosition;
			localPosition = pivotPosition;

			if (leftAnchor.target == null) {
				leftAnchor.offset += shift.x;
			}
			if (rightAnchor.target == null) {
				rightAnchor.offset += shift.x;
			}
			if (bottomAnchor.target == null) {
				bottomAnchor.offset += shift.y;
			}
			if (topAnchor.target == null) {
				topAnchor.offset += shift.y;
			}		
			if (backAnchor.target == null) {
				backAnchor.offset += shift.z;
			}
			if (frontAnchor.target == null) {
				frontAnchor.offset += shift.z;
			}
		}

		void OnDrawGizmos() {
			Gizmos.color = new Color(1, 1, 1, 0.4f);
			Gizmos.matrix = transform.localToWorldMatrix;
			Gizmos.DrawWireCube(
				new Vector3(volume.Width * (0.5f - pivot.x), volume.Height * (0.5f - pivot.y), volume.Depth * (0.5f - pivot.z)),
				new Vector3(volume.Width, volume.Height, volume.Depth)
			);
		}
	}
}
