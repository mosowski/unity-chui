﻿using UnityEngine;
using System.Collections;

namespace Chui {

	[ExecuteInEditMode]
	public class Content : MonoBehaviour {

		[HideInInspector]
		public Widget widget;

		public virtual void UpdateContent(Widget widget) {
			this.widget = widget;
		}

		public virtual void DeterministicStep(Widget widget) {
		}

		public virtual void ReloadContent() {
		}

		public virtual void Revalidate() {
		}

	}
}
