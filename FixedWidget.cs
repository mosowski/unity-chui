using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Chui {
	[ExecuteInEditMode]
	[AddComponentMenu("Chui/Fixed Widget")]
	public class FixedWidget : Widget {

		public Vector3 dimmensions;

		[System.NonSerialized]
		Vector3 last_dimmensions;

		public override float Width {
			get {
				return dimmensions.x;
			}
			set {
				dimmensions.x = value;
			}
		}

		public override float Height {
			get {
				return dimmensions.y;
			}
			set {
				dimmensions.y = value;
			}
		}

		public override float Depth {
			get {
				return dimmensions.z;
			}
			set {
				dimmensions.z = value;
			}
		}

		void Awake() {
			content = GetComponent<Content>();
			OnUpdate();
		}

		protected override void UpdateVolume() {
			if (dimmensions != last_dimmensions || pivot != last_pivot) {
				last_dimmensions = dimmensions;

				volume.Width = dimmensions.x;
				volume.Height = dimmensions.y;
				volume.Depth = dimmensions.z;
				volume.X = -dimmensions.x * pivot.x;
				volume.Y = -dimmensions.y * pivot.y;
				volume.Z = -dimmensions.z * pivot.z;

				changes |= ChangeBits.Transform;
			}
		}

		public override void AddTopology(List<Widget> order, int tag) {
			if (topologyUpdateTag != tag) {
				topologyUpdateTag = tag;
				order.Add(this);				
			}
		}

		void OnDrawGizmos() {
			Gizmos.color = new Color(1, 1, 1, 0.4f);
			Gizmos.matrix = transform.localToWorldMatrix;
			Gizmos.DrawWireCube(
				new Vector3(volume.Width * (0.5f - pivot.x), volume.Height * (0.5f - pivot.y), volume.Depth * (0.5f - pivot.z)),
				new Vector3(volume.Width, volume.Height, volume.Depth)
			);
		}
	}
}
