﻿#region Using statements

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#endregion

[System.Serializable]
public class StringDictionary 
{

    #region Delegates

    public delegate void ParameterValueUpdateDelegate(string key, string value);

    #endregion

    #region Fields

    [SerializeField]
    private List<string> keys = new List<string>();

    [SerializeField]
    private List<string> values = new List<string>();

    private Dictionary<string, int> keyToIndex = null;
    private ParameterValueUpdateDelegate onParameterUpdate = null;

    #endregion

    #region Public methods

    public bool Set(string key, string value)
    {
        bool changed = false;

        if(keyToIndex == null)
            CreateKeyToIndex();

        int index = -1;
        if(keyToIndex.TryGetValue(key, out index))
        {
            if(values[index] != value)
            {
                values[index] = value;
                changed = true;
            }
        }
        else
        {
            keyToIndex.Add(key, keys.Count);
            keys.Add(key);
            values.Add(value);
            changed = true;
        }

        if(changed && onParameterUpdate != null)
            onParameterUpdate(key, value);

        return changed;
    }

    public string Get(string key, string defaultValue = null)
    {
        if(keyToIndex == null)
            CreateKeyToIndex();

        int index = -1;
        if(keyToIndex.TryGetValue(key, out index))
            return values[index];
        else
            return defaultValue;
    }

    public void SetListener(ParameterValueUpdateDelegate onUpdate, bool generateEventsOnStart = true)
    {
        this.onParameterUpdate = onUpdate;
        if(generateEventsOnStart && onUpdate != null)
        {        
            for(int i = 0; i < keys.Count; ++i)
                onUpdate(keys[i], values[i]);
        }
    }

    public void GenerateUpdateEvents()
    {
        if(onParameterUpdate != null)
        {        
            for(int i = 0; i < keys.Count; ++i)
                onParameterUpdate(keys[i], values[i]);
        }
    }

    public float GetFloat(string key, float defaultValue = 0.0f)
    {
        string value = Get(key, null);
        if(value == null)
            return defaultValue;

        float result;
        if(float.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("en-US"), out result))
            return result;

        return defaultValue;
    }

    public bool SetFloat(string key, float value)
    {
        return Set(key, value.ToString(System.Globalization.CultureInfo.InvariantCulture));
    }

    public bool GetBool(string key, bool defaultValue = false)
    {
        string value = Get(key, null);
        if(value == null)
            return defaultValue;

        return (value == "true");
    }

    public bool SetBool(string key, bool value)
    {
        return Set(key, value ? "true" : "false");
    }

    #endregion

    #region Helper methods

    private void CreateKeyToIndex()
    {
        keyToIndex = new Dictionary<string, int>();
        for(int i = 0; i < keys.Count; ++i)
            keyToIndex.Add(keys[i], i);
    }

    #endregion

}
