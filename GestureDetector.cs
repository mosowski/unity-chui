﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	public class GestureDetector : MonoBehaviour {

		public enum GestureType {
			Tap,
			FreeDrag,
			HorizontalDrag,
			VerticalDrag
		}

		public enum DragAxisType {
			Horizontal,
			Vertical
		}

		[System.Serializable]
		public class GestureListenerSetup {
			public GestureType gestureType;
			public GameObject target;
		}

		public float tapThreshold = 0.01f;
		public float axisLockThreshold  = 0.01f;

		public List<GestureListenerSetup> listeners;

		protected DragListener dragListener;
		protected float fixedTimeStep;
		protected Widget widget;

		protected bool hasLockedAxis;
		protected DragAxisType lockedAxis;

		protected List<GameObject> activeListeners = new List<GameObject>();

		void Awake() {
			dragListener = Util.GetDragListener(gameObject);
			dragListener.onDragStarted.Add(OnDragStarted);
			dragListener.onDragMoved.Add(OnDragMoved);
			dragListener.onDragEnded.Add(OnDragEnded);
			dragListener.dragThreshold = 0;

			widget = GetComponent<Widget>();
			fixedTimeStep = 1.0f / Root.GetInstance().deterministicFrameRate;
		}

		protected virtual bool OnDragStarted(DragListener dl) {
			hasLockedAxis = false;
			SendStartGesture(GestureType.FreeDrag, dl, (receiver) => receiver.onFreeDragStarted.Invoke(dl) );
			return true;
		}

		protected virtual bool OnDragMoved(DragListener dl) {
			if (!hasLockedAxis) {
				Vector3 delta = widget.transform.InverseTransformVector(dl.touchInfo.position - dl.touchInfo.startPosition);
				delta.x = Mathf.Abs(delta.x);
				delta.y = Mathf.Abs(delta.y);

				if (delta.x > axisLockThreshold) {
					lockedAxis = DragAxisType.Horizontal;
					hasLockedAxis = true;
				}
				if (delta.y > axisLockThreshold && delta.y > delta.x) {
					lockedAxis = DragAxisType.Vertical;
					hasLockedAxis = true;
				}

				if (hasLockedAxis) {
					if (lockedAxis == DragAxisType.Horizontal) {
						SendStartGesture(GestureType.HorizontalDrag, dl, (receiver) => receiver.onHorizontalDragStarted.Invoke(dl) );
					} else {
						SendStartGesture(GestureType.VerticalDrag, dl, (receiver) => receiver.onVerticalDragStarted.Invoke(dl) );
					}
				}
			} else {
				if (lockedAxis == DragAxisType.Horizontal) {
					SendGesture(GestureType.HorizontalDrag, (receiver) => receiver.onHorizontalDragMoved.Invoke(dl) );
				} else {
					SendGesture(GestureType.VerticalDrag, (receiver) => receiver.onVerticalDragMoved.Invoke(dl) );
				}
			}

			SendGesture(GestureType.FreeDrag, (receiver) => receiver.onFreeDragMoved.Invoke(dl) );

			return true;
		}

		protected virtual bool OnDragEnded(DragListener dl) {
			if (hasLockedAxis) {
				if (lockedAxis == DragAxisType.Horizontal) {
					SendEndGesture(GestureType.HorizontalDrag, (receiver) => receiver.onHorizontalDragEnded.Invoke(dl) );
				} else {
					SendEndGesture(GestureType.VerticalDrag, (receiver) => receiver.onVerticalDragEnded.Invoke(dl) );
				}
			}

			SendEndGesture(GestureType.FreeDrag, (receiver) => receiver.onFreeDragEnded.Invoke(dl) );

			if ((dl.touchInfo.position - dl.touchInfo.startPosition).magnitude < tapThreshold * Root.PhysicalWidth) {
				SendGesture(GestureType.Tap, (receiver) => receiver.onTapped.Invoke(dl) );
			}
			return true;
		}

		void SendStartGesture(GestureType gestureType, DragListener dl, Action<GestureListener> cb) {
			foreach (var r in listeners) {
				if (r.gestureType == gestureType) {
					if (Root.GetInstance().CheckHit(dl.touchInfo.rawPosition, r.target)) {
						activeListeners.Add(r.target);
						cb(r.target.GetComponent<GestureListener>());
					}
				}
			}
		}
		void SendGesture(GestureType gestureType, Action<GestureListener> cb) {
			foreach (var r in listeners) {
				if (r.gestureType == gestureType && activeListeners.Contains(r.target)) {
					cb(r.target.GetComponent<GestureListener>());
				}
			}
		}

		void SendEndGesture(GestureType gestureType, Action<GestureListener> cb) {
			foreach (var r in listeners) {
				if (r.gestureType == gestureType && activeListeners.Contains(r.target)) {
					cb(r.target.GetComponent<GestureListener>());
					activeListeners.Remove(r.target);
				}
			}
		}

		public void RegisterListener(GameObject target, GestureType gestureType) {
			foreach (var r in listeners) {
				if (r.target == target && r.gestureType == gestureType) {
					return;
				}
			}
			var setup = new GestureListenerSetup();
			setup.target = target;
			setup.gestureType = gestureType;
			listeners.Add(setup);
		}

		public void UnregisterListener(GameObject target, GestureType gestureType) {
			GestureListenerSetup setup = null;
			foreach (var r in listeners) {
				if (r.target == target && r.gestureType == gestureType) {
					setup = r;
					break;
				}
			}
			if (setup != null) {
				listeners.Remove(setup);
			}
		}

		void Update() {
			activeListeners.RemoveAll(t => t == null);
		}
	}
}