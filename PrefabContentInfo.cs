﻿using UnityEngine;
using System.Collections;

namespace Chui {

	[AddComponentMenu("Chui/Prefab Content Info")]
	public class PrefabContentInfo : MonoBehaviour {
		public float width;
		public float height;
		public float depth;

		void OnDrawGizmos() {
			Gizmos.color = new Color(0.4f, 1, 1, 1);
			Gizmos.matrix = transform.localToWorldMatrix;
			Gizmos.DrawWireCube(
				new Vector3(width, height, -depth) * 0.5f,
				new Vector3(width, height, -depth)
			);
		}
	}
}
