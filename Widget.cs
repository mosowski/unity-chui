using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	[System.Serializable]
	public class Volume {
		public float xMin;
		public float yMin;
		public float zMin;
		public float xMax;
		public float yMax;
		public float zMax;

		public float Width {
			get {
				return xMax - xMin;
			}
			set {
				xMax = xMin + value;
			}
		}

		public float Height {
			get {
				return yMax - yMin;
			}
			set {
				yMax = yMin + value;
			}
		}

		public float Depth {
			get {
				return zMax - zMin;
			}
			set {
				zMax = zMin + value;
			}
		}

		public float X {
			get {
				return xMin;
			}
			set {
				xMax += value - xMin;
				xMin = value;
			}
		}

		public float Y {
			get {
				return yMin;
			}
			set {
				yMax += value - yMin;
				yMin = value;
			}
		}

		public float Z {
			get {
				return zMin;
			}
			set {
				zMax += value - zMin;
				zMin = value;
			}
		}

		public Vector3 Min {
			get {
				return new Vector3(xMin, yMin, zMin);
			}
		}

		public Vector3 Max {
			get {
				return new Vector3(xMax, yMax, zMax);
			}
		}

		public Vector3 Size {
			get {
				return new Vector3(Width, Height, Depth);
			}
		}

		public Volume Copy() {
			return new Volume() {
				xMin = xMin,
				yMin = yMin,
				zMin = zMin,
				xMax = xMax,
				yMax = yMax,
				zMax = zMax
			};
		}

		public void Set(Volume v) {
			xMin = v.xMin;
			yMin = v.yMin;
			zMin = v.zMin;
			xMax = v.xMax;
			yMax = v.yMax;
			zMax = v.zMax;
		}

		public bool IsEqual(Volume v) {
			return xMin == v.xMin
				&& yMin == v.yMin
				&& xMax == v.xMax
				&& yMax == v.yMax
				&& zMin == v.zMin
				&& zMax == v.zMax;
		}

		public Vector3 GetPoint(Vector3 p) {
			return new Vector3(
				xMin + Width * p.x,
				yMin + Height * p.y,
				zMin + Depth * p.z
			);
		}

        public override string ToString()
        {
            return string.Format("[Volume: Width={0}, Height={1}, Depth={2}, X={3}, Y={4}, Z={5}, Min={6}, Max={7}, Size={8}]", Width, Height, Depth, X, Y, Z, Min, Max, Size);
        }
	}

	public abstract class Widget : MonoBehaviour {
		public class ChangeBits {
			public static readonly uint None = 0;
			public static readonly uint Transform = 1;
			public static readonly uint NonTransform = 2;
			public static readonly uint Collider = 4;
		}

		[System.NonSerialized]
		public Volume volume = new Volume();
		[System.NonSerialized]
		public Color derivedColor = Color.white;
		[System.NonSerialized]
		protected Content content;
		[System.NonSerialized]
		protected BoxCollider boxCollider;

		[System.NonSerialized]
		int lastUpdateFrame = -1;
		[System.NonSerialized]
		public uint changes = ChangeBits.None;
		[System.NonSerialized]
		public bool isInitialized = false;

		public Vector3 pivot = new Vector3(0.5f, 0.5f, 0.0f);

		public Color color = Color.white;
		public bool isColliderEnabled;
		public bool isColliderAutoResizeEnabled;

		public bool isStatic = false;

		protected Color last_derivedColor = Color.white;
		protected Vector3 last_pivot;
		protected bool last_isColliderEnabled;
		protected bool last_isColliderAutoResizeEnabled;
		protected bool last_isActiveSelf;
		protected Transform last_parent;

		protected Widget parentWidget;

		protected int topologyUpdateTag = 0;

		static int topologyUpdateTagCounter = 1;

		public virtual float Width {
			get {
				return volume.Width;
			}
			set {
				volume.Width = value;
			}
		}

		public virtual float Height {
			get {
				return volume.Height;
			}
			set {
				volume.Height = value;
			}
		}

		public virtual float Depth {
			get {
				return volume.Depth;
			}
			set {
				volume.Depth = value;
			}
		}

		public virtual Color Color {
			get {
				return color;
			}
			set {
				color = value;
			}
		}

		public Color DerivedColor {
			get {
				return derivedColor;
			}
		}

		public bool HasChanged {
			get {
				return changes != 0;
			}
		}

		public virtual void Translate(float x, float y, float z = 0) {
			transform.Translate(x, y, z);
		}

		public virtual Vector3 GetLocalPosition() {
			return transform.localPosition;
		}

		public virtual void SetLocalPosition(Vector3 newLocalPosition) {
			transform.localPosition = newLocalPosition;
		}
		
		public void SetWorldPosition(Vector3 newWorldPosition) {
			if (transform.parent != null) {
				SetLocalPosition(transform.parent.InverseTransformPoint(newWorldPosition));
			} else {
				SetLocalPosition(newWorldPosition);
			}
		}

		public void SetParent(GameObject parent) {
			SetParent(parent.GetComponent<Widget>());
		}

		public virtual void SetParent(Widget parent) {
			transform.SetParent(parent.transform, false);
		}

		public Rect GetWorldRect() {
			Vector3 bottom = transform.TransformPoint(0, -Height * (pivot.y), 0);
			Vector3 top = transform.TransformPoint(0, Height * (1 - pivot.y), 0);
			Vector3 left = transform.TransformPoint(-Width * pivot.x, 0, 0);
			Vector3 right = transform.TransformPoint(Width * (1 - pivot.x), 0, 0);
			Rect rect = new Rect(left.x, bottom.x, right.x - left.x, top.y - bottom.y);
			return rect;
		}

		protected virtual void UpdateVolume() {
		}

		public virtual void DeterministicStep() {
		}

		public void Revalidate() {
			isInitialized = false;
			lastUpdateFrame = -1;
			OnUpdate();
		}

		static int totalUpdates;
		static int saveUpdates;
		static int staticUpdates;
		static int revalidateUpdates;
		public static int volumeUpdates;

		public void OnUpdate() {
			if (isInitialized) {
				if (isStatic) {
					return;
				} else if (lastUpdateFrame == Time.frameCount) {
					return;
				}
				changes = 0;
			} else {
				isInitialized = true;
				changes = ChangeBits.Transform;
			}

			lastUpdateFrame = Time.frameCount;


			if (transform.parent != last_parent) {
				last_parent = transform.parent;
				parentWidget = last_parent.GetComponent<Widget>();
				changes |= ChangeBits.Transform;
			}

			if (parentWidget != null) {
				if ((parentWidget.changes & ChangeBits.Transform) != 0) {
					changes |= ChangeBits.Transform;
				}
				if (!parentWidget.isInitialized) {
					isInitialized = false;
				}
				derivedColor = parentWidget.derivedColor * color;
			} else {
				derivedColor = color;
			}

			if (pivot != last_pivot) {
				changes |= ChangeBits.Transform;
			}
			
			bool isActiveSelf = gameObject.activeSelf;
			if (isActiveSelf != last_isActiveSelf) {
				last_isActiveSelf = isActiveSelf;
				changes |= ChangeBits.Transform;
			}

			if (derivedColor != last_derivedColor) {
				last_derivedColor = derivedColor;
				changes |= ChangeBits.NonTransform;
			}

			UpdateVolume();

			if (isColliderEnabled != last_isColliderEnabled) {
				last_isColliderEnabled = isColliderEnabled;
				changes |= ChangeBits.Collider;
			}

			if (isColliderAutoResizeEnabled != last_isColliderAutoResizeEnabled) {
				last_isColliderAutoResizeEnabled = isColliderAutoResizeEnabled;
				changes |= ChangeBits.Collider;
			}

			if ((changes & (ChangeBits.Transform | ChangeBits.Collider)) != 0) {
				last_pivot = pivot;

				UpdateCollider();
			}
			UpdateContent();
		}

		protected void UpdateContent() {
#if UNITY_EDITOR
			content = GetComponent<Content>();
#endif
			if (content != null) {
				content.UpdateContent(this);
			}
		}

		protected virtual void UpdateCollider() {
			if (isColliderEnabled) {
				if (boxCollider == null) {
					boxCollider = gameObject.GetComponent<BoxCollider>();
					if (boxCollider == null) {
						boxCollider = gameObject.AddComponent<BoxCollider>();
						boxCollider.enabled = true;
						boxCollider.isTrigger = true;
					}
				}

				if (isColliderAutoResizeEnabled) {
					boxCollider.size = new Vector3(volume.Width, volume.Height, volume.Depth);
					boxCollider.center = new Vector3(volume.Width * (0.5f - pivot.x), volume.Height * (0.5f - pivot.y), volume.Depth * (0.5f - pivot.z));
				}

				var rigidBody  = gameObject.GetComponent<Rigidbody>();
				if (rigidBody == null) {
					rigidBody = gameObject.AddComponent<Rigidbody>();
					rigidBody.isKinematic = true;
					rigidBody.useGravity = false;
				}
			} else if (boxCollider != null) {
				var rigidBody = GetComponent<Rigidbody>();

				boxCollider.enabled = false;
				if (Application.isPlaying) {
					if (rigidBody != null) {
						GameObject.Destroy(rigidBody);
					}
					GameObject.Destroy(boxCollider);
				} else {
					if (rigidBody != null) {
						GameObject.DestroyImmediate(rigidBody);
					}
					GameObject.DestroyImmediate(boxCollider);
				}
				boxCollider = null;

			}
		}

		public void RecursiveDeterministicStep() {
			DeterministicStep();

			if (content != null) {
				content.DeterministicStep(this);
			}

			for (int i = 0; i < transform.childCount; ++i) {
				Widget w = transform.GetChild(i).GetComponent<Widget>();
				if (w != null && w.gameObject.active) {
					w.RecursiveDeterministicStep();
				}
			}
		}

		public void RevalidateHierarchy() {
			var ws = GetComponentsInChildren<Widget>(false);
			foreach (var w in ws) {
				w.Revalidate();
			}
		}

		public void UpdateHierarchy() {
			var ws = GetComponentsInChildren<Widget>(false);
			foreach (var w in ws) {
				w.OnUpdate();
			}
		}

		public void RevalidateTopology() {
			Widget.topologyUpdateTagCounter++;

			List<Widget> order = new List<Widget>();		
			var ws = GetComponentsInChildren<Widget>(false);
			foreach (var w in ws) {
				w.AddTopology(order, Widget.topologyUpdateTagCounter);				
			}			

			foreach (var w in order) {
				w.Revalidate();
			}
		}

		public virtual void AddTopology(List<Widget> order, int tag) {
		}
	}
}
