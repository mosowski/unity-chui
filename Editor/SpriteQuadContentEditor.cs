﻿using UnityEditor;
using UnityEngine;

namespace Chui {
	[CustomEditor(typeof(SpriteQuadContent)), CanEditMultipleObjects]
	public class SpriteQuadContentEditor : QuadContentEditor {
		private SerializedProperty spriteProperty;
		private SerializedProperty scale9SizeModifierProperty;

		[MenuItem("Chui/Sprite/Change QuadContent to SpriteQuadContent")]
		static void ChangeQuadContentToSpriteQuadContent() {
			foreach (GameObject gameObject in Selection.gameObjects) {
				if (gameObject != null) {
					foreach (QuadContent quadContent in gameObject.GetComponentsInChildren<QuadContent>(true)) {
						if (quadContent.GetType() == typeof(QuadContent)) {
							Debug.LogFormat("Perform: {0}: {1}", quadContent.transform.root.name, quadContent.name);
							SpriteQuadContent spriteQuadContent = quadContent.gameObject.AddComponent<SpriteQuadContent>();
							spriteQuadContent.geometryMode = quadContent.geometryMode;
							spriteQuadContent.uFlip = quadContent.uFlip;
							spriteQuadContent.vFlip = quadContent.vFlip;
							spriteQuadContent.fillBorderCenter = quadContent.fillBorderCenter;
							spriteQuadContent.tileSettings = quadContent.tileSettings;
							spriteQuadContent.color = quadContent.color;
							spriteQuadContent.widget = quadContent.widget;
							if (quadContent.material != null) {
								if (quadContent.material.shader != spriteQuadContent.DefaultMaterial.shader) {
									spriteQuadContent.material = quadContent.material;
								} else {
									spriteQuadContent.material = spriteQuadContent.DefaultMaterial;
								}
								Texture texture = quadContent.material.mainTexture;
								string path = AssetDatabase.GetAssetPath(texture);
								spriteQuadContent.Sprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
								if (spriteQuadContent.Sprite == null) {
									Debug.LogErrorFormat(spriteQuadContent, "{0} null sprite: {1}, texture: {2}, path: {3}", spriteQuadContent.transform.root.name, quadContent.name, quadContent.material.mainTexture, path);
								}
							}
							DestroyImmediate(quadContent, true);
						}
					}
				}
			}
		}

		protected override void OnEnable() {
			base.OnEnable();
			spriteProperty = serializedObject.FindProperty("sprite");
			scale9SizeModifierProperty = serializedObject.FindProperty("scale9SizeModifier");
		}

		protected override void DrawUVRect() {
		}

		protected override void DrawBorder() {
		}

		protected override void DrawScale9Params() {
			DrawFillBorderCenter();
			DrawScale9Modifier();
		}

		protected override void DrawMaterialAndColor() {
			DisplayProperty(spriteProperty, UpdateSprite);
			base.DrawMaterialAndColor();
		}

		private void DrawScale9Modifier() {
			DisplayProperty(scale9SizeModifierProperty, UpdateSprite);
		}

		private void UpdateSprite(QuadContent spriteQuadContent) {
			serializedObject.ApplyModifiedProperties();
			spriteQuadContent.Revalidate();
		}
	}
}
