﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Chui {

	public abstract class AbstractEditor<Type> : Editor where Type : Object {

		protected EditorUtil.Foldouts foldouts = new EditorUtil.Foldouts();
		
		protected void PerformActionOnAllTargetObjects(Action<Type> action, string actionName) {
			Undo.RecordObjects(serializedObject.targetObjects, actionName);
			foreach (Type targetObject in serializedObject.targetObjects) {
				action(targetObject);
				EditorUtility.SetDirty(targetObject);
			}
			serializedObject.ApplyModifiedProperties();
			serializedObject.Update();
		}

		protected void PerformActionOnAllTargetObjects(SerializedProperty property, Action<Type, SerializedProperty> action, string actionName) {
			Undo.RecordObjects(serializedObject.targetObjects, actionName);
			foreach (Type targetObject in serializedObject.targetObjects) {
				action(targetObject, property);
				EditorUtility.SetDirty(targetObject);
			}
			serializedObject.ApplyModifiedProperties();
			serializedObject.Update();
		}

		protected void PerformActionOnAllTargetObjects(SerializedProperty property, Action<Type, SerializedProperty> action) {
			PerformActionOnAllTargetObjects(property, action, GetActionName(property));
		}

		protected void DisplayProperty(SerializedProperty property, Action<Type, SerializedProperty> action, string actionName) {
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(property);
			if (EditorGUI.EndChangeCheck()) {
				PerformActionOnAllTargetObjects(property, action, actionName);
			}
		}

		protected void DisplayProperty(SerializedProperty property, Action<Type> action, string actionName) {
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(property);
			if (EditorGUI.EndChangeCheck()) {
				PerformActionOnAllTargetObjects(action, actionName);
			}
		}

		protected void DisplayProperty(SerializedProperty property, Action<Type, SerializedProperty> action) {
			DisplayProperty(property, action, GetActionName(property));
		}

		protected void DisplayProperty(SerializedProperty property, Action<Type> action)
		{
			DisplayProperty(property, action, GetActionName(property));
		}

		protected void DisplayTextArea(SerializedProperty property, Action<Type, SerializedProperty> action, string actionName) {
			EditorGUI.BeginChangeCheck();
			string text = EditorGUILayout.TextArea(property.stringValue);
			if (EditorGUI.EndChangeCheck()) {
				property.stringValue = text;
				PerformActionOnAllTargetObjects(property, action, actionName);
			}
		}

		protected void DisplayTextArea(SerializedProperty property, Action<Type, SerializedProperty> action) {
			DisplayTextArea(property, action, GetActionName(property));
		}

		protected E GetEnum<E>(SerializedProperty property) where E : struct, IConvertible, IComparable, IFormattable {
			if (!typeof(E).IsEnum) {
				throw new ArgumentException("E must be an Enum type");
			}
			return (E)Enum.Parse(typeof(E), property.enumNames[property.enumValueIndex]);
		}

		private string GetActionName(SerializedProperty property) {
			return "Set " + property.displayName;
		}
	}
}
