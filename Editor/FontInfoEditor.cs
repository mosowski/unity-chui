﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	[CustomEditor(typeof(FontInfo))]
	public class FontInfoEditor : Editor {

		class GlyphAtlasSource {
			public FontInfo.GlyphSource glyphSource;
			public FontInfo.GlyphInfo glyph;
			public int x;
			public int y;
		}

		class MaterialPage {
			public string name;
			public Material material;
			public int page;
		}

		FontInfo fontInfo;
		AtlasBuilder atlasBuilder;
		Dictionary<string, MaterialPage> materials;
		List<Texture2D> pagesTextures;

		FontInfo glyphsSourceFontInfo;
		Material newGlyphMaterial;

		[MenuItem("Chui/New/Font")]
		public static void CreateFontInfoAsset() {
			EditorUtil.CreateCustomAsset<FontInfo>();
		}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			fontInfo = target as FontInfo;

			fontInfo.useSquareAtlas = EditorGUILayout.Toggle("Use square atlas", fontInfo.useSquareAtlas);

			if (GUILayout.Button("Rebuild")) {
				Rebuild();
			}

			if (GUILayout.Button("Update icons offsets")) {
				UpdateIconsOffsets();
			}

			glyphsSourceFontInfo = EditorGUILayout.ObjectField("Glyphs source", glyphsSourceFontInfo, typeof(FontInfo)) as FontInfo;
			if (glyphsSourceFontInfo != null) {
				newGlyphMaterial = EditorGUILayout.ObjectField("New glyphs material", newGlyphMaterial, typeof(Material)) as Material;
				if (GUILayout.Button("Import glyphs")) {
					ImportGlyphs();
				}
			}
		}

		void ImportGlyphs() {
			foreach (var source in glyphsSourceFontInfo.glyphsList) {
				FontInfo.GlyphInfo dest = null;
				foreach (var mine in fontInfo.glyphsList) {
					if (mine.id == source.id) {
						dest = mine;
						break;
					}
				}
				if (dest == null) {
					dest = new FontInfo.GlyphInfo() {
						id = source.id,
						material = newGlyphMaterial
					};
					fontInfo.glyphsList.Add(dest);
				}
				dest.geom = source.geom;
				dest.uv = source.uv;
				dest.advance = source.advance;
				dest.isColorEnabled = source.isColorEnabled;
			}

			foreach (var source in glyphsSourceFontInfo.iconSources) {
				FontInfo.IconSource dest = null;
				foreach (var mine in fontInfo.iconSources) {
					if (mine.id == source.id) {
						dest = mine;
						break;
					}
				}
				if (dest == null) {
					dest = new FontInfo.IconSource();
					dest.id = source.id;
					fontInfo.iconSources.Add(dest);
				}
				dest.offset = source.offset;
				dest.scale = source.scale;
				dest.shader = source.shader;
				dest.isColorEnabled = source.isColorEnabled;
				dest.texture = source.texture;
				dest.code = source.code;
			}

			EditorUtility.SetDirty(fontInfo);
		}

		void UpdateIconsOffsets() {
			foreach (FontInfo.IconSource iconSource in fontInfo.iconSources) {
				FontInfo.GlyphInfo glyph = fontInfo.glyphsList.Find(g => g.id == iconSource.id);
				if (glyph != null && iconSource.texture != null) {
					glyph.geom = new Rect(iconSource.offset.x, iconSource.offset.y, iconSource.texture.width * iconSource.scale, iconSource.texture.height * iconSource.scale);
					glyph.advance = iconSource.texture.width * iconSource.scale;
				}
			}
			EditorUtility.SetDirty(fontInfo);
			Debug.Log("[Chui.FontInfoEditor] Updated icon offsets.");
		}

		Material AddMaterial(string materialName, Shader shader, int page) {
			if (!materials.ContainsKey(materialName)) {
				string materialPath = AssetDatabase.GetAssetPath(fontInfo).Replace(fontInfo.name + ".asset", fontInfo.name + "-" + materialName + ".mat");
                Material m = AssetDatabase.LoadAssetAtPath(materialPath, typeof(Material)) as Material;
				if (m == null || m.shader != shader) {
					MaterialPage mp = new MaterialPage() {
						material = new Material(shader),
						page = page,
						name = materialName
					};
					Debug.Log("Material Path: " + materialPath);
					AssetDatabase.CreateAsset(mp.material, materialPath);
					AssetDatabase.ImportAsset(materialPath);
					materials[materialName] = mp;
					return mp.material;
				} else {
					return m;
				}
			} else {
				return materials[materialName].material;
			}
		}

		void Rebuild() {
			atlasBuilder = new AtlasBuilder();
			atlasBuilder.Init(fontInfo.useSquareAtlas);

			fontInfo.glyphsList.Clear();
			fontInfo.kerningsList.Clear();

			for (int i = 0; i < fontInfo.glyphSources.Count; ++i) {
				string srcTexturePath = AssetDatabase.GetAssetPath (fontInfo.glyphSources[i].texture);
				TextureImporter srcTextureImporter = AssetImporter.GetAtPath (srcTexturePath) as TextureImporter;
				srcTextureImporter.maxTextureSize = 2048;
				srcTextureImporter.textureFormat = TextureImporterFormat.ARGB32;
				srcTextureImporter.isReadable = true;
				AssetDatabase.ImportAsset (srcTexturePath);

				ParseGlyphSource(fontInfo.glyphSources[i]);
			}

			foreach (FontInfo.IconSource iconSource in fontInfo.iconSources) {
				if (iconSource.texture != null) {
					atlasBuilder.AddSource(iconSource, iconSource.texture.width, iconSource.texture.height, 2);
				} else {
					Debug.LogError("[Chui.FontInfoEditor] Icon source has no texture set.");
					return;
				}
			}

			if (!atlasBuilder.Build()) {
				Debug.LogError("[Chui.FontInfoEditor] Atlas could not be build.");
				return;
			}

			List<Color32[]> pagePixels = new List<Color32[]>();
			for (int i = 0; i < atlasBuilder.pages.Count; ++i) {
				pagePixels.Add(new Color32[atlasBuilder.pages[i].width * atlasBuilder.pages[i].height]);
			}

			Dictionary<Texture2D, Color32[]> sourcesPixels = new Dictionary<Texture2D, Color32[]>();

			materials = new Dictionary<string, MaterialPage>();

			for (int i = 0; i < atlasBuilder.sources.Count; ++i) {
				AtlasBuilder.Source src = atlasBuilder.sources[i];
				AtlasBuilder.Node page = atlasBuilder.pages[src.page];

				if (src.owner is GlyphAtlasSource) {
					GlyphAtlasSource gas = src.owner as GlyphAtlasSource;
					Texture2D srcTexture = gas.glyphSource.texture;
					Color32[] srcPixels;
					if (!sourcesPixels.TryGetValue(srcTexture, out srcPixels)) {
						sourcesPixels[srcTexture] = srcPixels = srcTexture.GetPixels32();
					}
					Util.CopyPixels(srcPixels, srcTexture.width, gas.x, gas.y, pagePixels[src.page], page.width, src.node.x, src.node.y, src.width, src.height);

					string materialName = EditorUtil.GetAssetFileNameWithoutExtension(gas.glyphSource.shader) + "-page-" + src.page;
					Material material = AddMaterial(materialName, gas.glyphSource.shader, src.page);

					gas.glyph.isColorEnabled = true;
					gas.glyph.material = material;
					gas.glyph.uv = new Rect(
						(float)src.node.x / page.width,
						1 - (float)src.node.y / page.height,
						(float)src.width / page.width,
						-(float)src.height / page.height
					);

					fontInfo.glyphsList.Add(gas.glyph);

					if (gas.glyphSource.shader == null) {
						Debug.LogError("[Chui.FontInfoEditor] Missing shader for glyph source");
						return;
					}

				} else if (src.owner is FontInfo.IconSource) {
					FontInfo.IconSource iconSource = src.owner as FontInfo.IconSource;
					Color32[] srcPixels;
					if (!sourcesPixels.TryGetValue(iconSource.texture, out srcPixels)) {
						sourcesPixels[iconSource.texture] = srcPixels = iconSource.texture.GetPixels32();
					}
					Util.CopyPixels(srcPixels, iconSource.texture.width, 0, 0, pagePixels[src.page], page.width, src.node.x, src.node.y, src.width, src.height);

					string materialName = EditorUtil.GetAssetFileNameWithoutExtension(iconSource.shader) + "-page-" + src.page;
					Material material = AddMaterial(materialName, iconSource.shader, src.page);

					FontInfo.GlyphInfo glyph = new FontInfo.GlyphInfo();
					glyph.id = iconSource.id;
					glyph.isColorEnabled = iconSource.isColorEnabled;
					glyph.material = material;
					glyph.geom = new Rect(iconSource.offset.x, iconSource.offset.y, iconSource.texture.width * iconSource.scale, iconSource.texture.height * iconSource.scale);
					glyph.advance = iconSource.texture.width * iconSource.scale;
					glyph.uv = new Rect(
						(float)src.node.XPadded / page.width,
						1 - (float)src.node.YPadded / page.height,
						(float)src.width / page.width,
						-(float)src.height / page.height
					);
					fontInfo.glyphsList.Add(glyph);

					if (iconSource.shader == null) {
						Debug.LogError("[Chui.FontInfoEditor] Missing shader for icon source");
						return;
					}

				}
			}
			fontInfo.DeserializeData();

			pagesTextures = new List<Texture2D>();

			for (int i = 0; i < atlasBuilder.pages.Count; ++i) {
				Texture2D t = new Texture2D(atlasBuilder.pages[i].width, atlasBuilder.pages[i].height);
				t.SetPixels32(pagePixels[i]);
				t.Apply(true);

				string texturePath = AssetDatabase.GetAssetPath(fontInfo).Replace(fontInfo.name + ".asset", fontInfo.name + "-page-" + i + ".png");
				EditorUtil.WriteTextureToAsset(t, texturePath);
				DestroyImmediate(t);

				TextureImporter textureImporter = (TextureImporter)AssetImporter.GetAtPath(texturePath);
				textureImporter.textureType = TextureImporterType.Advanced;
				textureImporter.maxTextureSize = 2048;
				textureImporter.mipmapEnabled = false;
				textureImporter.alphaIsTransparency = true;
                textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
				AssetDatabase.ImportAsset(texturePath);

				pagesTextures.Add(AssetDatabase.LoadAssetAtPath<Texture2D>(texturePath));
			}
			Debug.Log("[Chui.FontInfoEditor] Saved atlas textures.");

			foreach (var item in materials) {
				item.Value.material.mainTexture = pagesTextures[item.Value.page];
			}
			AssetDatabase.SaveAssets();
			Debug.Log("[Chui.FontInfoEditor] Generated materials.");

			EditorUtility.SetDirty(fontInfo);
			Debug.Log("[Chui.FontInfoEditor] Performed full rebuild.");

            for (int i = 0; i < fontInfo.glyphSources.Count; ++i) {
                string srcTexturePath = AssetDatabase.GetAssetPath (fontInfo.glyphSources[i].texture);
                TextureImporter srcTextureImporter = AssetImporter.GetAtPath (srcTexturePath) as TextureImporter;
                srcTextureImporter.maxTextureSize = 16;
                srcTextureImporter.textureFormat = TextureImporterFormat.PVRTC_RGB2;
                AssetDatabase.ImportAsset (srcTexturePath);
            }
		}

		void ParseGlyphSource(FontInfo.GlyphSource gs) {
			Debug.Log("[Chui.FontInfoEditor] Loaded glyphs from " + gs.description.name);
			string[] lines = gs.description.text.Split('\n');
			foreach (string line in lines) {
				if(line.IndexOf("char ") == 0) {
					FontInfo.GlyphInfo glyph = new FontInfo.GlyphInfo();
					int width = int.Parse(GetFontDescriptionField(line, "width="));
					int height = int.Parse(GetFontDescriptionField(line, "height="));
					glyph.geom = new Rect(
						float.Parse(GetFontDescriptionField(line, "xoffset=")) / 100,
						(-float.Parse(GetFontDescriptionField(line, "yoffset=")) - height) / 100,
						(float)width / 100,
						(float)height / 100
					);
					glyph.advance = float.Parse(GetFontDescriptionField(line, "xadvance=")) / 100;
					glyph.id = int.Parse(GetFontDescriptionField(line, "id="));

					GlyphAtlasSource glyphAtlasSource = new GlyphAtlasSource() {
						glyphSource = gs,
						glyph = glyph,
						x = int.Parse(GetFontDescriptionField(line, "x=")),
						y = int.Parse(GetFontDescriptionField(line, "y="))
					};

					atlasBuilder.AddSource(glyphAtlasSource, width, height, 2);
				} else if (line.IndexOf("common ") == 0) {
					fontInfo.lineHeight = float.Parse(GetFontDescriptionField(line, "lineHeight=")) / 100;
					fontInfo.baseSize = float.Parse(GetFontDescriptionField(line, "base=")) / 100;
				} else if (line.IndexOf("kerning ") == 0) {
					FontInfo.Kerning k = new FontInfo.Kerning();
					k.from = int.Parse(GetFontDescriptionField(line, "first="));
					k.to = int.Parse(GetFontDescriptionField(line, "second="));
					k.amount = float.Parse(GetFontDescriptionField(line, "amount=")) / 100;
					fontInfo.kerningsList.Add(k);
				}
			}
		}

		string GetFontDescriptionField(string input, string field) {
			int start = input.IndexOf(field) + field.Length;
			int end = input.IndexOf(" ", start);
			if (end == -1) {
				return input.Substring(start);
			} else {
				return input.Substring(start, end - start);
			}
		}
	}
}
