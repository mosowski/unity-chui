﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {
	public class ChuiPropertyDrawer : PropertyDrawer {
		public Rect masterRect;
		public Rect lineRect;
		public int indentLevel = 0;

		public virtual float LineHeight { get { return 15; } }
		public virtual float LineSpacing { get { return 3; } }
		public virtual float IndentWidth { get { return 4; } }

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			masterRect = position;
			lineRect = position;
			lineRect.height = LineHeight;
		}

		public void Indent(int n) {
			indentLevel += n;
			lineRect.position = new Vector2(masterRect.position.x + indentLevel * IndentWidth, lineRect.position.y);
			lineRect.width = masterRect.width - indentLevel * IndentWidth;
		}

		public void NewLine(float extraSpacing = 0) {
			lineRect.position = new Vector2(masterRect.position.x + indentLevel * IndentWidth, lineRect.position.y  + LineHeight + LineSpacing + extraSpacing);
			lineRect.width = masterRect.width - indentLevel * IndentWidth;
		}

		public void Label(string label) {
			Label(label, GUIStyle.none);
		}

		public void Label(string label, GUIStyle style) {
			EditorGUI.LabelField(lineRect, label, style);
			NewLine();
		}

		public void PropertyLine(SerializedProperty property) {
			EditorGUI.PropertyField(lineRect, property);
			NewLine();
		}

		public Rect ProportionaLineRect(float begin, float end) {
			Rect rect = lineRect;
			rect.position = new Vector2(lineRect.position.x + lineRect.width * begin, lineRect.position.y);
			rect.width = lineRect.width * (end - begin);
			return rect;
		}

		public float GetLinesHeight(int numLines) {
			return numLines * LineHeight + (numLines - 1) * LineSpacing;
		}
	}
}
