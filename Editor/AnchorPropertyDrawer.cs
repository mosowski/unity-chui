﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {
	[CustomPropertyDrawer(typeof(Anchor))]
	public class AnchorPropertyDrawer : ChuiPropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			base.OnGUI(position, property, label);
			Label(label.text, EditorStyles.boldLabel);
			PropertyLine(property.FindPropertyRelative("target"));

			EditorGUIUtility.labelWidth = 50;
			EditorGUI.PropertyField(ProportionaLineRect(0.0f, 0.45f), property.FindPropertyRelative("offset"));
			EditorGUI.PropertyField(ProportionaLineRect(0.55f, 1.0f), property.FindPropertyRelative("relative"));
			NewLine();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			return GetLinesHeight(3);
		}
	}
}