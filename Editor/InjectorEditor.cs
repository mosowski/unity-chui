﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;

namespace Chui {

	[CustomEditor(typeof(Injector))]
	public class InjectorEditor : Editor {

		Injector injector;

		public override void OnInspectorGUI() {
			injector = target as Injector;
			var autoSetInstanceLayers = EditorGUILayout.Toggle ("Auto Set Instance Layers", injector.autoSetInstanceLayers);
			injector.autoSetInstanceLayers = autoSetInstanceLayers;
			var autoSetInstanceStatic = EditorGUILayout.Toggle ("Auto Static", injector.autoSetInstanceStatic);
			injector.autoSetInstanceStatic = autoSetInstanceStatic;

			GameObject prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", injector.prefab, typeof(GameObject), false);
			if (prefab != injector.prefab) {
				if (prefab == null || prefab.GetComponent<AnchoredWidget>() != null) {
					Undo.RecordObject(injector , "Set Injector Prefab");
					injector.prefab = prefab;
					injector.Revalidate();
					EditorUtility.SetDirty(injector);
				}
			}

			if (prefab != null) {
				if (GUILayout.Button("Revalidate")) {
					injector.Revalidate();
				}
			}
		}
	}
}
