﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {

	[CustomEditor(typeof(PrefabContent))]
	public class PrefabContentEditor : Editor {

		PrefabContent prefabContent;

		public override void OnInspectorGUI() {
			prefabContent = target as PrefabContent;

			EditorGUI.BeginChangeCheck();
			bool isContainerLocked = EditorGUILayout.Toggle("Lock container", prefabContent.isContainerLocked);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(prefabContent, "Set Prefab Content Container Locked");
				prefabContent.isContainerLocked = isContainerLocked;
				EditorUtility.SetDirty(prefabContent);
			}

			if (!isContainerLocked) {
				EditorGUI.BeginChangeCheck();
				GameObject newPrefab = EditorGUILayout.ObjectField("Prefab", prefabContent.prefab, typeof(GameObject), false) as GameObject;
				if (EditorGUI.EndChangeCheck() && newPrefab.GetComponent<PrefabContentInfo>() != null) {
					Undo.RecordObject(prefabContent, "Set Prefab Content Prefab");
					prefabContent.prefab = newPrefab;
					EditorUtility.SetDirty(prefabContent);
				}
			}

			EditorGUI.BeginChangeCheck();
			bool isScaleXEnabled = EditorGUILayout.Toggle("Scale X", prefabContent.isScaleXEnabled);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(prefabContent, "Set Prefab Content Scale X Enabled");
				prefabContent.isScaleXEnabled = isScaleXEnabled;
				EditorUtility.SetDirty(prefabContent);
			}

			EditorGUI.BeginChangeCheck();
			bool isScaleYEnabled = EditorGUILayout.Toggle("Scale Y", prefabContent.isScaleYEnabled);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(prefabContent, "Set Prefab Content Scale Y Enabled");
				prefabContent.isScaleYEnabled = isScaleYEnabled;
				EditorUtility.SetDirty(prefabContent);
			}

			EditorGUI.BeginChangeCheck();
			bool isScaleZEnabled = EditorGUILayout.Toggle("Scale Z", prefabContent.isScaleZEnabled);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(prefabContent, "Set Prefab Content Scale Z Enabled");
				prefabContent.isScaleZEnabled = isScaleZEnabled;
				EditorUtility.SetDirty(prefabContent);
			}

			EditorGUI.BeginChangeCheck();
			PrefabContent.ZScaleMode zScaleMode = (PrefabContent.ZScaleMode)EditorGUILayout.EnumPopup("Z Scale Mode", prefabContent.zScaleMode);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(prefabContent, "Set Prefab Content ZScale Mode");
				prefabContent.zScaleMode = zScaleMode;
				EditorUtility.SetDirty(prefabContent);
			}

			if (!isContainerLocked) {
				if (GUILayout.Button("Reload content")) {
					prefabContent.ReloadContent();
				}
			}
		}
	}
}
