﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	static public class EditorUtil {

		public enum Mode {
			Normal,
			Alternative	
		}

		public class Foldouts {
			public Dictionary<string, bool> state = new Dictionary<string, bool>();

			public bool Foldout(string id, string label, bool initial = false) {
				bool s = false;
				if (!state.TryGetValue(id, out s)) {
					s = initial;
				}
				s = EditorGUILayout.Foldout(s, label);
				state[id] = s;
				return s;
			}
		}

		static public class Layout {
			static public bool TexturedButton(string textureId, float scale = 1) {
				Texture t = EditorUtil.GetTexture(textureId);
				return GUILayout.Button(t, GUILayout.MaxWidth(t.width * scale), GUILayout.MaxHeight(t.height * scale));
			}
		}

		static Dictionary<string, Texture> textures = new Dictionary<string, Texture>();
		static bool isDragging;
		static Plane dragPlane;
		static Mode editMode;
		static bool isCtrlDown;

		static public Mode EditMode { get { return editMode; } }
		static public bool IsCtrlDown { get { return isCtrlDown; } }

		static public Texture GetTexture(string id) {
			Texture t = null;
			if (!textures.TryGetValue("Chui/" + id, out t) || t != null) {
				t = Resources.Load<Texture>("Chui/" + id);
				textures[id] = t;
			}
			return t;
		}

		static public void HandleEditModeKeys() {
			Event e = Event.current;
			if (e.type == EventType.KeyDown) {
				if (e.keyCode == KeyCode.Alpha1) {
					if (editMode == Mode.Alternative) {
						editMode = Mode.Normal;
					} else {
						editMode = Mode.Alternative;
					}
					SceneView.RepaintAll();
					e.Use();
				}
			}
			isCtrlDown = e.control;
		}

		static public float SnapUniformValue(float v, float density) {
			return ((int)(v * density + 0.5f)) / density;
		}

		static public Vector3 DragDotHandle(Transform transform, Vector3 point, string color) {
			int controlId = GUIUtility.GetControlID(color.GetHashCode(), FocusType.Passive);

			Vector3 transformedPoint = transform.TransformPoint(point);
			Vector2 guiPoint = HandleUtility.WorldToGUIPoint(transformedPoint);
			Rect rect = new Rect(guiPoint.x - 8, guiPoint.y - 8, 16, 16);

			bool isMouseOver = rect.Contains(Event.current.mousePosition);

			if (Event.current.type == EventType.Repaint) {
				string textureId = "chui-editor-" + color + "-dot";
				if (GUIUtility.hotControl == controlId) {
					textureId = "chui-editor-yellow-dot";
				}

				Handles.BeginGUI();
				GUI.DrawTexture(rect, GetTexture(textureId), ScaleMode.StretchToFill);
				Handles.EndGUI();
			}

			switch (Event.current.GetTypeForControl(controlId)) {
				case EventType.MouseDown:
					if (isMouseOver && !isDragging) {
						isDragging = true;
						GUIUtility.hotControl = controlId;
						dragPlane = new Plane(transform.TransformDirection(Vector3.forward), transformedPoint);
					}
					break;
				case EventType.MouseUp:
					if (isDragging) {
						isDragging = false;
						GUIUtility.hotControl = 0;
						Event.current.Use();
					}
					break;
				case EventType.MouseDrag:
					if (isDragging && GUIUtility.hotControl == controlId) {
						SceneView.RepaintAll();
						Ray r = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
						float distance;
						if (dragPlane.Raycast(r, out distance)) {
							return transform.InverseTransformPoint(r.GetPoint(distance));
						}
					}
					break;
			}
			return point;
		}

		static public void DrawWireCube(Vector3 position, Vector3 size) {
			Vector3 half = size * 0.5f;
			// draw front
			Handles.DrawLine(position + new Vector3(-half.x, -half.y, half.z), position + new Vector3(half.x, -half.y, half.z));
			Handles.DrawLine(position + new Vector3(-half.x, -half.y, half.z), position + new Vector3(-half.x, half.y, half.z));
			Handles.DrawLine(position + new Vector3(half.x, half.y, half.z), position + new Vector3(half.x, -half.y, half.z));
			Handles.DrawLine(position + new Vector3(half.x, half.y, half.z), position + new Vector3(-half.x, half.y, half.z));
			// draw back
			Handles.DrawLine(position + new Vector3(-half.x, -half.y, -half.z), position + new Vector3(half.x, -half.y, -half.z));
			Handles.DrawLine(position + new Vector3(-half.x, -half.y, -half.z), position + new Vector3(-half.x, half.y, -half.z));
			Handles.DrawLine(position + new Vector3(half.x, half.y, -half.z), position + new Vector3(half.x, -half.y, -half.z));
			Handles.DrawLine(position + new Vector3(half.x, half.y, -half.z), position + new Vector3(-half.x, half.y, -half.z));
			// draw corners
			Handles.DrawLine(position + new Vector3(-half.x, -half.y, -half.z), position + new Vector3(-half.x, -half.y, half.z));
			Handles.DrawLine(position + new Vector3(half.x, -half.y, -half.z), position + new Vector3(half.x, -half.y, half.z));
			Handles.DrawLine(position + new Vector3(-half.x, half.y, -half.z), position + new Vector3(-half.x, half.y, half.z));
			Handles.DrawLine(position + new Vector3(half.x, half.y, -half.z), position + new Vector3(half.x, half.y, half.z));
		}

		static public T CreateCustomAsset<T>() where T : ScriptableObject {
			string path = AssetDatabase.GetAssetPath (Selection.activeObject);
			if (path == "") {
				path = "Assets";
			} else if (Path.GetExtension(path) != "")  {
				path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
			}

			string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New " + typeof(T).ToString() + ".asset");

			T asset = ScriptableObject.CreateInstance<T>();
			AssetDatabase.CreateAsset(asset, assetPathAndName);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
			EditorUtility.FocusProjectWindow();
			Selection.activeObject = asset;

			EditorGUIUtility.PingObject(asset);

			return asset;
		}

		static public void WriteTextureToAsset(Texture2D t, string path) {
			byte[] textureBytes = t.EncodeToPNG();
			File.WriteAllBytes(Application.dataPath + path.Substring("Assets".Length), textureBytes);
			Debug.Log("[Chui.EditorUtil] Saved texture at " + path);
			AssetDatabase.Refresh();
			AssetDatabase.ImportAsset(path);
		}

		static public string GetAssetFileName(UnityEngine.Object o) {
			return System.IO.Path.GetFileName(AssetDatabase.GetAssetPath(o));
		}

		static public string GetAssetFileNameWithoutExtension(UnityEngine.Object o) {
			return System.IO.Path.GetFileNameWithoutExtension(AssetDatabase.GetAssetPath(o));
		}

		static public string GetAssetResourceDirectory(UnityEngine.Object o) {
			string path = System.IO.Path.GetDirectoryName(AssetDatabase.GetAssetPath(o));
			return path.Substring(7);
		}
	}
}
