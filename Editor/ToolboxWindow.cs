﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {
	public class ToolboxWindow : EditorWindow {

		Widget selectedWidget;

		[MenuItem("Chui/Toolbox")]
		public static void OpenWindow() {
			GetWindow<ToolboxWindow>("Chui Toolbox");
		}

		void OnGUI() {
			if (GUILayout.Button("Revalidate")) {
				Root.GetInstance().ClearInjectorContainer();
				Root.GetInstance().RevalidateHierarchy();
			}

			selectedWidget = (Widget)EditorGUILayout.ObjectField("Selected widget", selectedWidget, typeof(Widget), true);

			if (GUILayout.Button("Select from hierarchy")) {
				if (Selection.activeGameObject != null) {
					selectedWidget = (Selection.activeGameObject).GetComponent<Widget>();
				}
			}

			EditorGUILayout.Separator();

			if (selectedWidget != null) {
				Widget targetWidget = null;
				if (Selection.activeGameObject != null) {
					targetWidget = Selection.activeGameObject.GetComponent<Widget>();
				}
				if (targetWidget != null && targetWidget != selectedWidget) {
					if (GUILayout.Button("Paste as child")) {
						selectedWidget.SetParent(targetWidget);
						Selection.activeGameObject = selectedWidget.gameObject;
					}
				}

				if (GUILayout.Button("Convert to FixedWidget")) {
					Util.ConvertToFixedWidget(selectedWidget);
					selectedWidget.enabled = false;
				}

				if (selectedWidget is AnchoredWidget && GUILayout.Button("Convert offsets to relative")) {
					(selectedWidget as AnchoredWidget).ConvertOffsetsToRelative();
				}
			}
		}
	}
}
