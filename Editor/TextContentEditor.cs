﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {

	[CustomEditor(typeof(TextContent))]
	public class TextContentEditor : Editor {

		TextContent textContent;
		Vector2 textScroll;

		public override void OnInspectorGUI() {
			
			textContent = target as TextContent;

			string text = EditorGUILayout.TextArea(textContent.text);
			if (text != textContent.text) {
				Undo.RecordObject(textContent, "Set Text Value");
				textContent.text = text;
				EditorUtility.SetDirty(textContent);
			}

			FontInfo fontInfo = EditorGUILayout.ObjectField("Font Info", textContent.fontInfo, typeof(FontInfo), false) as FontInfo;
			if (fontInfo != textContent.fontInfo) {
				Undo.RecordObject(textContent, "Set Text Font");
				textContent.fontInfo = fontInfo;
				EditorUtility.SetDirty(textContent);
			}

			float fontSize = EditorGUILayout.FloatField("Size", textContent.fontSize);
			if (fontSize != textContent.fontSize) {
				Undo.RecordObject(textContent, "Set Text Size");
				textContent.fontSize = fontSize;
				EditorUtility.SetDirty(textContent);
			}

			Color color = EditorGUILayout.ColorField("Color", textContent.color);
			if (color != textContent.color) {
				Undo.RecordObject(textContent, "Set Text Color");
				textContent.color = color;
				EditorUtility.SetDirty(textContent);
			}

			TextContent.HorizontalAlign hAlign = (TextContent.HorizontalAlign)EditorGUILayout.EnumPopup("Horizontal Layout", textContent.hAlign);
			if (hAlign != textContent.hAlign) {
				Undo.RecordObject(textContent, "Set Text HAlign");
				textContent.hAlign = hAlign;
				EditorUtility.SetDirty(textContent);
			}

			TextContent.VerticalAlign vAlign = (TextContent.VerticalAlign)EditorGUILayout.EnumPopup("Vertical Layout", textContent.vAlign);
			if (vAlign != textContent.vAlign) {
				Undo.RecordObject(textContent, "Set Text VAlign");
				textContent.vAlign = vAlign;
				EditorUtility.SetDirty(textContent);
			}

			TextContent.ScaleMode scaleMode = (TextContent.ScaleMode)EditorGUILayout.EnumPopup("Scale Mode", textContent.scaleMode);
			if (scaleMode != textContent.scaleMode) {
				Undo.RecordObject(textContent, "Set Scale Mode");
				textContent.scaleMode = scaleMode;
				EditorUtility.SetDirty(textContent);
			}

			bool isWordWrapEnabled = EditorGUILayout.Toggle("Word Wrap", textContent.isWordWrapEnabled);
			if (isWordWrapEnabled != textContent.isWordWrapEnabled) {
				Undo.RecordObject(textContent, "Set Text Word Wrap");
				textContent.isWordWrapEnabled = isWordWrapEnabled;
				EditorUtility.SetDirty(textContent);
			}

			bool isJustifyEnabled = EditorGUILayout.Toggle("Justify", textContent.isJustifyEnabled);
			if (isJustifyEnabled != textContent.isJustifyEnabled) {
				Undo.RecordObject(textContent, "Set Text Justify");
				textContent.isJustifyEnabled = isJustifyEnabled;
				EditorUtility.SetDirty(textContent);
			}

			bool isTagEnabled = EditorGUILayout.Toggle("Tags", textContent.isTagEnabled);
			if (isTagEnabled != textContent.isTagEnabled) {
				Undo.RecordObject(textContent, "Set Text Tags");
				textContent.isTagEnabled = isTagEnabled;
				EditorUtility.SetDirty(textContent);
			}

			float lineSpacing = EditorGUILayout.FloatField("Line spacing", textContent.lineSpacing);
			if (lineSpacing != textContent.fontSize) {
				Undo.RecordObject(textContent, "Set Line Spacing");
				textContent.lineSpacing = lineSpacing;
				EditorUtility.SetDirty(textContent);
			}


			EditorGUILayout.Separator();
		}
	}
}
