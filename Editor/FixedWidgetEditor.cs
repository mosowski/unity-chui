using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	[CustomEditor(typeof(FixedWidget))]
	public class FixedWidgetEditor : WidgetEditor {

		protected FixedWidget widget;

		public void HandleVertex(Vector3 unitVertex, Vector3 axis) {
			Vector3 vertex = new Vector3(
				widget.volume.xMin + unitVertex.x * widget.volume.Width,
				widget.volume.yMin + unitVertex.y * widget.volume.Height,
				widget.volume.zMin + unitVertex.z * widget.volume.Depth
			);

			Vector3 newVertex = EditorUtil.DragDotHandle(widget.transform, vertex, "blue");

			newVertex = new Vector3(
				newVertex.x * axis.x + vertex.x * (1 - axis.x),
				newVertex.y * axis.y + vertex.y * (1 - axis.y),
				newVertex.z * axis.z + vertex.z * (1 - axis.z)
			);

			if (Vector3.Distance(newVertex, vertex) > 0.01f) {
				Undo.RecordObject(widget, "Set FixedWidget Size");
				Undo.RecordObject(widget.transform, "Set FixedWidget Transform");

				Volume newVolume = widget.volume.Copy();

				newVolume.xMin += axis.x * Mathf.Abs(1 - unitVertex.x) * (newVertex.x - newVolume.xMin);
				newVolume.xMax += axis.x * Mathf.Abs(unitVertex.x) * (newVertex.x - newVolume.xMax);
				newVolume.yMin += axis.y * Mathf.Abs(1 - unitVertex.y) * (newVertex.y - newVolume.yMin);
				newVolume.yMax += axis.y * Mathf.Abs(unitVertex.y) * (newVertex.y - newVolume.yMax);

				Vector3 deltaPos = new Vector3(
					newVolume.xMin + widget.pivot.x * newVolume.Width - (widget.volume.xMin + widget.pivot.x * widget.volume.Width),
					newVolume.yMin + widget.pivot.y * newVolume.Height - (widget.volume.yMin + widget.pivot.y * widget.volume.Height),
					0
				);
				deltaPos.Scale(widget.transform.localScale);

				widget.Width = newVolume.Width;
				widget.Height = newVolume.Height;

				Vector3 localPosition = widget.transform.localPosition;
				localPosition += deltaPos;
				widget.transform.localPosition = localPosition;

				EditorUtility.SetDirty(target);
			}
		}

		public void EdgesHandle() {
			HandleVertex(new Vector3(0, 0, 0), new Vector3(1, 1, 0));
			HandleVertex(new Vector3(1, 0, 0), new Vector3(1, 1, 0));
			HandleVertex(new Vector3(1, 1, 0), new Vector3(1, 1, 0));
			HandleVertex(new Vector3(0, 1, 0), new Vector3(1, 1, 0));

			HandleVertex(new Vector3(0, 0.5f, 0), new Vector3(1, 0, 0));
			HandleVertex(new Vector3(1, 0.5f, 0), new Vector3(1, 0, 0));
			HandleVertex(new Vector3(0.5f, 0, 0), new Vector3(0, 1, 0));
			HandleVertex(new Vector3(0.5f, 1, 0), new Vector3(0, 1, 0));
		}

		public virtual void OnSceneGUI() {
			widget = target as FixedWidget;

			if ((widget.gameObject.hideFlags & HideFlags.NotEditable) != HideFlags.NotEditable) {
				EdgesHandle();
				DrawGizmos();
			}
		}

		public override void OnInspectorGUI() {
			widget = target as FixedWidget;

			InspectPivot(widget);
			InspectSize(widget);
			InspectColor(widget);
			InspectCollisions(widget);
			InspectStatic(widget);
			
			serializedObject.ApplyModifiedProperties();
		}
	}
}
