using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {

	public class WidgetEditor : Editor {

		[MenuItem("Chui/Widget/Set Static/Recursive True")]
		static void MenuWidgetSetStaticRecursiveTrue() {
			GameObject go = Selection.activeObject as GameObject;
			if (go != null) {
				foreach (Widget w in go.GetComponentsInChildren<Widget>()) {
					w.isStatic = true;
				}
			}
		}
		[MenuItem("Chui/Widget/Set Static/Recursive False")]
		static void MenuWidgetSetStaticRecursiveFalse() {
			GameObject go = Selection.activeObject as GameObject;
			if (go != null) {
				foreach (Widget w in go.GetComponentsInChildren<Widget>()) {
					w.isStatic = false;
				}
			}
		}

		public void InspectLocalPosition(Widget widget) {
			Vector3 localPosition = widget.transform.localPosition;
			EditorGUI.BeginChangeCheck();
			localPosition = EditorGUILayout.Vector3Field("Local Position", localPosition);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(widget, "Set Widget Local Position");
				widget.SetLocalPosition(localPosition);
				EditorUtility.SetDirty(widget);
			}
		}

		public void InspectPivot(Widget widget) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("pivot"));
		}

		public void InspectSize(Widget widget) {
			EditorGUILayout.LabelField("Size", EditorStyles.boldLabel);
			EditorGUI.BeginChangeCheck();
			float newWidth = EditorGUILayout.FloatField("Width", widget.Width);
			float newHeight = EditorGUILayout.FloatField("Height", widget.Height);
			float newDepth = EditorGUILayout.FloatField("Depth", widget.Depth);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(widget, "Set Widget Anchor");
				widget.Width = newWidth;
				widget.Height = newHeight;
				widget.Depth = newDepth;
				EditorUtility.SetDirty(widget);
			}
		}

		public void InspectColor(Widget widget) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("color"));
		}

		public void InspectCollisions(Widget widget) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("isColliderEnabled"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("isColliderAutoResizeEnabled"));
		}

		public void InspectStatic(Widget widget) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("isStatic"));
		}

		public virtual void DrawGizmos() {
			Widget widget = target as Widget;
			Handles.matrix = widget.gameObject.transform.localToWorldMatrix;
			if (Selection.activeGameObject == widget.gameObject) {
				Handles.color = new Color(1, 1, 0, 1);
			}

			EditorUtil.DrawWireCube(
				new Vector3(widget.volume.Width * (0.5f - widget.pivot.x), widget.volume.Height * (0.5f - widget.pivot.y), widget.volume.Depth * (0.5f - widget.pivot.z)),
				new Vector3(widget.volume.Width, widget.volume.Height, widget.Depth)
			);
		}
	}
}
