﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Chui {

	[CustomEditor(typeof(AnchoredWidget))]
	[CanEditMultipleObjects]
	public class AnchoredWidgetEditor : WidgetEditor {

		Tool lastTool;
		protected AnchoredWidget widget;

		Vector3 xAxis;
		Vector3 yAxis;
		List<float> xSnaps = new List<float>();
		List<float> ySnaps = new List<float>();
		EditorUtil.Foldouts foldouts = new EditorUtil.Foldouts();

		[MenuItem("Chui/Widget/Set nearest relative anchors")]
		static void MenuWidgetSetNearestRelativeAnchors() {
			GameObject go = Selection.activeObject as GameObject;
			if (go != null) {
				AnchoredWidget w = go.GetComponent<AnchoredWidget>();
				if (w != null) {
					Undo.RecordObject(w, "Set AnchoredWidget Anchor");
					w.SetNearestRelativeAnchors();
					EditorUtility.SetDirty(w);
				}
			}
		}

		static AnchoredWidget cutWidget;
		[MenuItem("Chui/AnchoredWidget/Cut")]
		static void MenuWidgetCut() {
			GameObject go = Selection.activeObject as GameObject;
			if (go != null) {
				cutWidget = go.GetComponent<AnchoredWidget>();
				if (cutWidget != null) {
					Debug.Log("[Chui.WidgetEditor] AnchoredWidget " + go.name + " selected for cut.");
				}
			}
		}

		[MenuItem("Chui/AnchoredWidget/Paste")]
		static void MenuWidgetPaste() {
			GameObject go = Selection.activeObject as GameObject;
			if (go != null && cutWidget != null) {
				AnchoredWidget parentWidget = go.GetComponent<AnchoredWidget>();
				if (parentWidget != null) {
					Undo.SetTransformParent(cutWidget.transform, parentWidget.transform, "Set AnchoredWidget Parent");
					Undo.RecordObject(cutWidget, "Set AnchoredWidget Anchor");
					cutWidget.SetParent(parentWidget);
					Debug.Log("[Chui.WidgetEditor] Pasted widget " + cutWidget.name + " on " + parentWidget.name);
					EditorUtility.SetDirty(cutWidget);
					Selection.activeObject = cutWidget.gameObject;
					cutWidget = null;
				}
			}
		}

		void OnEnable() {
			lastTool = Tools.current;
			Tools.current = Tool.None;
		}

		void OnDisable() {
			Tools.current = lastTool;
		}

		public void TranslationHandle() {
			Vector3 oldPosition = widget.transform.position;
			Vector3 newPosition = Handles.PositionHandle(oldPosition, Quaternion.identity);
			if (oldPosition != newPosition) {
				Vector3 diff = widget.transform.parent.InverseTransformVector(newPosition - oldPosition);
				Undo.RecordObject(widget, "Set AnchoredWidget Anchor");
				widget.Translate(diff.x, diff.y, diff.z);
				EditorUtility.SetDirty(widget);
			}
		}

		public void AddRectSnaps(Transform t, Volume volume, List<float> x, List<float> y) {
			Vector3 le = widget.transform.InverseTransformPoint(t.TransformPoint(new Vector3(volume.xMin, volume.yMin, 0)));
			x.Add(le.x);

			Vector3 re = widget.transform.InverseTransformPoint(t.TransformPoint(new Vector3(volume.xMax, volume.yMin, 0)));
			x.Add(re.x);

			Vector3 to = widget.transform.InverseTransformPoint(t.TransformPoint(new Vector3(volume.xMin, volume.yMax, 0)));
			y.Add(to.y);

			Vector3 bo = widget.transform.InverseTransformPoint(t.TransformPoint(new Vector3(volume.xMax, volume.yMin, 0)));
			y.Add(bo.y);
		}

		public void GetSnaps(List<float> x, List<float> y) {
			x.Clear();
			y.Clear();

			Volume relativeVolume = widget.volume.Copy();
			relativeVolume.xMin -= widget.leftAnchor.offset;
			relativeVolume.yMin -= widget.bottomAnchor.offset;
			relativeVolume.xMax -= widget.rightAnchor.offset;
			relativeVolume.yMax -= widget.topAnchor.offset;
			AddRectSnaps(widget.transform, relativeVolume, x, y);

			Transform pt = widget.transform.parent;
			if (pt != null) {
				for (int i = 0; i < pt.childCount; ++i) {
					GameObject ch = pt.GetChild(i).gameObject;
					Widget chw = ch.GetComponent<Widget>();
					if (chw != null) {
						AddRectSnaps(chw.transform, chw.volume, x, y);
					}
				}
				Widget pw = pt.GetComponent<Widget>();
				if (pw != null) {
					AddRectSnaps(pw.transform, pw.volume, x, y);
				} else {
					Root r = pt.GetComponent<Root>();
					if (r != null) {
						AddRectSnaps(r.transform, r.volume, x, y);
					}
				}
			}
		}

		public void HandleAnchor(ref Anchor anchor, Vector3 axis, List<float> snaps, float edgeX, float edgeY) {
			Volume targetVolume = anchor.target == null ? widget.ParentVolume : anchor.target.volume;
			if (targetVolume == null) {
				return;
			}

			Transform targetTransform = anchor.target == null ? widget.transform.parent : anchor.target.transform;
			Vector3 midpoints = targetTransform.InverseTransformPoint(widget.transform.TransformPoint(new Vector3((widget.volume.xMin + widget.volume.xMax) / 2, (widget.volume.yMin + widget.volume.yMax) / 2, 0)));

			float parentSize = targetVolume.Width * axis.x + targetVolume.Height * axis.y;

			Vector3 relativeOrigin = new Vector3(
				axis.x * (targetVolume.xMin + anchor.relative * targetVolume.Width) + 
				(1 - axis.x) * (midpoints.x),
				axis.y * (targetVolume.yMin + anchor.relative * targetVolume.Height) + 
				(1 - axis.y) * (midpoints.y),
				targetVolume.Depth
			);

			Vector3 origin = new Vector3(
				axis.x * (targetVolume.xMin + anchor.relative * targetVolume.Width + anchor.offset) + 
				(1 - axis.x) * (midpoints.x),
				axis.y * (targetVolume.yMin + anchor.relative * targetVolume.Height + anchor.offset) + 
				(1 - axis.y) * (midpoints.y),
				targetVolume.Depth
			);

			Handles.color = Color.cyan;
			Handles.DrawLine(targetTransform.TransformPoint(relativeOrigin), targetTransform.TransformPoint(origin));

			if (EditorUtil.EditMode == EditorUtil.Mode.Normal) {
				Vector3 newOrigin = EditorUtil.DragDotHandle(targetTransform, origin, "blue");
				if (Vector3.Distance(newOrigin, origin) > 0.01f) {
					Undo.RecordObject(widget, "Set AnchoredWidget Anchor");

					float newLocalOrigin = Vector3.Dot(axis, widget.transform.InverseTransformPoint(targetTransform.TransformPoint(newOrigin)));
					float originDot = Vector3.Dot(axis, origin);
					float newOriginDot = Vector3.Dot(axis, newOrigin);

					if (EditorUtil.IsCtrlDown) {
						int bestSnap = -1;
						for (int i = 0; i < snaps.Count; ++i) {
							if (bestSnap == -1 || Mathf.Abs(newLocalOrigin - snaps[i]) < Mathf.Abs(newLocalOrigin - snaps[bestSnap])) {
								bestSnap = i;
							}
						}
						if (bestSnap != -1) {
							newOriginDot = Vector3.Dot(axis, targetTransform.InverseTransformPoint(widget.transform.TransformPoint(new Vector3(snaps[bestSnap], snaps[bestSnap], 0))));
						}
					}
					anchor.offset += newOriginDot - originDot;
					EditorUtility.SetDirty(target);
				}
			} else {
				Vector3 newRelativeOrigin = EditorUtil.DragDotHandle(targetTransform, relativeOrigin, "teal");
				if (Vector3.Distance(newRelativeOrigin, relativeOrigin) > 0.01f) {
					Undo.RecordObject(widget, "Set AnchoredWidget Anchor");
					float originDot = Vector3.Dot(axis, newRelativeOrigin - relativeOrigin);
					float newRelative = anchor.relative + originDot / parentSize;
					if (EditorUtil.IsCtrlDown) {
						newRelative = EditorUtil.SnapUniformValue(newRelative, 4);
					}
					anchor.relative = newRelative;
					EditorUtility.SetDirty(target);
				}
			}
		}

		public void EdgesHandle() {
			xAxis = Vector3.right;
			yAxis = Vector3.up;
			GetSnaps(xSnaps, ySnaps);

			HandleAnchor(ref widget.leftAnchor, xAxis, xSnaps, 0, 0.5f);
			HandleAnchor(ref widget.rightAnchor, xAxis, xSnaps, 1, 0.5f);
			HandleAnchor(ref widget.bottomAnchor, yAxis, ySnaps, 0.5f, 0);
			HandleAnchor(ref widget.topAnchor, yAxis, ySnaps, 0.5f, 1);
		}

		public void PivotHandle() {
			Vector3 pivotDepth = new Vector3(0, 0, -widget.pivot.z * widget.Depth);
			Vector3 pivot = Vector3.zero;

			Handles.color = Color.green;
			Handles.DrawLine(widget.transform.TransformPoint(pivotDepth), widget.transform.TransformPoint(pivot));

			Vector3 newPivot = EditorUtil.DragDotHandle(widget.transform, Vector3.zero, "green");
			if (pivot != newPivot) {
				Undo.RecordObject(widget, "Set AnchoredWidget Pivot");
				newPivot.x /= widget.volume.Width;
				newPivot.y /= widget.volume.Height;
				widget.pivot.x += newPivot.x;
				widget.pivot.y += newPivot.y;
				if (EditorUtil.IsCtrlDown) {
					widget.pivot.x = EditorUtil.SnapUniformValue(widget.pivot.x, 4);
					widget.pivot.y = EditorUtil.SnapUniformValue(widget.pivot.y, 4);
				}
				EditorUtility.SetDirty(target);
			}
		}

		public virtual void OnSceneGUI() {
			widget = target as AnchoredWidget;
			if (Tools.current == Tool.Move) {
				Tools.current = Tool.None;
			}

			if ((widget.gameObject.hideFlags & HideFlags.NotEditable) != HideFlags.NotEditable) {
				EditorUtil.HandleEditModeKeys();

				if (EditorUtil.EditMode == EditorUtil.Mode.Alternative) {
					TranslationHandle();
				} else {
					PivotHandle();
				}
				EdgesHandle();

				DrawGizmos();
			}
		}

		public void InspectAnchorConstraint() {
			EditorGUI.BeginChangeCheck();
			AnchoredWidget.AnchorConstraint anchorConstraint = (AnchoredWidget.AnchorConstraint)EditorGUILayout.EnumPopup("Constraint", widget.anchorConstraint);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchor Constraint");
				if (anchorConstraint != AnchoredWidget.AnchorConstraint.None) {
					widget.fixedAspect = widget.Width / widget.Height;
				}
				widget.anchorConstraint = anchorConstraint;
				EditorUtility.SetDirty(widget);
			}

			if (anchorConstraint != AnchoredWidget.AnchorConstraint.None) {
				EditorGUI.BeginChangeCheck();
				float fixedAspect = EditorGUILayout.FloatField("Fixed aspect", widget.fixedAspect);
				if (fixedAspect == 0) {
					fixedAspect = 1;
				}

				if (EditorGUI.EndChangeCheck()) {
					Undo.RecordObject(widget, "Set AnchoredWidget Fixed Aspect");
					widget.fixedAspect = fixedAspect;
					EditorUtility.SetDirty(widget);
				}
			}
		}

		Texture chuiEditorAnchorTopLeftTexture;

		void InspectAutoAnchorButtons() {
			float buttonScale = 0.25f;
			EditorGUILayout.BeginHorizontal();
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-topleft", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0, 1, 0, 1);
				EditorUtility.SetDirty(widget);
			}
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-topcenter", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0.5f, 1, 0.5f, 1);
				EditorUtility.SetDirty(widget);
			}
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-topright", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(1, 1, 1, 1);
				EditorUtility.SetDirty(widget);
			}
			GUILayout.Space(10);
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-verticalcenter", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0.5f, 0, 0.5f, 1);
				EditorUtility.SetDirty(widget);
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-centerleft", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0, 0.5f, 0, 0.5f);
				EditorUtility.SetDirty(widget);
			}
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-center", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0.5f, 0.5f, 0.5f, 0.5f);
				EditorUtility.SetDirty(widget);
			}
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-centerright", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(1, 0.5f, 1, 0.5f);
				EditorUtility.SetDirty(widget);
			}
			GUILayout.Space(10);
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-full", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0, 0, 1, 1);
				EditorUtility.SetDirty(widget);
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-bottomleft", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0, 0, 0, 0);
				EditorUtility.SetDirty(widget);
			}
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-bottomcenter", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0.5f, 0, 0.5f, 0);
				EditorUtility.SetDirty(widget);
			}
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-bottomright", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(1, 0, 1, 0);
				EditorUtility.SetDirty(widget);
			}
			GUILayout.Space(10);
			if (EditorUtil.Layout.TexturedButton("chui-editor-anchor-horizontalcenter", buttonScale)) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetRelativeAnchors(0, 0.5f, 1, 0.5f);
				EditorUtility.SetDirty(widget);
			}
			EditorGUILayout.EndHorizontal();

			if (GUILayout.Button("Reset Offsets", GUILayout.MinWidth(150), GUILayout.MaxWidth(150))) {
				Undo.RecordObject(widget, "Set AnchoredWidget Anchors");
				widget.SetOffsetAnchors(0, 0, 0, 0);
				EditorUtility.SetDirty(widget);
			}
		}

		public override void OnInspectorGUI() {
			widget = target as AnchoredWidget;

			InspectAnchorConstraint();

			if (foldouts.Foldout("anchorPresets", "Anchor presets", false)) {
				InspectAutoAnchorButtons();
			}

			InspectLocalPosition(widget);

			if (foldouts.Foldout("anchors", "Anchors", true)) {
				// InspectAnchorProperty(ref widget.leftAnchor, "Left anchor");
				EditorGUILayout.PropertyField(serializedObject.FindProperty("leftAnchor"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("rightAnchor"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("bottomAnchor"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("topAnchor"));
				if (foldouts.Foldout("depthAnchors", "Depth Anchors", false)) {
					EditorGUILayout.PropertyField(serializedObject.FindProperty("backAnchor"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("frontAnchor"));
				}
			}
			InspectPivot(widget);
			InspectSize(widget);
			InspectColor(widget);
			InspectCollisions(widget);
			InspectStatic(widget);

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Revalidate")) {
				widget.Revalidate();
			}
			if (GUILayout.Button("Rev. Hierarchy")) {
				widget.RevalidateHierarchy();
			}
			GUILayout.EndHorizontal();

			serializedObject.ApplyModifiedProperties();

		}
	}
}
