﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {
	
	[CustomEditor(typeof(RoundTextContent))]
	public class RoundTextContentEditor : TextContentEditor {
		
		RoundTextContent roundText;
		
		public void OnSceneGUI () 
		{
			roundText = target as RoundTextContent;
			Handles.color = Color.red;
			Handles.matrix = roundText.transform.localToWorldMatrix;
			Handles.color = new Color(1, 1, 1, 0.8f);
			Handles.DrawWireDisc(Vector3.zero, roundText.transform.forward, roundText.radius);
			Handles.color = Color.yellow;
			float angleRad = roundText.widget.Width / roundText.radius;
			float angle = Mathf.Rad2Deg * angleRad;
			
			Handles.DrawWireArc(Vector3.zero, roundText.transform.forward, Quaternion.Euler(0, 0, roundText.angle - angle / 2) * roundText.transform.up, angle, roundText.radius + roundText.widget.Height / 2);
			Handles.DrawWireArc(Vector3.zero, roundText.transform.forward, Quaternion.Euler(0, 0, roundText.angle - angle / 2) * roundText.transform.up, angle, roundText.radius - roundText.widget.Height / 2);
			
			float c = Mathf.Cos(roundText.angle * Mathf.Deg2Rad - angleRad / 2 + Mathf.PI / 2);
			float s = Mathf.Sin(roundText.angle * Mathf.Deg2Rad - angleRad / 2 + Mathf.PI / 2);
			Handles.DrawLine( new Vector3(c * (roundText.radius + roundText.widget.Height / 2), s * (roundText.radius + roundText.widget.Height / 2), 0f), new Vector3(c * (roundText.radius - roundText.widget.Height / 2), s * (roundText.radius - roundText.widget.Height / 2), 0f));
			c = Mathf.Cos(roundText.angle * Mathf.Deg2Rad + angleRad / 2 + Mathf.PI / 2);
			s = Mathf.Sin(roundText.angle * Mathf.Deg2Rad + angleRad / 2 + Mathf.PI / 2);
			Handles.DrawLine( new Vector3(c * (roundText.radius + roundText.widget.Height / 2), s * (roundText.radius + roundText.widget.Height / 2), 0f), new Vector3(c * (roundText.radius - roundText.widget.Height / 2), s * (roundText.radius - roundText.widget.Height / 2), 0f));
		}
		
		public override void OnInspectorGUI() {
			
			roundText = target as RoundTextContent;
			
			float radius = EditorGUILayout.FloatField("Radius", roundText.radius);
			if (radius != roundText.radius) {
				Undo.RecordObject(roundText, "Set Radius Value");
				roundText.radius = radius;
				EditorUtility.SetDirty(roundText);
			}
			
			float angle = EditorGUILayout.FloatField("Angle", roundText.angle);
			if (angle != roundText.angle) {
				Undo.RecordObject(roundText, "Set Angle Value");
				roundText.angle = angle;
				EditorUtility.SetDirty(roundText);
			}

			float spacing = EditorGUILayout.FloatField("Spacing", roundText.spacing);
			if (spacing != roundText.spacing) {
				Undo.RecordObject(roundText, "Set Spacing Value");
				roundText.spacing = spacing;
				EditorUtility.SetDirty(roundText);
			}
			
			base.OnInspectorGUI();
		}
	}
}
