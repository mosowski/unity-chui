﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Chui {
	[CustomEditor(typeof(QuadContent)), CanEditMultipleObjects]
	public class QuadContentEditor : AbstractEditor<QuadContent> {
		private SerializedProperty geometryModeProperty;
		private SerializedProperty uFlipProperty;
		private SerializedProperty vFlipProperty;
		private SerializedProperty uvRectProperty;
		private SerializedProperty borderProperty;
		private SerializedProperty fillBorderCenterProperty;
		private SerializedProperty borderSizeUVLeftProperty;
		private SerializedProperty borderSizeUVRightProperty;
		private SerializedProperty borderSizeUVBottomProperty;
		private SerializedProperty borderSizeUVTopProperty;
		private SerializedProperty borderSizeRealLeftProperty;
		private SerializedProperty borderSizeRealRightProperty;
		private SerializedProperty borderSizeRealBottomProperty;
		private SerializedProperty borderSizeRealTopProperty;
		private SerializedProperty tileSettingsXSizeProperty;
		private SerializedProperty tileSettingsYSizeProperty;
		private SerializedProperty tileSettingsXOffsetProperty;
		private SerializedProperty tileSettingsYOffsetProperty;
		private SerializedProperty tileSettingsOriginProperty;
		private SerializedProperty materialProperty;
		private SerializedProperty colorProperty;

		QuadContent quadContent;

		protected virtual void OnEnable() {
			geometryModeProperty = serializedObject.FindProperty("geometryMode");
			uFlipProperty = serializedObject.FindProperty("uFlip");
			vFlipProperty = serializedObject.FindProperty("vFlip");
			uvRectProperty = serializedObject.FindProperty("uvRect");
			borderProperty = serializedObject.FindProperty("border");
			fillBorderCenterProperty = serializedObject.FindProperty("fillBorderCenter");
			borderSizeUVLeftProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("uvLeft");
			borderSizeUVRightProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("uvRight");
			borderSizeUVBottomProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("uvBottom");
			borderSizeUVTopProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("uvTop");
			borderSizeRealLeftProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("realLeft");
			borderSizeRealRightProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("realRight");
			borderSizeRealBottomProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("realBottom");
			borderSizeRealTopProperty = serializedObject.FindProperty("borderSize").FindPropertyRelative("realTop");
			tileSettingsXSizeProperty = serializedObject.FindProperty("tileSettings").FindPropertyRelative("xSize");
			tileSettingsYSizeProperty = serializedObject.FindProperty("tileSettings").FindPropertyRelative("ySize");
			tileSettingsXOffsetProperty = serializedObject.FindProperty("tileSettings").FindPropertyRelative("xOffset");
			tileSettingsYOffsetProperty = serializedObject.FindProperty("tileSettings").FindPropertyRelative("yOffset");
			tileSettingsOriginProperty = serializedObject.FindProperty("tileSettings").FindPropertyRelative("origin");
			materialProperty = serializedObject.FindProperty("material");
			colorProperty = serializedObject.FindProperty("color");
		}

		public override void OnInspectorGUI() {
			quadContent = target as QuadContent;

			EditorGUILayout.PropertyField(geometryModeProperty);
			EditorGUILayout.PropertyField(uFlipProperty);
			EditorGUILayout.PropertyField(vFlipProperty);

			DrawUVRect();
			DrawBorder();
			DrawMaterialAndColor();
			DrawGeometryModeParams();

			if (GUILayout.Button("Set Size From Texture")) {
				PerformActionOnAllTargetObjects(SetSizeFromTexture, "Set Size From Texture");
			}

			serializedObject.ApplyModifiedProperties();
		}

		protected virtual void DrawUVRect() {
			EditorGUILayout.PropertyField(uvRectProperty);
		}

		protected virtual void DrawBorder() {
			EditorGUILayout.PropertyField(borderProperty);
		}

		protected virtual void DrawGeometryModeParams() {
			QuadGeometryMode geometryMode = GetEnum<QuadGeometryMode>(geometryModeProperty);
			if (geometryMode == QuadGeometryMode.Scale9) {
				DrawScale9Params();
			} else if (geometryMode == QuadGeometryMode.UvTiled) {
				DrawUVTiledParams();
			}
		}

		protected virtual void DrawScale9Params() {
			if (foldouts.Foldout("borderSize", "Border Size")) {
				EditorGUI.indentLevel++;
				EditorGUILayout.PropertyField(borderSizeUVLeftProperty);
				EditorGUILayout.PropertyField(borderSizeUVRightProperty);
				EditorGUILayout.PropertyField(borderSizeUVBottomProperty);
				EditorGUILayout.PropertyField(borderSizeUVTopProperty);
				EditorGUILayout.PropertyField(borderSizeRealLeftProperty);
				EditorGUILayout.PropertyField(borderSizeRealRightProperty);
				EditorGUILayout.PropertyField(borderSizeRealBottomProperty);
				EditorGUILayout.PropertyField(borderSizeRealTopProperty);
				EditorGUI.indentLevel--;
			}
		}

		protected virtual void DrawFillBorderCenter() {
			EditorGUILayout.PropertyField(fillBorderCenterProperty, new GUIContent("Fill Center"));
		}

		protected virtual void DrawUVTiledParams() {
			if (foldouts.Foldout("tileSettings", "Tile Settings")) {
				EditorGUI.indentLevel++;
				EditorGUILayout.PropertyField(tileSettingsXSizeProperty);
				EditorGUILayout.PropertyField(tileSettingsYSizeProperty);
				EditorGUILayout.PropertyField(tileSettingsXOffsetProperty);
				EditorGUILayout.PropertyField(tileSettingsYOffsetProperty);
				EditorGUILayout.PropertyField(tileSettingsOriginProperty);
				EditorGUI.indentLevel--;
			}
		}

		protected virtual void DrawMaterialAndColor() {
			EditorGUILayout.PropertyField(materialProperty);
			EditorGUILayout.PropertyField(colorProperty);
		}

		private void SetSizeFromTexture(QuadContent quadContent) {
			QuadGeometryMode geometryMode = GetEnum<QuadGeometryMode>(geometryModeProperty);
			if (quadContent.material != null && quadContent.material.mainTexture != null && geometryMode == QuadGeometryMode.Quad && quadContent.widget != null) {
				quadContent.widget.Width = quadContent.material.mainTexture.width;
				quadContent.widget.Height = quadContent.material.mainTexture.height;
			}
		}
	}
}
