﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Chui {

	[CustomEditor(typeof(PrefabContentInfo))]
	public class PrefabContentInfoEditor : Editor {

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			PrefabContentInfo pi = target as PrefabContentInfo;

			if (GUILayout.Button("Get Bounds from children")) {
				Bounds b = new Bounds();
				b.size = Vector3.zero;

				foreach (Renderer r in pi.GetComponentsInChildren<Renderer>()) {
					if (b.size == Vector3.zero) {
						b = r.bounds;
					} else {
						b.Encapsulate(r.bounds);
					}
				}

				pi.width = b.size.x;
				pi.height = b.size.y;
				pi.depth = b.size.z;

				foreach (Transform ch in pi.transform) {
					ch.localPosition -= new Vector3(b.min.x, b.min.y, b.min.z + b.size.z);
				}
			}
		}
	}
}
